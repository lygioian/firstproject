import {handleActions} from 'redux-actions';
import {SHOW_DIALOG, HIDE_DIALOG} from '../action-types'
import { ActionSheetIOS } from 'react-native';

const initialState = {
    title: "",
    description: "",
    isShow: false,
    button1: "YES",
    button2: "NO",
    onButton1: () => {},
    onButton2: () => {},
    needInput: false,
}

export default handleActions(
    {
        [SHOW_DIALOG]: (state, action) => ({
            ...initialState,
            title: action.payload.title,
            description: action.payload.description,
            button1: action.payload.button1,
            button2: action.payload.button2,
            onButton1: action.payload.onButton1,
            onButton2: action.payload.onButton2,
            isShow: true,
            needInput: action.payload.needInput,
        }),
        [HIDE_DIALOG]: (state, action) => ({
            ...state,
            isShow: false,
        })
    },
    initialState
);