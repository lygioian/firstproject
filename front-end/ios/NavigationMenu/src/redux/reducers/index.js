import { combineReducers } from 'redux';

import me from './me';
import events from './events';
import news from './news';
import loading from './loading'
import toasting from './toastNotification'
import dialog from './dialog';
import users from './user'

export default function createRootReducer() {
    return combineReducers({
        me,
        events,
        news,
        loading,
        toasting,
        dialog,
        users,
    });
}
