import { handleActions } from 'redux-actions';
import {
    GET_NEWS,
    UPDATE_NEWS,
    UPDATE_POST
} from '../action-types';
import { BUNDLE_STATUS } from '../../config';

const initialState = {
    newsfeed: {
        data: [],
        offsetId: null,
    },
    post: null,
    activeBundleStatus: BUNDLE_STATUS.SUCCESS
};

export default handleActions(
    {
        [GET_NEWS]: (state, action) => {
            return {
                ...state,
                newsfeed: action.payload,
            };
        },
        [UPDATE_POST]: (state, action) => {
            return {
                ...state,
                post: action.payload,
            };
        },
        [UPDATE_NEWS]: (state, action) => {
            let newState = state.newsfeed.data.map((value, index, array) => {
                if (value._id == action.payload.data._id) {
                    console.log("You are here")
                    return action.payload.data
                }
                return value
            })
            return {
                ...state,
                newsfeed: {
                    ...state.newsfeed,
                    data: newState
                }
            }
        },
    },
    initialState,
);
