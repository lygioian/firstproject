import { handleActions } from 'redux-actions';
import {
    GET_USERS,
    UPDATE_USER,
    UPDATE_USER_REPORT,
    UPDATE_STAFF_REPORT
} from '../action-types';

const initialState = {
    list: {
        data: [],
        offsetId: null,
    },
    user: null,
    userReport: null,
    staffReport: null
};

export default handleActions(
    {
        [GET_USERS]: (state, action) => ({
            ...state,
            list: action.payload,
        }),
        [UPDATE_USER]: (state, action) => ({
            ...state,
            user: action.payload,
        }),
        [UPDATE_USER_REPORT]: (state, action) => ({
            ...state,
            userReport: action.payload,
        }),
        [UPDATE_STAFF_REPORT]: (state, action) => ({
            ...state,
            staffReport: action.payload,
        }),
    },
    initialState
);
