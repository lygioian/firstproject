import { handleActions } from 'redux-actions';
import {
    GET_NEWEST_EVENTS,
    GET_MORE_EVENTS,
    GET_MY_EVENTS,
    UPDATE_EVENTS,
    UPDATE_EVENT
} from '../action-types';
import { BUNDLE_STATUS } from '../../config';
import _ from "lodash";

const initialState = {
    newsfeed: {
        data: [],
        offsetId: null,
    },
    myEvents: {
        data: [],
        offsetId: null,
    },
    event: null,
    activeBundleStatus: BUNDLE_STATUS.SUCCESS
};

export default handleActions(
    {
        [GET_NEWEST_EVENTS]: (state, action) => {
            return {
                ...state,
                newsfeed: action.payload,
            };
        },
        [UPDATE_EVENT]: (state, action) => {
            return {
                ...state,
                event: action.payload,
            };
        },
        [GET_MORE_EVENTS]: (state, action) => {
            console.log("State: ", state)
            return {
                ...state,
                newsfeed: {
                    ...state.newsfeed.data,
                    ...action.payload
                }
            }
        },
        [UPDATE_EVENTS]: (state, action) => {
            let newState = state.myEvents.data.map((value, index, array) => {
                if (value._id == action.payload.data._id) {
                    console.log("You are here")
                    return action.payload.data
                }
                return value
            })
            return {
                ...state,
                myEvents: {
                    ...state.myEvents,
                    data: newState
                }
            }
        },
        [GET_MY_EVENTS]: (state, action) => {
            return {
                ...state,
                myEvents: action.payload,
            };
        },
    },
    initialState,
);
