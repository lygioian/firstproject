// import { handleActions } from 'redux-actions';
// import {
//     GET_USERS,
//     GET_USERS_SUCCEEDED ,
// } from '../action-types';

// const initialState = {
//     token: '',
//     role: '',
//     isLoggedIn: false,
//     information: {
        
//     },
//     latest: {
//         data: [],
//         offsetId: null,
//     },
//     saved: {
//         data: [],
//     },
//     following: [],
//     followers: [],
// };

// export default handleActions(
//     {
//         [SET_TOKEN]: (state, action) => ({
//             ...state,
//             ...action.payload,
//             isLoggedIn: true,
//         }),
//         [LOG_OUT]: (state, action) => ({
//             ...state,
//             isLoggedIn: false
//         }),
//         [RESET_TOKEN]: (state, action) => initialState,
//         [GET_PROFILE_SUCCEEDED]: (state, action) => ({
//                 ...state,
//                 information: action.payload,
//             }
//         ),
//         [GET_MY_EVENTS_LATEST_SUCCEEDED]: (state, action) => ({
//             ...state,
//             latest: action.payload,
//         }),
//         [GET_MY_EVENTS_SAVED_SUCCEEDED]: (state, action) => ({
//             ...state,
//             saved: { data: action.payload },
//         }),
//         [GET_MY_FOLLOWING_SUCCEEDED]: (state, action) => ({
//             ...state,
//             following: action.payload,
//         }),
//         [GET_MY_FOLLOWERS_SUCCEEDED]: (state, action) => ({
//             ...state,
//             followers: action.payload,
//         }),
//     },
//     initialState
// );
