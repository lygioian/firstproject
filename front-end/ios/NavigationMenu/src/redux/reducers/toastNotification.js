import {handleActions} from 'redux-actions';
import {SHOW_NOTIFICATION, HIDE_NOTIFICATION} from '../action-types'

const initialState = {
    notification: "",
    isShow: false,
    positionIndicator: "bottom",
    type: "success",
}

export default handleActions(
    {
        [SHOW_NOTIFICATION]: (state, action) => ({
            ...initialState,
            notification: action.payload.notification,
            positionIndicator: action.payload.position,
            type: action.payload.type,
            isShow: true
        }),
        [HIDE_NOTIFICATION]: (state, action) => ({
            ...state,
            isShow: false,
        })
    },
    initialState
);