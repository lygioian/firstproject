import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import createRootReducer from './reducers';

export let store;
export function initializeStore(initialState) {
    store = createStore(
        createRootReducer(),
        initialState,
        composeWithDevTools(
            applyMiddleware(thunk)
        )
    );

    return store;
}
