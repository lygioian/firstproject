import { bindActionCreators } from 'redux';
import { createAction } from 'redux-actions';
import { createActionThunk } from 'redux-thunk-actions';
import _ from 'lodash';

import AsyncStorage from '@react-native-community/async-storage';
import { axios } from '../../lib/custom-axios';
import ImagePicker from "react-native-image-picker";
import ImageEditor from "@react-native-community/image-editor";
import { MeAPI } from '../../api/me';
import { UsersAPI } from '@api';

import { EVENTS_PAGE_SIZE, AUTH_URL } from '../../config';

import {
    GET_USERS,
    GET_USERS_SUCCEEDED,
    UPDATE_USER_REPORT,
    UPDATE_STAFF_REPORT
} from '../action-types';
// import { FacebookSDK } from '../../lib/wrapper/facebook';
// import { setCookie, removeCookie } from '../../lib/storage';
// import { BundlesAPI } from '../../api/bundles';

const fetchUsers = createAction(
    GET_USERS, (data, offsetId) => ({
        data,
        offsetId,
    })
)

const fetchUserReport = createAction(
    UPDATE_USER_REPORT, data => ({
        ...data
    })
)

const fetchStaffReport = createAction(
    UPDATE_STAFF_REPORT, data => ({
        ...data
    })
)

const getUserAPI = {
    limit: 8,
    keyword: null,
    type: 0,
}

export const getUsers = (parms, isReload) => async (dispatch, getState) => {
    try {
        let res;
        parms = {
            ...getUserAPI,
            ...parms,
        }
        console.log(parms)
        res = await UsersAPI.getUsers(parms.limit, parms.keyword, parms.type);
        console.log("res: ", res)
        await dispatch(fetchUsers(res.data));
        return Promise.resolve({data: res.data});
    } catch(err) {
        console.log("Error in getUser");
        return Promise.reject(err);
    }
};

export const getUserReport = data => async(dispatch, getState) => {
    try {
        await dispatch(fetchUserReport(data))
    } catch(err) {
        console.log('Error in getReport')
        return Promise.reject(err)
    }  
}

export const getStaffReport = data => async(dispatch, getState) => {
    try {
        await dispatch(fetchStaffReport(data))
    } catch(err) {
        console.log('Error in getReport')
        return Promise.reject(err)
    }  
}

export const deleteUserDataAction = (token, data) => async (dispatch, getState) => {
    try {
        await AsyncStorage.removeItem(TOKEN);
        dispatch(logout());
        Promise.resolve();
    } catch (error) {
        Promise.reject(error);
    }
}

export const usersActions = {
    deleteUserDataAction,
    getUsers,
    getUserReport,
    getStaffReport
};

export function bindUsersActions(currentActions, dispatch) {
    return {
        ...currentActions,
        usersActions: bindActionCreators(usersActions, dispatch),
    };
}
