import { bindActionCreators } from 'redux';
import { createAction } from 'redux-actions';
import { createActionThunk } from 'redux-thunk-actions';

import { NewsAPI } from '@api';
// import { async } from 'rxjs/internal/scheduler/async';

import { NEWS_PAGE_SIZE, BUNDLE_STATU } from '../../config';

import {
    GET_NEWS,
    GET_NEWEST_NEWS,
    UPDATE_NEWS,
    UPDATE_POST
} from '../action-types';
import { MeAPI } from '../../api/me';

const fetchNews = createAction(
    GET_NEWS, (data, offsetId, isReload) => ({
        data,
        offsetId,
        isReload
    })
)

const fetchPost = createAction(
    UPDATE_POST, data => ({
        ...data
    })
)

const updateNewsfeed = createAction(
    UPDATE_NEWS, (data) => ({
        data,
    })
)

export const createNews = (data) => async (dispatch, getState) => {
    try {
        const res = await NewsAPI.create(data);
        return Promise.resolve(res.data);
    } catch(err) {
        console.log(err);
        return Promise.reject(err);
    }
};

export const updateNews = (postId, data, isP) => async (dispatch, getState) => {
    try {
        const res = await NewsAPI.update(postId, data, isP);
        await dispatch(fetchPost(res.data));
        return Promise.resolve(res.data);
    } catch(err) {
        console.log(err);
        return Promise.reject(err);
    }
};

export const deleteNews = (newsId) => async (dispatch, getState) => {
    try {
        console.log(newsId);
        const res = await NewsAPI.deleteNews(newsId);
        return Promise.resolve(res.data);
    } catch(err) {
        console.log(err);
        return Promise.reject(err);
    }
    
};

export const getNews = () => async (dispatch, getState) => {
    try {
        let res;
        res = await NewsAPI.getAll();
        await dispatch(fetchNews(res.data))
        return Promise.resolve({data: res.data});
    } catch(err) {
        console.log("Error in getEvents");
        return Promise.reject(err);
    }
};

export const getNewsById = eventId => async (dispatch, getState) => {
    try {
        const res = await NewsAPI.getById(eventId);
        console.log("res", res.data)
        await dispatch(fetchPost(res.data))
        return Promise.resolve({data: res.data});
    } catch(err) {
        console.log("Error in getEvents");
        return Promise.reject(err);
    }
};

export const newsActions = {
    createNews,
    getNews,
    getNewsById,
    updateNews,
    // getNewsByUserId,
    deleteNews,
};

export function bindNewsActions(currentActions, dispatch) {
    return {
        ...currentActions,
        newsActions: bindActionCreators(newsActions, dispatch),
    };
}
