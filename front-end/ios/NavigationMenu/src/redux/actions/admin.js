import { bindActionCreators } from 'redux';
import { createAction } from 'redux-actions';
import { createActionThunk } from 'redux-thunk-actions';
import _ from 'lodash';

import AsyncStorage from '@react-native-community/async-storage';
import { axios } from '../../lib/custom-axios';
import ImagePicker from "react-native-image-picker";
import ImageEditor from "@react-native-community/image-editor";
import { MeAPI } from '@api';

import { EVENTS_PAGE_SIZE, AUTH_URL } from '../../config';

import {
    TOKEN,
    SET_TOKEN,
    RESET_TOKEN,
    GET_PROFILE,
    GET_PROFILE_SUCCEEDED,
    GET_MY_EVENTS_LATEST,
    GET_MY_EVENTS_SAVED,
    GET_MY_FOLLOWERS,
    GET_MY_FOLLOWING,
    SET_CREATING_BUNDLE_STATE,
    LOG_OUT
} from '../action-types';
// import { FacebookSDK } from '../../lib/wrapper/facebook';
// import { setCookie, removeCookie } from '../../lib/storage';
// import { BundlesAPI } from '../../api/bundles';

const logout = createAction(LOG_OUT, () => ({
}))

const storeToken = createAction(SET_TOKEN, (token) => ({
    token
}))
const storeInfo = createAction(GET_PROFILE_SUCCEEDED, (data) => ({
    ...data
}))

export const getUserById = (userId) => async (dispatch, getState) => {
    return new Promise(async (resolve, reject) => {
        try {
            const {data}  = await MeAPI.getUserById(userId);
            if(data.error){
                reject(data);
            }
            resolve(data);
        } catch (err) {
            reject(err.response);
        }
    });
};

export const getUserByKeyword = (keyword) => async (dispatch, getState) => {
    return new Promise(async (resolve, reject) => {
        try {
            const {data}  = await MeAPI.getUserByKeyword(keyword);
            console.log(data)
            if(data.error){
                reject(data);
            }
            resolve({data: data});
        } catch (err) {
            reject(err.response);
        }
    });
};

export const deleteUserDataAction = (token, data) => async (dispatch, getState) => {
    try {
        await AsyncStorage.removeItem(TOKEN);
        dispatch(logout());
        Promise.resolve();
    } catch (error) {
        Promise.reject(error);
    }
}

// export const logout = () => async (dispatch, getState) => {
//     dispatch(resetToken());
// };

export const meActions = {
    getUserById,
    getUserByKeyword,
    deleteUserDataAction,
};

export function bindMeActions(currentActions, dispatch) {
    return {
        ...currentActions,
        meActions: bindActionCreators(meActions, dispatch),
    };
}
