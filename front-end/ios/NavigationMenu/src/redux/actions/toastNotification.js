import { bindActionCreators } from "redux";
import {createAction} from 'redux-actions';
import {SHOW_NOTIFICATION, HIDE_NOTIFICATION} from '../action-types'

export const showNotification = createAction(SHOW_NOTIFICATION, (notification, position, type) => ({
    notification: notification,
    position: position,
    type: type
}));

export const hideNotification =  createAction(HIDE_NOTIFICATION)

export function bindToastNotificationActions(currentActions, dispatch) {
    return {
        ...currentActions,
        toastingActions: bindActionCreators(
            {
                show : showNotification,
                hide: hideNotification,
            },
            dispatch
        ),
    }
}