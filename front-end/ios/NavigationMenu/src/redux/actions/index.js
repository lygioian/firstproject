import { bindAdminActions } from './admin'
import { bindDialogActions } from './dialog'
import { bindEventsActions } from './events'
import { bindLoadingActions } from './loading'
import { bindMeActions } from './me'
import { bindNewsActions } from './news'
import { bindToastNotificationActions } from './toastNotification'
import { bindUsersActions } from './user'

export {
    bindAdminActions,
    bindDialogActions,
    bindEventsActions,
    bindLoadingActions,
    bindMeActions,
    bindNewsActions,
    bindToastNotificationActions,
    bindUsersActions
}
