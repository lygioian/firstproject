import { bindActionCreators } from "redux";
import {createAction} from 'redux-actions';
import {SHOW_DIALOG, HIDE_DIALOG} from '../action-types'

export const showDialog = createAction(SHOW_DIALOG, (title, description, button1, button2, onButton1 = () => {}, onButton2 = () => {}, needInput=false) => ({
    title: title,
    description: description,
    button1: button1,
    button2: button2,
    onButton1: onButton1,
    onButton2: onButton2,
    needInput: needInput,
}));

export const hideDialog =  createAction(HIDE_DIALOG);

export function bindDialogActions(currentActions, dispatch) {
    return {
        ...currentActions,
        dialogActions: bindActionCreators(
            {
                show : showDialog,
                hide: hideDialog,
            },
            dispatch
        ),
    }
}