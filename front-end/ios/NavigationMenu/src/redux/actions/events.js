import { bindActionCreators } from 'redux';
import { createAction } from 'redux-actions';
import { createActionThunk } from 'redux-thunk-actions';

import { EventsAPI } from '@api';
// import { async } from 'rxjs/internal/scheduler/async';

import { EVENTS_PAGE_SIZE, BUNDLE_STATU } from '../../config';

import {
    GET_EVENT,
    GET_NEWEST_EVENTS,
    GET_MORE_EVENTS,
    GET_MY_EVENTS,
    UPDATE_EVENTS,
    UPDATE_EVENT
} from '../action-types';

const fetchNewEvents = createAction(
    GET_NEWEST_EVENTS, (data) => ({
        data,
    })
)

const fetchEvent = createAction(
    UPDATE_EVENT, (data) => ({
        ...data,
    })
)

const fetchMyEvents = createAction(
    GET_MY_EVENTS, (data) => ({
        data,
    })
)

const fetchMoreEvents = createAction(
    GET_MORE_EVENTS, (data) => ({
        data,
    })
)

const updateEvents = createAction(
    UPDATE_EVENTS, (data) => ({
        data,
    })
)

export const createEvent = (data) => async (dispatch, getState) => {
    try {
        const res = await EventsAPI.create(data);
        return Promise.resolve(res.data);
    } catch(err) {
        return Promise.reject(err);
    }
};

let addStaffAPI = {
    eventId: null,
    userId: null,
    role: "Staff"
}

export const addStaff = (eventId, userId, role="Staff") => async (dispatch, getState) => {
    try {
        const res = await EventsAPI.addStaff(eventId, userId, role);
        await dispatch(fetchEvent(res.data))
        return Promise.resolve(res.data);
    } catch(err) {
        console.log(err);
        return Promise.reject(err);
    }
};

export const addLeader = (eventId, userId, role="Leader") => async (dispatch, getState) => {
    try {
        const res = await EventsAPI.addStaff(eventId, userId, role);
        await dispatch(fetchEvent(res.data))
        return Promise.resolve(res.data);
    } catch(err) {
        console.log(err);
        return Promise.reject(err);
    }
};

export const addRegister = (eventId, userId) => async (dispatch, getState) => {
    try {
        const res = await EventsAPI.addRegister(eventId, userId);
        await dispatch(fetchEvent(res.data))
        return Promise.resolve(res.data);
    } catch(err) {
        console.log(err);
        return Promise.reject(err);
    }
};

export const deleteStaff = (eventId, userId, role="Staff") => async (dispatch, getState) => {
    try {
        const res = await EventsAPI.deleteStaff(eventId, userId, role);
        await dispatch(fetchEvent(res.data))
        return Promise.resolve(res.data);
    } catch(err) {
        console.log(err);
        return Promise.reject(err);
    }
};

export const deleteLeader = (eventId, userId, role="Leader") => async (dispatch, getState) => {
    try {
        const res = await EventsAPI.deleteStaff(eventId, userId, role);
        await dispatch(fetchEvent(res.data))
        return Promise.resolve(res.data);
    } catch(err) {
        console.log(err);
        return Promise.reject(err);
    }
};

export const deleteRegister = (eventId, userId) => async (dispatch, getState) => {
    try {
        const res = await EventsAPI.deleteRegister(eventId, userId);
        await dispatch(fetchEvent(res.data))
        return Promise.resolve(res.data);
    } catch(err) {
        console.log(err);
        return Promise.reject(err);
    }
};

export const startEvent = (eventId) => async (dispatch, getState) => {
    try {
        const res = await EventsAPI.startEvent(eventId);
        await dispatch(fetchEvent(res.data))
        return Promise.resolve(res.data);
    } catch(err) {
        console.log(err);
        return Promise.reject(err);
    } 
};

export const finishEvent = (eventId) => async (dispatch, getState) => {
    try {
        const res = await EventsAPI.finishEvent(eventId);
        await dispatch(fetchEvent(res.data))
        return Promise.resolve(res.data);
    } catch(err) {
        console.log(err);
        return Promise.reject(err);
    } 
};

export const startFirstCheck = (eventId) => async (dispatch, getState) => {
    try {
        const res = await EventsAPI.startFirstCheck(eventId);
        await dispatch(fetchEvent(res.data))
        return Promise.resolve(res.data);
    } catch(err) {
        console.log(err);
        return Promise.reject(err);
    } 
};

export const finishFirstCheck = (eventId) => async (dispatch, getState) => {
    try {
        const res = await EventsAPI.finishFirstCheck(eventId);
        await dispatch(fetchEvent(res.data))
        return Promise.resolve(res.data);
    } catch(err) {
        console.log(err);
        return Promise.reject(err);
    } 
};

export const startSecondCheck = (eventId) => async (dispatch, getState) => {
    try {
        const res = await EventsAPI.startSecondCheck(eventId);
        await dispatch(fetchEvent(res.data))
        return Promise.resolve(res.data);
    } catch(err) {
        console.log(err);
        return Promise.reject(err);
    } 
};

export const finishSecondCheck = (eventId) => async (dispatch, getState) => {
    try {
        const res = await EventsAPI.finishSecondCheck(eventId);
        await dispatch(fetchEvent(res.data))
        return Promise.resolve(res.data);
    } catch(err) {
        console.log(err);
        return Promise.reject(err);
    } 
};

export const updateEvent = (data, eventId, isP = false) => async (dispatch, getState) => {
    try {
        const res = await EventsAPI.update(data, eventId, isP);
        await dispatch(fetchEvent(res.data))
        return Promise.resolve(res.data);
    } catch(err) {
        console.log(err);
        return Promise.reject(err);
    }
    
};

let eventsAPI = {
    offsetId: 0,
    limit: 8,
    eventId: null,
    type: 0,
    constraint: 0,
    keyword: ''
}

export const getEvents = (parms, isReload) => async (dispatch, getState) => {
    try {
        const { offsetId, data } = getState().events.newsfeed;
        let res;
        parms = {
            ...eventsAPI,
            ...parms,
        }
        if (!parms.eventId) {
            res = await EventsAPI.getEvents(parms.offsetId, parms.limit, parms.type, parms.constraint);
            if (parms.constraint == 1) {
                console.log("Reach actions constraint = 1")
                await dispatch(fetchMyEvents(res.data));
            } else {
                console.log("isReload: ", isReload)
                // !isReload ? await dispatch(fetchMoreEvents(res.data)) : await dispatch(fetchNewEvents(res.data))
                await dispatch(fetchNewEvents(res.data))
            }
        } else {
            res = await EventsAPI.getEventId(parms.eventId);
            await dispatch(updateEvent(res.data))
        }

        return Promise.resolve({data: res.data, offsetId});
    } catch(err) {
        console.log("Error in getEvents");
        return Promise.reject(err);
    }
};

export const getEventById = (eventId) => async(dispatch, getState) => {
    try {
        const res = await EventsAPI.getEventById(eventId)
        await dispatch(fetchEvent(res.data))
        return Promise.resolve({data: res.data})
    } catch (err) {
        console.log("Error in getEventById")
        return Promise.reject(err)
    }
}

export const deleteEvent = (eventId) => async (dispatch, getState) => {
    try {
        const res = await EventsAPI.deleteEvent(eventId);
        return Promise.resolve(res.data);
    } catch(err) {
        console.log(err);
        return Promise.reject(err);
    }
    
};

const defaultRequest = {
    limit: 8,
    offset: 0,
    keyword: null
}

export const eventsActions = {
    createEvent,
    updateEvent,
    addStaff,
    addLeader,
    deleteStaff,
    deleteEvent,
    addRegister,
    deleteRegister,
    getEvents,
    deleteLeader,
    startEvent,
    finishEvent,
    getEventById,
    startFirstCheck,
    finishFirstCheck,
    startSecondCheck,
    finishSecondCheck
};

export function bindEventsActions(currentActions, dispatch) {
    return {
        ...currentActions,
        eventsActions: bindActionCreators(eventsActions, dispatch),
    };
}
