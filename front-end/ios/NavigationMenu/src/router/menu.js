import React from "react";
import { TouchableWithoutFeedback, ScrollView, StyleSheet, Image } from "react-native";
import { Block, Text, theme } from "galio-framework";
import { useSafeArea } from "react-native-safe-area-context";

import DrawerCustomItem from '../components/Drawer';
import { Images, materialTheme } from "../constants/";
import { connect } from 'react-redux'
import {channingActions} from '../lib/helper'
import {bindMeActions} from '../redux/actions/me'
import {bindEventsActions} from '../redux/actions/events'
import {bindLoadingActions} from '../redux/actions/loading'
import {API_URL} from '../config'
import { CommonActions } from '@react-navigation/native';

function CustomDrawerContent({
  drawerPosition,
  navigation,
  profile,
  focused,
  state,
  me,
  ...rest
}) {
  const insets = useSafeArea();
  const screens = [
    {
        Title: "Event Dashboard",
        Name: "Event"
    },
    {
        Title: "News Dashboard",
        Name: "News"
    },
    {
        Title: "Account",
        Name: "Profile"
    },
    {
      Title: "Setting",
      Name: "Setting",
    }
  ];
  if (me.information == undefined)
    return null;
  return (
    <Block
      style={styles.container}
      forceInset={{ top: "always", horizontal: "never" }}
    >
      <Block flex={0.25} style={styles.header}>
        <TouchableWithoutFeedback
          onPress={() => navigation.navigate("Profile")}
        >
          <Block style={styles.profile} row >
            <Image
 
            source={{ uri: API_URL + me.information.media.thumbnail }} 
            style={styles.avatar} />
            <Block middle style ={styles.name}>
                <Text h5 color={"white"}>
                {me.information.name}
                </Text>
            </Block>
          </Block>
        </TouchableWithoutFeedback>
        <Block row>
          <Block middle style={styles.pro}>
            <Text size={14} color="white">
              OISP
            </Text>
          </Block>
          <Text size={16} muted style={styles.seller}>
            {me.information.email}
          </Text>
        </Block>
      </Block>
      <Block flex style={{ paddingLeft: 7, paddingRight: 14 }}>
        <ScrollView
          contentContainerStyle={[
            {
              paddingTop: insets.top * 0.4,
              paddingLeft: drawerPosition === "left" ? insets.left : 0,
              paddingRight: drawerPosition === "right" ? insets.right : 0
            }
          ]}
          showsVerticalScrollIndicator={false}
        >
          {screens.map((item, index) => {
            return (
              <DrawerCustomItem
                role = {me.information.role}
                title={item.Title}
                key={index}
                navigation={navigation}
                focused={state.index === index ? true : false}
                onPress = {() => {navigation.navigate(item.Name)}}
              />
            );
          })}
        </ScrollView>
      </Block>
    </Block>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    backgroundColor: '#4B1958',
    paddingHorizontal: 28,
    paddingBottom: theme.SIZES.BASE,
    paddingTop: theme.SIZES.BASE * 2,
    justifyContent: 'center',
  },
  footer: {
    paddingHorizontal: 28,
    justifyContent: 'flex-end'
  },
  profile: {
    marginBottom: theme.SIZES.BASE / 2,
  },
  avatar: {
    height: 60,
    width: 60,
    borderRadius: 30,
    marginBottom: theme.SIZES.BASE,
  },
  pro: {
    backgroundColor: materialTheme.COLORS.LABEL,
    paddingHorizontal: 6,
    marginRight: 8,
    borderRadius: 4,
    height: 19,
    width: 38*4/3,
  },
  seller: {
    marginRight: 18,
  },
  name: {
      marginLeft: 10
  }
});

export default connect(
    state => ({me: state.me}),
    dispatch => channingActions({}, dispatch, bindMeActions, bindEventsActions, bindLoadingActions)
  )(CustomDrawerContent)