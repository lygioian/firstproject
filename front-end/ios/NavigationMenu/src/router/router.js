import React from "react";
import {Dimensions } from 'react-native';
import { connect } from 'react-redux'
import {channingActions} from '../lib/helper'
import {bindMeActions} from '../redux/actions/me'
import {bindEventsActions} from '../redux/actions/events'
import { bindLoadingActions } from '../redux/actions/loading';
import { bindToastNotificationActions } from '../redux/actions/toastNotification';

import {
  Login,
  Setting,
  Event,
  EventDetail,
  EditEvent,
  CreateEvent,
  ViewStaff,
  ViewRegister,
  AddRegister,
  AddStaff,
  ConfirmEvent,
  Profile,
  ProfileView,
  ChangePassword,
  EditProfile,
  News,
  NewsDetail,
  CreateNews,
  EditNews,
  MyNews,
  FirstCheck,
  SecondCheck,
  RegistersReport,
  StaffsReport,
  RegisterReport,
  StaffReport
} from "../screens";

import { SafeAreaProvider } from 'react-native-safe-area-context';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from "@react-navigation/drawer";
import CustomDrawerContent from './menu';
import { Images, materialTheme } from "../constants/";
const { width } = Dimensions.get("screen");

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

function NewsStack() {
  return (
    <Stack.Navigator
    // headerMode="none"
    >
      <Stack.Screen name="News" component={News} />
      <Stack.Screen name="NewsDetail" options={{title: `News Detail`}} component={NewsDetail} />
      <Stack.Screen name="ProfileView" options={{title: `View Profile`}} component={ProfileView} />
      <Stack.Screen name="MyNews" options={{title: `News List`}} component={MyNews} />
      <Stack.Screen name="EditNews" options={{title: `Edit News`}} component={EditNews} />
      <Stack.Screen name="CreateNews" options={{title: `Create News`}} component={CreateNews} />
      
    </Stack.Navigator>
  );
}

function EventStack() {
  return (
    <Stack.Navigator
    // headerMode="none"
    >
      <Stack.Screen name="Event" component={Event} />
      <Stack.Screen name="EventDetail" options={{title: `Event Detail`}} component={EventDetail} />
      <Stack.Screen name="EditEvent" options={{title: 'Edit Event'}} component={EditEvent} />
      <Stack.Screen name="AddStaff" options={{title: 'Add Staffs'}} component={AddStaff} />
      <Stack.Screen name="ProfileView" options={{title: 'View Profile'}} component={ProfileView} />
      <Stack.Screen name="CreateEvent" options={{title: 'Create Event'}} component={CreateEvent} />
      <Stack.Screen name="ViewStaff" options={{title: `Staff's List`}} component={ViewStaff} />
      <Stack.Screen name="ViewRegister" options={{title: `Register's List`}} component={ViewRegister} />
      <Stack.Screen name="AddRegister" options={{title: `Add Registers`}} component={AddRegister} />
      <Stack.Screen name="ConfirmEvent" options={{title: `Report`}} component={ConfirmEvent} />
      <Stack.Screen name="FirstCheck" options={{title: `First Attendance Check`}} component={FirstCheck} />
      <Stack.Screen name="SecondCheck" options={{title: `First Attendance Check`}} component={SecondCheck} />
      <Stack.Screen name="RegistersReport" options={{title: `Register's Report`}} component={RegistersReport} />
      <Stack.Screen name="StaffsReport" options={{title: `Staff Report`}} component={StaffsReport} />
      <Stack.Screen name="RegisterReport" options={{title: `Register's Report`}} component={RegisterReport} />
      <Stack.Screen name="StaffReport" options={{title: `Staff Report`}} component={StaffReport} />
    </Stack.Navigator>
  );
}

function AccountStack() {
  return (
    <Stack.Navigator
    // headerMode="none"
    >
      <Stack.Screen name="Profile" component={Profile} />
      <Stack.Screen name="ProfileView" options={{title: `View Profile`}} component={ProfileView} />
      <Stack.Screen name="ChangePassword" options={{title: `Change Password`}} component={ChangePassword} />
      <Stack.Screen name="EditProfile" options={{title: `Edit Profile`}} component={EditProfile} />
    </Stack.Navigator>
  );
}

function SettingStack() {
  return (
    <Stack.Navigator
    // headerMode="none"
    >
      <Stack.Screen name="Setting" component={Setting} />

    </Stack.Navigator>
  );
}

function AppStack(props) {
  return (
    <Drawer.Navigator
      headerMode="none"
      style={{ flex: 1 }}
      drawerContent={props => (
        <CustomDrawerContent {...props} />
      )}
      drawerStyle={{
        backgroundColor: "white",
        width: width * 0.8
      }}
      drawerContentOptions={{
        activeTintColor: "white",
        inactiveTintColor: "#000",
        activeBackgroundColor: materialTheme.COLORS.ACTIVE,
        inactiveBackgroundColor: "transparent",
        itemStyle: {
          width: width * 0.74,
          paddingHorizontal: 12,
          // paddingVertical: 4,
          justifyContent: "center",
          alignContent: "center",
          // alignItems: 'center',
          overflow: "hidden"
        },
        labelStyle: {
          fontSize: 18,
          fontWeight: "normal"
        }
      }}
      initialRouteName="Event"
    >
      <Drawer.Screen
        name="Event"
        component={EventStack}
      />
      <Drawer.Screen
        name="News"
        component={NewsStack}
      />
      <Drawer.Screen
        name="Profile"
        component={AccountStack}
      />
      <Drawer.Screen
        name="Setting"
        component={SettingStack}
      />
    </Drawer.Navigator>
  );
}

function LoginStack() {
  return (
    <Stack.Navigator
    headerMode="none"
    >
      <Stack.Screen name="Login" component={Login} headerMode = "none" />
    </Stack.Navigator>
  );
}

// export default createAppContainer(App);
class App extends React.Component {
  

  componentDidMount() {
    console.log("DidMount")
    const {meActions, loadingActions, toastingActions} = this.props;
    loadingActions.show();
    // console.log(this.props);
    meActions.loadLocal().then(()=>{
      loadingActions.hide();
      this.setState({email: '', password: ''});
      // toastingActions.show("Log in  successfully", "bottom", "success")
      // this.props.navigation.navigate('MainTab');

      console.log("Load Token Success");
    }).catch(err =>{
      console.log(err);
      loadingActions.hide();
      console.log("Local Data not exist")
    })
  }

  render() {
  const {isLoggedIn} = this.props.me;
  return (
  <SafeAreaProvider>
    <NavigationContainer>
      <Stack.Navigator mode="card" headerMode="none"
      screenOptions = {{
        // Default config for all screens
        headerMode: "none",
        // initialRouteName: "LoginStack",
        // transitionConfig: noTransitionConfig,
        navigationOptions: ({ navigation }) => ({
          color: "black"
        })
      }}
      
      >
        {isLoggedIn === false ? (
        <Stack.Screen 
          name="LoginStack" 
          component={LoginStack}   
          headerMode = "none"      
          options={{ headerShown: false }}
          option={{
            headerTransparent: true
          }}

        />):(
        
        <Stack.Screen
          name="MainTab"
          component= {AppStack}
          options={{ headerShown: false }}
        />)}
      </Stack.Navigator>
    </NavigationContainer>
  </SafeAreaProvider>);
  }
}

export default connect(
  state => ({me: state.me}),
  dispatch => channingActions({}, dispatch, bindMeActions, bindEventsActions, bindLoadingActions, bindToastNotificationActions)
)(App)