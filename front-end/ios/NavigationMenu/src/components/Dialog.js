import React from 'react'
import { connect } from 'react-redux'
import {channingActions} from '../lib/helper'
import {  View } from "react-native";
import Dialog from "react-native-dialog";
import { bindDialogActions } from '../redux/actions/dialog';
 
class DialogScreen extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            input: ''
        }
    } 
     
    handleButton1 = () => {
        const { dialogActions } = this.props;
        const { onButton1 } = this.props.dialog;
        dialogActions.hide();
        onButton1(this.state.input);
    };

    handleButton2 = () => {
        const { dialogActions } = this.props;
        const { onButton2 } = this.props.dialog;
        dialogActions.hide();
        onButton2();
    };
 
    render() {
        console.log("Props in Dialog: ", this.props);
        const { dialogActions } = this.props;
        const reactNativeModalProps = {
            onBackdropPress: dialogActions.hide,
        };
        const { isShow, title, description, button1, button2, needInput } = this.props.dialog;
        return (
            <View>
                <Dialog.Container visible={isShow} {...reactNativeModalProps}>
                    <Dialog.Title> {title} </Dialog.Title>
                    <Dialog.Description>
                        {description}
                    </Dialog.Description>
                    {needInput ? (
                        <Dialog.Input style={{borderColor: 'black', borderWidth: 1}} onChangeText={value => this.setState({input: value})} />
                    ) : null}
                    <Dialog.Button label={button2} onPress={() => this.handleButton2()} />
                    <Dialog.Button label={button1} onPress={() => this.handleButton1()} />
                </Dialog.Container>
            </View>
        );
    }
}

export default connect(
    state => ({dialog: state.dialog}),
    dispatch => channingActions({}, dispatch, bindDialogActions)
  )(DialogScreen)