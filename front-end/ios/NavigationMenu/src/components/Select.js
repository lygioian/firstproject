import React from 'react';
import { StyleSheet } from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';
import { Block, Text, Icon } from 'galio-framework';
import theme from '../constants/Theme';

export default class DropDown extends React.Component {
  state = {
    value: 1,
  }

  handleOnSelect = (index, value) => {
    const { onSelect } = this.props;

    this.setState({ value: value });
    onSelect && onSelect(index, value);
  }

  render() {
    const { handleOnSelect, style, value, ...props } = this.props;
    return (
      <ModalDropdown
        style={[styles.qty, style]}
        onSelect={handleOnSelect}
        dropdownStyle={styles.dropdown}
        dropdownTextStyle={{paddingLeft:16, fontSize:12}, styles.socialTextButtons}
        {...props}>
        <Block row middle space="between">
          <Text size={12} style={styles.socialTextButtons}>{value}</Text>
          <Icon name="angle-down" family="font-awesome" size={15} style={{padding: 5}} />
        </Block>
      </ModalDropdown>
    )
  }
}

const styles = StyleSheet.create({
  qty: {
    // width: 120,
    backgroundColor: "#fff",
    paddingHorizontal: 16,
    paddingTop: 10,
    paddingBottom:9.5,
    borderRadius: 3,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 4,
    shadowOpacity: 1,
  },
  dropdown: {
    marginTop: 8,
    marginLeft: -16,
    // width: 120,
    
  },
  socialTextButtons: {
    color: theme.COLORS.PRIMARY,
    fontWeight: "800",
    fontSize: 14
  },
});
