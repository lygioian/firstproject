import React from 'react'
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Dimensions
} from 'react-native'
import Text from "./Text"
import Icon from "react-native-vector-icons/Ionicons";

const {width, height} = Dimensions.get('window')

export class Notification extends React.Component {
    selectIcon = (type) => {
        switch (type) {
            case "success":
                return "md-checkmark-circle"
                break;
            case "error":
                return "md-warning"
                break;
            case "warning":
                return "md-alert"
                break;
            default:
                return "md-information-circle-outline" 
        }
    }

    render() {
        const SIZE = width*0.07;
        return (
            <View style={{
                flexDirection: "row",
                flex:1,
            }}>
                <View style={{justifyContent:'center', alignItems:'center', marginRight: SIZE/2, marginLeft: SIZE/2}}>
                    <Icon
                        name= {this.selectIcon(this.props.type)}
                        backgroundColor="rgba(255,255,255,0)"
                        color="white"
                        size={SIZE + 8}
                    />
                </View>
                <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={{color: "white", fontSize: 20, fontWeight: "bold" }} numberOfLines={1}>{this.props.notification}</Text>
                </View>
                <View style={{justifyContent:'center', alignItems:'center', marginRight: SIZE/2, marginLeft: SIZE/2}}>
                    <TouchableOpacity style={{width: SIZE + 4, height: SIZE + 4, borderRadius: SIZE/2 + 2, borderColor: 'white', borderWidth: 2, justifyContent:'center', alignItems:'center'}} onPress={() => this.props.action.hide()}>
                        <Icon
                        name="ios-close"
                        backgroundColor="rgba(255,255,255,0)"
                        color="white"
                        size={SIZE}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}