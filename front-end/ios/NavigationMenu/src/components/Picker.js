import React from "react";
import Text from './Text'
import {
    View,
    StyleSheet,
} from "react-native";
import RNPickerSelect from "react-native-picker-select";

const borderColor = '#ddd';
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 15,
    borderBottomWidth: 0.5,
    borderColor,
    color: "black"
  }
});

const styles = StyleSheet.create({
    item: {
        width: "100%",
        margin: 10,
        marginBottom: 30,
        height: 60,
    },
})

function Picker(props) {
    return (
        <View style={styles.item}>
            <View style={{ width: "100%", marginTop: 10, marginBottom: 10 }}>
            <Text style={{
                fontSize: 15,
                alignItems: "center",
                justifyContent: "center",
                fontWeight: 'bold'
            }}>
                {props.title}
            </Text>
            </View>
            <View style={{ width: "80%", paddingLeft: 20, paddingRight: 20 }}>
            <RNPickerSelect
                items={props.arr}
                onValueChange={props.onChange}
                style={{ ...pickerSelectStyles }}
                value={props.value}
                placeholder={props.placeholder}
            />
            </View>
        </View>
    )
}

export default Picker;