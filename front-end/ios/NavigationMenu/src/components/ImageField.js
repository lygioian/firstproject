import React from "react";
import Text from './Text'
import { Dimensions } from "react-native";
import {
    View,
    StyleSheet,
    Image,
    TouchableOpacity,
} from "react-native";

const screenWidth = Math.round(Dimensions.get("window").width);
const profilePictureSize = screenWidth * 0.35;

const styles = StyleSheet.create({
    personImg: {
        width: profilePictureSize*2.5,
        height: profilePictureSize,
        borderWidth: 2,
        borderColor: "#7D153F"
    },
})

function ImageField(props) {
    return (
        <View
            style={{
              flex: 1,
              paddingTop: 30,
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: "white"
            }}
        >
            <TouchableOpacity
              onPress={props.onPress ? props.onPress : null}
              activeOpacity={1}
            >
              <Image
                source = {{uri : props.source}}
                resizeMode="cover"
                style={styles.personImg}
              />
            </TouchableOpacity>
            <Text
              body2
              style={{
                color: "rgba(0, 0, 0, 0.7)",
                fontSize: 12,
                marginTop: 10
              }}
            >
              {props.title ? props.title : ''}
            </Text>
        </View>
    )
}

export default ImageField;