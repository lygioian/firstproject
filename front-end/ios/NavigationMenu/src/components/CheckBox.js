import React from "react";
import Text from './Text'
import {
    View,
    StyleSheet,
    CheckBox
} from "react-native";

const styles = StyleSheet.create({
    item: {
        width: "100%",
        margin: 10,
        marginBottom: 30,
        height: 60,
    },
})

function CheckBoxField(props) {
    return (
        <View style={styles.item}>
            <View style={{ width: "100%", marginTop: 10, marginBottom: 10 }}>
            <Text style={{
                fontSize: 15,
                alignItems: "center",
                justifyContent: "center",
                fontWeight: 'bold'
            }}>
                {props.title}
            </Text>
            </View>
            <View style={{flexDirection: "row", marginTop: 10}}>
            <CheckBox
                value={props.value}
                onValueChange={props.onChange}
                style={{
                alignSelf: "center"
                }}
            />
            <Text style={{
                margin: 8, 
                fontsize: 15,
                alignItems: "center",
                justifyContent: "center",
                fontWeight: 'bold'
            }}>
                {props.name}
            </Text>
            </View>
        </View>
    )
}

export default CheckBoxField;