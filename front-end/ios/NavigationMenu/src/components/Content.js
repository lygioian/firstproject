import React from "react";

import Text from './Text'

function Content(props) {
    return (
        <Text body2 grayColor lineHeight={20}>
            {props.content}
        </Text>
    )
}

export default Content;