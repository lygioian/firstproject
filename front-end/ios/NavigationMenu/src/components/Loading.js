import React from 'react'
import {
    View,
} from 'react-native'
import { connect } from 'react-redux'
import {channingActions} from '../lib/helper'

var Spinner = require('react-native-spinkit');

class Loading extends React.Component{
    constructor(props) {
        super(props)
    }

    render() {
        const {visible} = this.props.loading;
        if (!visible) return null;
        return (
            <View style ={{
                position: 'absolute',
                width:"100%",
                height: "100%",
                backgroundColor: "rgba(255,255,255,0.4)",
                justifyContent: "center",
                alignItems: "center",
            }}>
                <Spinner style={{marginBottom: 50}} type="Circle" isVisible={true} size={100} color="purple" />
            </View>
        )
    }
}
export default connect(
    state => ({loading: state.loading}),
    dispatch => channingActions({}, dispatch)
  )(Loading)