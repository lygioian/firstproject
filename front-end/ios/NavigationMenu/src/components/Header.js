import React from "react";

import Text from './Text'

function Header(props) {
    return (
        <Text body2 semibold style={{marginTop: 20, marginBottom: 10}}>
            {props.header}
        </Text>
    )
}

export default Header;