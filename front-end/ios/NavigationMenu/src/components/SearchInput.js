import React from "react";
import { StyleSheet, View, TouchableOpacity, Keyboard } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import _ from "lodash";

// import { routes } from "../config/routes";
import { TextInput } from "../ui";

const styles = StyleSheet.create({
  searchBar: {
    flexDirection: "row",
    backgroundColor: "rgba(0,0,0,0)",
    flex: 1,
    width: "100%",
    borderRadius: 12,
    borderWidth: 0.5,
    borderColor: "#bbb",
    alignItems: "center",
    padding: 10,
    paddingRight: 20,
    marginTop: 10,
    justifyContent: "flex-start"
  },
  inputText: {
    backgroundColor: "rgba(0,0,0,0)",
    color: "black",
    paddingHorizontal: 10,
    fontSize: 16,
    width: "100%"
  },
  searchIcon: {
    backgroundColor: "rgba(0,0,0,0)"
  }
});

class SearchInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      keyboardIsShow: false,
      search: ""
    };
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener("keyboardDidShow", () =>
      this._keyboardDidShow()
    );
    this.keyboardDidHideListener = Keyboard.addListener("keyboardDidHide", () =>
      this._keyboardDidHide()
    );
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow() {
    this.setState({ keyboardIsShow: true });
    console.log(this.state);
  }

  _keyboardDidHide() {
    this.setState({ keyboardIsShow: false });
  }

  updateSearch = search => {
    this.setState({ search });
  };

  onPress = () => {
    const {action} = this.props;
    const {search} = this.state;
    if (!search || search == "") {
      return;
    } else {
      action.getUsers({keyword: search}, true).then(response => {

      });
    }
  }

  render() {
    console.log("Props in search: ", this.props)
    return (
      <View style={{ flexDirection: "row", margin: 10 }}>
        <View style={[styles.searchBar]}>
          <Icon
            style={styles.searchIcon}
            size={20}
            name="account-search"
            color="#bbb"
          />
          <TextInput
            value={() => this.state.search}
            style={styles.inputText}
            onChangeText={this.updateSearch}
            clearButtonMode="always"
            autoCapitalize="none"
            clearTextOnFocus
            placeholder="Search..."
            placeholderTextColor="#bbb"
            selectionColor="#bbb"
            keyboardType="web-search"
          />
        </View>
        {this.state.keyboardIsShow ? (
          <TouchableOpacity
            style={{ marginLeft: 20, paddingTop: 5, justifyContent: "center" }}
            activeOpacity={1}
            onPress={Keyboard.dismiss}
          >
            <Icon
              style={{ color: "#7D153F" }}
              size={25}
              name="check"
              color="#bbb"
              onPress={this.onPress}
            />
          </TouchableOpacity>
        ) : null}
      </View>
    );
  }
}

export default SearchInput;