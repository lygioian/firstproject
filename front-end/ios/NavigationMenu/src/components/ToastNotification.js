import React from 'react'
import { connect } from 'react-redux'
import {channingActions} from '../lib/helper'
import { StyleSheet, Animated, Dimensions} from 'react-native'
import { Toast } from 'galio-framework';
import { Notification } from './Notification';
import { bindToastNotificationActions } from '../redux/actions/toastNotification';

const {width, height} = Dimensions.get('window')

class ToastingNotification extends React.Component{
    constructor(props) {
        super(props)


        this.state = {
            animation: new Animated.Value(0),
            opacity: new Animated.Value(1)
        }
    }

    // _moveAnimation = () => {
    //     Animated.timing(this.state.xValue, {
    //         toValue: width - 100,
    //         duration: 1000,
    //         asing: Easing.linear,
    //     }).start();
    // }

    increaseHeight = () => {

    }

    selectColor = (type) => {
        switch (type) {
            case "success":
                return "#4CB050"
            case "error":
                return "#FF5253"
            case "warning":
                return "#FEC107"
            case "infor2":
                return "#1976D3"
            default:
                return "#2196F3"
        }
    }

    // componentDidMount() {

    // }

    // componentWillUnmount() {


    // }

    componentDidUpdate() {
        const { toastingActions } = this.props
        const {isShow, notification, positionIndicator} = this.props.toasting
        // setTimeout(() => toastingActions.hide(), 5000)


        this.state.animation.setValue(0);
        this.state.opacity.setValue(1);

        if (isShow == true){
            Animated.timing(this.state.animation, {
                toValue: 1,
                duration: 7200
            }).start(({finished}) => {
                if (finished) {
                    toastingActions.hide();
                }
            });
        }
    }

    render() {
        const progressInterpolate = this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: ["0%", "100%"],
            extrapolate: "clamp"
        })

        const colorInterpolate = this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: ["white", "white"]
        })

        const progressStyle = {
            width: progressInterpolate,
            bottom: 0,
            backgroundColor: colorInterpolate,
            opacity: this.state.opacity,
        }

        const {toastingActions} = this.props
        const {isShow, notification, positionIndicator, type} = this.props.toasting;
        return (
            <Toast isShow={isShow} fadeOutDuration={0} positionIndicator={positionIndicator} positionOffset = {150} style={{
                width: width - 10,
                marginLeft: 5,
                marginRight: 5,
                padding: 0,
                display: !isShow ? 'none' : 'flex',
                backgroundColor: this.selectColor(type), borderRadius: 20, height: 70
            }}>
                <Notification action={toastingActions} type={type} notification={notification}/>
                <Animated.View style={[styles.progress, progressStyle]} /> 
            </Toast>
        )
    }
}

const styles = StyleSheet.create({
    progress: {
        position: "absolute",
        height: 10,
        width: '100%',
    }
})


export default connect(
    state => ({toasting: state.toasting}),
    dispatch => channingActions({}, dispatch, bindToastNotificationActions)
  )(ToastingNotification)