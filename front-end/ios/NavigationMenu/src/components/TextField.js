import React from "react";
import {
    View,
    StyleSheet,
} from "react-native";

import Text from './Text'

const borderColor = '#ddd';
const styles = StyleSheet.create({
    item: {
        width: "100%",
        margin: 10,
        marginBottom: 30,
        height: 60,
    },
    inputText: {
        fontSize: 15,
        paddingLeft: 20,
        borderBottomWidth: 0.5,
        borderColor,
        color: "black",
        textAlign: 'left',
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: '#F5F5F5', 
        borderRadius: 10
    },
})

function TextField(props) {
    return (
        <View style={styles.item} key={props.title}>
          <View style={{ width: "100%", marginTop: 10, marginBottom: 10 }}>
            <Text
              style={{
                fontSize: 15,
                alignItems: "center",
                justifyContent: "center",
                fontWeight: 'bold'
              }}
            >
              {props.title}
            </Text>
          </View>
          <View style={{ width: "100%", paddingLeft: 20, paddingRight: 20}}>
            <TextInput
              style={styles.inputText}
              onChangeText={props.onChange}
              placeholder={props.placeholder ? props.placeholder : ''}
              value={props.value}
              editable={props.isEdit}
              keyboardType= {props.isNumberic ? "decimal-pad" : "default"}
            />
          </View>
        </View>
    );
}

export default TextField;