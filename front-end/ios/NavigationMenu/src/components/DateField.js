import React from "react";
import {
    View,
    StyleSheet,
} from "react-native";

import Text from './Text'

import DatePicker from 'react-native-datepicker'

const styles = StyleSheet.create({
    item: {
        width: "100%",
        margin: 10,
        marginBottom: 30,
        height: 60,
    },
})

function DateField(props) {
    return (
        <View style={styles.item} key={props.title}>
            <View style={{ width: "100%", marginTop: 10, marginBottom: 10 }}>
            <Text
                style={{
                fontSize: 15,
                alignItems: "center",
                justifyContent: "center",
                fontWeight: 'bold'
                }}
            >
                {props.title}
            </Text>
            </View>
            <View style={{ width: "100%", paddingLeft: 20, paddingRight: 20 }}>
                <DatePicker
                    style={{width: 200}}
                    date={props.value}
                    mode='datetime'
                    placeholder="select date"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                        dateIcon: {
                        position: 'absolute',
                        left: 0,
                        top: 4,
                        marginLeft: 0
                        },
                        dateInput: {
                        marginLeft: 36
                        }
                    }}
                    onDateChange={props.onChange}
                />
            </View>
        </View>
    )
}

export default DateField;