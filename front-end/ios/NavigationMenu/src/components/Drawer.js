import React from "react";
import { TouchableOpacity, StyleSheet } from "react-native";
import { Block, Text, theme } from "galio-framework";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

// import Icon from "./Icon";
import materialTheme from "../constants/Theme";

const proScreens = [
  "Event Dashboard",
  "News Dashboard"
];

class DrawerItem extends React.Component {
  renderIcon = () => {
    const { title, focused } = this.props;
    let SIZE = 22;
    switch (title) {
      case "Event Dashboard":
        return (
          <Icon
            size={SIZE}
            name="home"
            color={focused ? "white" : materialTheme.COLORS.MUTED}
          />
        );
      case "News Dashboard":
        return (
          <Icon
            size={SIZE}
            name="newspaper"
            color={focused ? "white" : materialTheme.COLORS.MUTED}
          />
        );
      case "Account":
      return (
        <Icon
          size={SIZE}
          name="account-circle"
          color={focused ? "white" : materialTheme.COLORS.MUTED}
        />
      );
      case "Setting":
        return (
          <Icon
            size={SIZE}
            name="settings"
            color={focused ? "white" : materialTheme.COLORS.MUTED}
          />
        );
      default:
        return null;
    }
  };

  render() {
    const { focused, title, navigation, onPress, role } = this.props;
    const proScreen = proScreens.includes(title);
    if(proScreen && role !== 'admin') {
      return null;
    }
    return (
      <TouchableOpacity style={{ height: 55}} onPress={() => onPress(this.props)}>
        <Block
          flex
          row
          style={[
            styles.defaultStyle,
            focused ? [styles.activeStyle, styles.shadow] : null
          ]}
        >
          <Block middle flex={0.2} style={{ marginRight: 28 }}>
            {this.renderIcon()}
          </Block>
          <Block row center flex={0.9}>
            <Text
              size={18}
              color={
                focused
                  ? "white"
                  : "black"
              }
            >
              {title}
            </Text>
          </Block>
        </Block>
      </TouchableOpacity>
    );
  }
}

export default DrawerItem;

const styles = StyleSheet.create({
  defaultStyle: {
    paddingVertical: 16,
    paddingHorizontal: 16
  },
  activeStyle: {
    backgroundColor: materialTheme.COLORS.ACTIVE,
    borderRadius: 4
  },
  shadow: {
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 8,
    shadowOpacity: 0.2
  },
  pro: {
    backgroundColor: materialTheme.COLORS.LABEL,
    paddingHorizontal: 6,
    marginLeft: 8,
    borderRadius: 2,
    height: 20,
    width: 66
  }
});