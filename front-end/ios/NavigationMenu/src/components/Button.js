import React from "react";
import { Button, View } from "react-native";

function EventButton(props) {
    return (
        <View style={{
            margin: 10,
            marginHorizontal: 30
        }}>
            <Button
                onPress = {props.onPress}
                title = {props.title}
                disabled = {props.disabled ? props.disabled : false}
            />
        </View>
    )
}

export default EventButton;