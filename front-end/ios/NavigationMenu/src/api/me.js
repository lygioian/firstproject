import { axios } from '../lib/custom-axios';
import { API_URL, TEST_ID, EVENTS_PAGE_SIZE, LOGIN_URL,LOGIN_ADMIN_URL } from '../config';

export const MeAPI = {

    loginAdmin: (username, password) => {
        console.log(LOGIN_ADMIN_URL);
        return axios.post(LOGIN_ADMIN_URL, {
            username,
            password,
        })
    },

    getInfo: () => {
        return axios.get(`${API_URL}api/me`);
    },

    changePassword: (oldPassword, newPassword) => {
        return axios.patch(`${API_URL}api/me/password/change`, {
            oldPassword,
            newPassword,
        }); 
    },

    updateProfile: (data, isP) => {
        const formData = new FormData();
        if(isP) {
            formData.append("media", {
            uri: data.media.uri,
                name: "image.jpg",
                type: "image/jpeg"
            });
        }

        data.name ? formData.append('name', data.name) : null
        data.description ? formData.append('description', data.description) : null
        data.address ? formData.append('address', data.address) : null
        data.email ? formData.append('email', data.email) : null
        data.phone ? formData.append('phone', data.phone) : null

        return axios.patch(`${API_URL}api/me`, formData);
    },
};
