import { axios } from '../lib/custom-axios';
import { API_URL, TEST_ID, EVENTS_PAGE_SIZE } from '../config';

export const AdminAPI = {
    
    signUp: data => {
        return axios.post(`${API_URL}api/admin`, data);
    },
    
    // getInformationByUserName: username => {
    //     return axios.get(`${API_URL}users/viewById/${username}`);
    // },

    getAll: () => {
        return axios.get(`${API_URL}api/admin`);
    },

    changePassword: data => {
        return axios.patch(`${API_URL}api/admin/password/change`, data);
    },

    // update: data => {
    //     return axios.patch(`${API_URL}users/update`, data);
    // },
};
