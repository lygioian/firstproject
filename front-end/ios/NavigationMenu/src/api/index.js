import {AdminAPI} from './admin'
import {AuthAPI} from './auth'
import {EventsAPI} from './events'
import {MeAPI} from './me'
import {NewsAPI} from './news'
import {UsersAPI} from './users'

export {
    AdminAPI,
    AuthAPI,
    EventsAPI,
    NewsAPI,
    MeAPI,
    UsersAPI,
}