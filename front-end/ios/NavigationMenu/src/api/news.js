import { axios } from '../lib/custom-axios';
import { API_URL } from '../config';

export const NewsAPI = {

    create: data => {
        const formData = new FormData();
        formData.append("media", {
        uri: data.media.uri,
            name: "image.jpg",
            type: "image/jpeg"
        });

        formData.append('title', data.title)
        formData.append('author', data.author)
        formData.append('content', data.content)
        formData.append('eventAddress', data.eventAddress)
        formData.append('type', data.type)
        formData.append('source', data.source)
        formData.append('writtenAt', data.writtenAt)
        formData.append('location', data.location)
        console.log("You reach here")
        return axios.post(`${API_URL}api/news/`, formData);
    },

    // viewNewsById: postId => {
    //     return axios.get(`${API_URL}api/news/${postId}`);
    // },

    getAll: () => {
        return axios.get(`${API_URL}api/news/`);
    },

    getById: (newsId) => {
        return axios.get(`${API_URL}api/news/${newsId}`);
    },

    update: (postId, data, isP) => {
        const formData = new FormData();
        if(isP) {
            formData.append("media", {
            uri: data.media.uri,
                name: "image.jpg",
                type: "image/jpeg"
            });
        }

        data.title ? formData.append('title', data.title) : null
        data.author ? formData.append('author', data.author) : null
        data.content ? formData.append('content', data.content) : null
        data.eventAddress ? formData.append('eventAddress', data.eventAddress) : null
        data.type ? formData.append('type', data.type) : null
        data.source ? formData.append('source', data.source) : null
        data.writtenAt ? formData.append('writtenAt', data.writtenAt) : null
        data.location ? formData.append('location', data.location) : null

        return axios.patch(`${API_URL}api/news/${postId}`, formData);
    },

    // getNewsByUserId: () => {
    //     return axios.get(`${API_URL}api/news/usercreated`);
    // },

    deleteNews: (newsId) => {
        return axios.delete(`${API_URL}api/news/${newsId}/`);
    },
};