import { axios } from '@lib';
import { API_URL } from '../config';    

export const EventsAPI = {
    
    create: data => {
        const formData = new FormData();
    
        formData.append("media", {
          uri: data.media.uri,
          name: "image.jpg",
          type: "image/jpeg"
        });

        formData.append('title', data.title)
        formData.append('description', data.description)
        formData.append('socialDays', Number(data.socialDays))
        formData.append('maxRegister', Number(data.maxRegister))
        formData.append('formStart', data.formStart)
        formData.append('formEnd', data.formEnd)
        // data.eventAddress ? formData.append('eventAddress', data.eventAddress) : null
        // data.eventUnit ? formData.append('eventUnit', data.eventUnit) : null
        // data.type ? formData.append('type', data.type) : null
        // data.requirements ? formData.append('requirements', data.requirements) : null
        // data.placeGather ? formData.append('placeGather', data.placeGather) : null
        // data.isUrgent ? formData.append('isUrgent', data.isUrgent) : null
        formData.append('eventAddress', data.eventAddress)
        formData.append('eventUnit', data.eventUnit)
        formData.append('type', data.type)
        formData.append('requirements', data.requirements)
        formData.append('placeGather', data.placeGather)
        formData.append('isUrgent', data.isUrgent)

        data.eventStart ? formData.append('eventStart', data.eventStart) : null
        data.eventEnd ? formData.append('eventEnd', data.eventEnd) : null
        data.timeGather ? formData.append('timeGather', data.timeGather) : null
        console.log("Checking CreateEvent API: ", formData)
        return axios.post(`${API_URL}api/event/`, formData);
    },

    getEvents: (offset, limit, type, constraint) => {
        console.log(`${API_URL}api/event?offset=${offset}&limit=${limit}&type=${type}&constraint=${constraint}`)
        return axios.get(`${API_URL}api/event?offset=${offset}&limit=${limit}&type=${type}&constraint=${constraint}`)
    },

    getEventById: (eventId) => {
        return axios.get(`${API_URL}api/event?eventId=${eventId}`)
    },

    update: (data, eventId, isP) => {
        const formData = new FormData();
        if(isP) {
            formData.append("media", {
            uri: data.media.uri,
            name: "image.jpg",
            type: "image/jpeg"
            });
        }

        data.title ? formData.append('title', data.title) : null
        data.description ? formData.append('description', data.description) : null
        data.socialDays ? formData.append('socialDays', Number(data.socialDays)) : null
        data.maxRegister ? formData.append('maxRegister', Number(data.maxRegister)) : null
        data.formStart ? formData.append('formStart', data.formStart) : null
        data.formEnd ? formData.append('formEnd', data.formEnd) : null
        data.eventAddress ? formData.append('eventAddress', data.eventAddress) : null
        data.eventUnit ? formData.append('eventUnit', data.eventUnit) : null
        data.type ? formData.append('type', data.type) : null
        data.requirements ? formData.append('requirements', data.requirements) : null
        data.placeGather ? formData.append('placeGather', data.placeGather) : null 
        data.isUrgent ? formData.append('isUrgent', data.isUrgent) : null
        data.eventStart ? formData.append('eventStart', data.eventStart) : null
        data.eventEnd ? formData.append('eventEnd', data.eventEnd) : null
        data.timeGather ? formData.append('timeGather', data.timeGather) : null

        return axios.patch(`${API_URL}api/event/${eventId}`, formData);
    },

    addRegister: (eventId, userId) => {
        return axios.post(`${API_URL}api/event/${eventId}/register?userId=${userId}`);
    },

    addStaff: (eventId, userId, role) => {
        return axios.post(`${API_URL}api/event/${eventId}/staff?userId=${userId}&role=${role}`);
    },

    deleteStaff: (eventId, userId, role) => {
        return axios.delete(`${API_URL}api/event/${eventId}/staff?userId=${userId}&role=${role}`);
    },
    
    addRegister: (eventId, userId) => {
        return axios.post(`${API_URL}api/event/${eventId}/register?userId=${userId}`);
    },

    deleteRegister: (eventId, userId) => {
        return axios.delete(`${API_URL}api/event/${eventId}/register?userId=${userId}`);
    },

    deleteEvent: (eventId) => {
        return axios.delete(`${API_URL}api/event/${eventId}/`);
    },

    startEvent: (eventId) => {
        return axios.patch(`${API_URL}api/event/${eventId}/start`)
    },

    finishEvent: (eventId) => {
        return axios.patch(`${API_URL}api/event/${eventId}/finish`)
    },

    startFirstCheck: (eventId) => {
        return axios.patch(`${API_URL}api/event/${eventId}/attendance/first/start`)
    },

    finishFirstCheck: (eventId) => {
        return axios.patch(`${API_URL}api/event/${eventId}/attendance/first/finish`)
    },

    startSecondCheck: (eventId) => {
        return axios.patch(`${API_URL}api/event/${eventId}/attendance/second/start`)
    },

    finishSecondCheck: (eventId) => {
        return axios.patch(`${API_URL}api/event/${eventId}/attendance/second/finish`)
    }
};