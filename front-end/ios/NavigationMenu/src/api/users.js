import { axios } from '../lib/custom-axios';
import { API_URL, TEST_ID, EVENTS_PAGE_SIZE } from '../config';

export const UsersAPI = {
    
    signUp: data => {
        return axios.post(`${API_URL}api/users/register`, data);
    },

    getUsers: (limit, keyword, type) => {
        if (keyword == null) {
            console.log("You are here")
            return axios.get(`${API_URL}api/users?limit=${limit}&type=${type}`)
        } else {
            return axios.get(`${API_URL}api/users?limit=${limit}&keyword=${keyword}&type=${type}`);
        }
    },

    changePassword: data => {
        return axios.patch(`${API_URL}users/changPassword`, data);
    },

    update: data => {
        return axios.patch(`${API_URL}users/update`, data);
    },
};
