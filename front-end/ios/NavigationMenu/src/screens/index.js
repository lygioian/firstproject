import React from "react";
import LoginScreen from './login';
import SettingScreen from "./setting";
import {
    EventScreen,
    EventDetailScreen,
    EditEventScreen,
    CreateEventScreen,
    ViewStaffScreen,
    ViewRegisterScreen,
    AddRegisterScreen,
    AddStaffScreen,
    EventConfirmScreen,
    FirstCheckScreen,
    SecondCheckScreen,
    RegistersReportScreen,
    StaffsReportScreen,
    RegisterReportScreen,
    StaffReportScreen
} from './event'
import {
    ProfileScreen,
    ProfileViewScreen,
    ChangePasswordScreen,
    EditProfileScreen
} from './profile'
import {
    NewsScreen,
    NewsDetailScreen,
    CreateNewsScreen,
    EditNewsScreen,
    MyNewsScreen,
} from './news'

export const Login = (navigation) => <LoginScreen {...navigation} name="Login" />;
export const Setting = (navigation) => <SettingScreen {...navigation} name="Setting" />;
export const Event = (navigation) => <EventScreen {...navigation} name="Events" />;
export const EventDetail = (navigation) => <EventDetailScreen {...navigation} name="EventDetail" />;
export const EditEvent = (navigation) => <EditEventScreen {...navigation} name="EditEvent" />;
export const CreateEvent = (navigation) => <CreateEventScreen {...navigation} name="CreateEvent" />;
export const ViewStaff = (navigation) => <ViewStaffScreen {...navigation} name="ViewStaff" />;
export const ViewRegister = (navigation) => <ViewRegisterScreen {...navigation} name="ViewRegister" />;
export const AddRegister = (navigation) => <AddRegisterScreen {...navigation} name="AddRegister" />;
export const AddStaff = (navigation) => <AddStaffScreen {...navigation} name="AddStaff" />;
export const ConfirmEvent = (navigation) => <EventConfirmScreen {...navigation} name="ConfirmEvent" />;
export const Profile = (navigation) => <ProfileScreen {...navigation} name="Profile" />;
export const ProfileView = (navigation) => <ProfileViewScreen {...navigation} name="ProfileView" />;
export const ChangePassword = (navigation) => <ChangePasswordScreen {...navigation} name="ChangePassword" />;
export const EditProfile = (navigation) => <EditProfileScreen {...navigation} name="EditProfile" />;
export const News = (navigation) => <NewsScreen {...navigation} name="News" />;
export const NewsDetail = (navigation) => <NewsDetailScreen {...navigation} name="NewsDetail" />
export const CreateNews = (navigation) => <CreateNewsScreen {...navigation} name="CreateNews" />
export const EditNews = (navigation) => <EditNewsScreen {...navigation} name="EditNews" />;
export const MyNews = (navigation) => <MyNewsScreen {...navigation} name="MyNews" />;
export const FirstCheck = (navigation) => <FirstCheckScreen {...navigation} name="FirstCheck" />
export const SecondCheck = (navigation) => <SecondCheckScreen {...navigation} name="SecondCheck" />
export const RegistersReport = (navigation) => <RegistersReportScreen {...navigation} name="RegistersReport" />
export const StaffsReport = (navigation) => <StaffsReportScreen {...navigation} name="StaffsReport" />
export const RegisterReport = (navigation) => <RegisterReportScreen {...navigation} name="RegisterReport" />
export const StaffReport = (navigation) => <StaffReportScreen {...navigation} name="StaffReport" />