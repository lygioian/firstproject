import ProfileScreen from './profile';
import ProfileViewScreen from './profile-view';
import ChangePasswordScreen from './change-password'
import EditProfileScreen from "./edit-profile";

export {
    ProfileScreen,
    ProfileViewScreen,
    ChangePasswordScreen,
    EditProfileScreen
}