import React from 'react';
import { StyleSheet, ScrollView, TouchableOpacity, View ,SafeAreaView} from 'react-native';
import { Text } from 'galio-framework';
import { connect } from 'react-redux'
import {
  channingActions
} from '@lib'
import {
  bindMeActions,
  bindEventsActions,
  bindToastNotificationActions,
  bindLoadingActions,
  bindDialogActions
} from '@actions'
import {API_URL} from '../../config'
import Icon from "react-native-vector-icons/FontAwesome";
import {
  ProfileDetail,
  Button
} from '@components'

const styles =  StyleSheet.create({
  contentTitle: {
    alignItems: "flex-start",
    width: "100%",
    height: 32,
    justifyContent: "center"
  },
  contain: {
    flex: 1,
    padding: 20
  },
  textInput: {
    height: 56,
    backgroundColor: 'white',
    borderRadius: 5,
    padding: 10,
    width: "100%"
  },
  profileItem: {
    flexDirection: "row",
    justifyContent: "space-between",
    borderBottomColor: "#c7c7cc",
    borderBottomWidth: 1,
    alignItems: "center",
    marginTop: 5,
    paddingVertical: 20
  }
});


class ProfileScreen extends React.Component {
  constructor(props) {
    super(props);
  }

  onLogOut = () => {
    const {meActions, loadingActions, toastingActions, } = this.props;
    loadingActions.show();
    meActions.deleteUserDataAction().then(res =>{
      console.log("logout Success")
      loadingActions.hide();
        // props.navigation.navigate('Login');
    }).catch(rej => {
      console.log("Rej in log-out", rej)
      loadingActions.hide()
      toastingActions.show("Error !!!");
    })
  }

  renderItem = (title, onHandle) => (
    <TouchableOpacity
      style={[
      styles.profileItem,
      // {borderBottomColor: colors.border, borderBottomWidth: 1},
      ]}
      onPress={onHandle}
    >
      <Text color="#212121">{title}</Text>
      <Icon
        name="chevron-right"
        size={15}
        color="#5DADE2"
        style={{marginLeft: 5}}
        enableRTL={true}
      />
    </TouchableOpacity>
  )

  render() {
    const user = this.props.me.information;
    console.log("Profile Props: ", this.props)
    const {navigation, dialogActions} = this.props;
    return (
      <SafeAreaView forceInset={{top: 'always'}}>
      {/* <Header
        title={t('profile')}
        renderRight={() => {
          return <Icon name="bell" size={24} color={colors.primary} />;
        }}
        onPressRight={() => {
          navigation.navigate('Notification');
        }}
      /> */}
      <ScrollView>
        <View style={styles.contain}>
            <ProfileDetail
              image={API_URL + user.media.thumbnail}
              textFirst={user.name}
              point={'9.5'}
              textSecond={user.phone}
              textThird={user.username}
              onPress={() => navigation.navigate('ProfileView', {product: user})}
            />
          {/* <ProfilePerformance
            data={userData.performance}
            style={{marginTop: 20, marginBottom: 20}}
          /> */}
          {this.renderItem("Profile",() => {this.props.navigation.navigate('ProfileView', {product: user})})}
          {this.renderItem("Edit Profile",() => {this.props.navigation.navigate('EditProfile')})}
          <TouchableOpacity
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
              marginTop: 5,
              paddingVertical: 20
            }}
            onPress={() => navigation.navigate('ChangePassword')}
          >
            <Text color="#212121">Change Password</Text>
            <Icon
              name="chevron-right"
              size={15}
              color="#5DADE2"
              style={{marginLeft: 5}}
              enableRTL={true}
            />
          </TouchableOpacity>
        </View>
      </ScrollView>
      <View style={{paddingHorizontal: 20, paddingVertical: 15}}>
        <Button
          onPress={() => {dialogActions.show("LogOut", "Are you sure to Log Out", "YES", "NO", this.onLogOut)}}
          title="Sign out"
        />
      </View>
    </SafeAreaView>

    );
  }
}

export default connect(
  state => ({me: state.me}),
  dispatch => channingActions({}, dispatch, bindMeActions, bindEventsActions, bindLoadingActions, bindToastNotificationActions, bindDialogActions)
)(ProfileScreen)