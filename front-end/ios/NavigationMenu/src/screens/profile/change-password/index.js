import React from "react";
import {
  View,
  StyleSheet,
  KeyboardAvoidingView,
  ScrollView,
} from "react-native";
import { connect } from 'react-redux'
import {
  channingActions
} from '@lib'
import {
  bindMeActions,
  bindEventsActions,
  bindLoadingActions,
  bindDialogActions,
  bindToastNotificationActions
} from '@actions'
import {API_URL} from '../../../config'
import {
  TextField,
  Button,
  ImageField
} from '@components'

const borderColor = '#ddd';

class ChangePasswordScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: this.props.me.information.username,
      oldPassword: "",
      newPassword: "",
      confirmPassword: "",
      password: this.props.me.information.password,
      role: this.props.me.information.role,
      urlThumnail: API_URL+ this.props.me.information.media.thumbnail,
    };
  }

  checkPassword() {
    if (this.state.newPassword != this.state.confirmPassword) {
      this.setState({
        oldPassword: "",
        newPassword: "",
        confirmPassword: "",
      })
      return false;
    }
    return true;
  }

  navigationButtonPressed = () =>  {
    const { meActions, loadingActions, toastingActions } = this.props;
    loadingActions.show()
    const { 
    oldPassword,
    newPassword, 
    confirmPassword,
    password, } = this.state;

    const data = {
      oldPassword: oldPassword,
      newPassword: newPassword,
    }

    meActions
      .changePassword(data.oldPassword, data.newPassword)
      .then((res) => {
        loadingActions.hide()
        toastingActions.show("Change Password successfully", "bottom", "success")
        this.props.navigation.goBack();
      })
      .catch((rej) => {
        loadingActions.hide();
        toastingActions.show("Old Password Incorrect", "bottom", "error");
        this.setState({
          oldPassword: "Old Password",
          newPassword: "Password",
          confirmPassword: "Password Confirm"
        })
        console.log("Error in changePassword: ", rej);
      });  
    
    return;
  }

  onButtonHandle = () => {
    const { toastingActions, dialogActions } = this.props;
    const { 
    oldPassword,
    newPassword, 
    confirmPassword,
    password, } = this.state;
    if (oldPassword == "" || newPassword == "" || confirmPassword == "") {
      toastingActions.show("You need to fill all information", "bottom", "error")
    }
    else if (!this.checkPassword()) {
      toastingActions.show("Confirm Password Incorrect", "bottom", "error");
      return;
    }
    else {
      dialogActions.show("changePassword", "Are you sure to change your password", "YES", "NO", this.navigationButtonPressed)
    }
  }
  
  render() {
    console.log("password: ",this.state.password);
    return (
      <KeyboardAvoidingView
        style={styles.container}
        enabled
        key={this.state.timestamp}
      >
        <ScrollView>
          <ImageField
            source={this.state.urlThumnail}
          />

          <View style={styles.information}>
            <TextField
              title="Old Password"
              placeholder="Old Password"
              value={this.state.oldPassword}
              onChange={oldPassword => this.setState({ oldPassword })}
            />

            <TextField
              title="New Password"
              placeholder="Password"
              value={this.state.newPassword}
              onChange={newPassword => this.setState({ newPassword })}
            />

            <TextField
              title="Confirm Password"
              placeholder="Confirm Password"
              value={this.state.confirmPassword}
              onChange={confirmPassword => this.setState({ confirmPassword })}
            />
            
          </View>
          </ScrollView>
          <View style={{
            paddingVertical: 15, 
            paddingHorizontal: 15,
          }}>
            <Button
              title="Confirm"
              onPress={this.onButtonHandle}
            />
          </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  information: {
    width: "100%",
    borderTopWidth: 0.5,
    borderColor,
    paddingTop: 20,
    paddingBottom: 20,
  },
});

export default connect(
  state => ({me: state.me}),
  dispatch => channingActions({}, dispatch, bindMeActions, bindEventsActions, bindLoadingActions, bindToastNotificationActions, bindDialogActions)
)(ChangePasswordScreen)

