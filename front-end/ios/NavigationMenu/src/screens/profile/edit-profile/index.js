import React from "react";
import {
  View,
  StyleSheet,
  KeyboardAvoidingView,
  ScrollView,
} from "react-native";
import { connect } from 'react-redux'
import {
  channingActions
} from '@lib'
import {
  bindMeActions,
  bindEventsActions,
  bindLoadingActions,
  bindDialogActions,
  bindToastNotificationActions
} from '@actions'
import ImagePicker from "react-native-image-picker";
import _ from 'lodash';
import {API_URL} from '../../../config'
import {
  TextField,
  ImageField,
  Button
} from '@components'

class EditProfile extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: this.props.me.information.username,
      phone: this.props.me.information.phone + "",
      email: this.props.me.information.email,
      role: this.props.me.information.role,
      name: this.props.me.information.name,
      description: this.props.me.information.description,
      address: this.props.me.information.address,
      media: API_URL+ this.props.me.information.media.thumbnail,
      responseChangePhoto: null
    };

    this.choose = [
      {
        label: "Male",
        value: "Male"
      },
      {
        label: "Female",
        value: "Female"
      },
      {
        label: "Other",
        value: "Other"
      }
    ];

    // Navigation.events().bindComponent(this);

    this.changeProfilePhoto = this.changeProfilePhoto.bind(this);
  }

  changeProfilePhoto() {
    const { meActions } = this.props;

    const options = {
      title: "Select Photo",
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };
    
    ImagePicker.showImagePicker(options, async response => {
      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        this.setState({media: response.uri, responseChangePhoto: response})
       
      }
    });    
  }

  navigationButtonPressed = () =>  {
    const { meActions, loadingActions, toastingActions } = this.props;
    loadingActions.show()
    const { 
    phone,
    email,
    name,
    description,
    address,
    responseChangePhoto, } = this.state;
    const data = {
      phone: parseInt(phone),
      email: email,
      name: name,
      description: description,
      address: address,
    }
    console.log("data: ", data)

    if(this.state.responseChangePhoto != null){
      data.media = this.state.responseChangePhoto
      meActions
      .updateProfile(data, true)
      .then((res) => {
        loadingActions.hide()
        toastingActions.show("Update Successfully !!", "bottom", "success");
        console.log("Succc", res)
        this.props.navigation.goBack();
      }).catch(rej => {
        loadingActions.hide()
        toastingActions.show("Error", "bottom", "error");
        console.log("Fail: ", rej)
      })     
    } else {
      meActions
      .updateProfile(data, false)
      .then((res) => {
        loadingActions.hide()
        toastingActions.show("Update Successfully !!", "bottom", "success");
        console.log("Succc", res)
        this.props.navigation.goBack();
      }).catch(rej => {
        loadingActions.hide()
        toastingActions.show("Error", "bottom", "error");
        console.log("Fail: ", rej)
      })
    }
  }
  
  render() {
    const { dialogActions, toastingActions } = this.props;
    console.log("Event edit props: ", this.props)
    const oldProfile = this.props.me.information; 
    const data = {
      ...oldProfile,
      phone: this.state.phone,
      email: this.state.email,
      name: this.state.name,
      description: this.state.description,
      address: this.state.address,
    }
    const isChange = (!_.isEqual(data, oldProfile) || this.state.responseChangePhoto != null);
    const placeholder = {
      label: "Select type ...",
      value: ""
    };
    return (
      <KeyboardAvoidingView
        style={styles.container}
        enabled
        key={this.state.timestamp}
      >
        <ScrollView>
          <ImageField
            onPress={this.changeProfilePhoto}
            source={this.state.media}
            title="Change Image"
          />

          <View style={styles.information}>
            <TextField
              title="Account"
              value={this.state.username}
              isEdit={false}
            />

            <TextField
              title="Email"
              value={this.state.email}
              onChange={email => this.setState({ email })}
            />

            <TextField
              title="Name"
              value={this.state.name}
              onChange={name => this.setState({ name })}
            />

            <TextField
              title="Phone"
              value={this.state.phone}
              onChange={phone => this.setState({ phone })}
            />

            <TextField
              title="Role"
              value={this.state.role}
              onChange={role => this.setState({ role })}
              isEdit={false}
            />

            <TextField
              title="Description"
              value={this.state.description}
              onChange={description => this.setState({ description })}
            />

            <TextField
              title="Address"
              value={this.state.address}
              onChange={address => this.setState({ address })}
            />
          </View>
        </ScrollView>
        <View style={{
          paddingVertical: 15, 
          paddingHorizontal: 15,
        }}>
          <Button
            title="Confirm"
            onPress={() => {
              if (!isChange) {
                return toastingActions.show("You haven't change anything !", "bottom", "warning")
              }
              else {
                return dialogActions.show("EditProfile", "Are you sure to update your profile ?", "YES", "NO", this.navigationButtonPressed)
              }
            }}
          />
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  information: {
    width: "100%",
    paddingTop: 20,
    paddingBottom: 20,
    backgroundColor: 'white'
  },
});

export default connect(
  state => ({me: state.me}),
  dispatch => channingActions({}, dispatch, bindMeActions, bindEventsActions, bindLoadingActions, bindDialogActions, bindToastNotificationActions)
)(EditProfile)