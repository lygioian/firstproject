import React from "react";
import {
  View,
  StyleSheet,
  KeyboardAvoidingView,
  ScrollView,
  Dimensions
} from "react-native";
import { connect } from 'react-redux'
import {
  channingActions
} from '@lib'
import {
  bindMeActions,
  bindNewsActions,
  bindLoadingActions,
  bindToastNotificationActions,
  bindDialogActions
} from '@actions'
import ImagePicker from "react-native-image-picker";
import {API_URL} from '../../../config'
import Carousel, { Pagination } from "react-native-snap-carousel";
import {
  TextField,
  DateField,
  ImageField,
  Picker,
  Button,
  generateGalleryTemplate
} from '@components'

const screenWidth = Math.round(Dimensions.get("window").width);

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  information: {
    width: "100%",
    paddingTop: 20,
    paddingBottom: 20,
    backgroundColor: 'white'
  },
});


class EditNewsScreen extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      title: this.props.post.title,
      author: this.props.post.author,
      source: this.props.post.source,
      type: this.props.post.type,
      location: this.props.post.location,
      content: this.props.post.content,
      eventAt: new Date(this.props.post.postAt),
      writtenAt: new Date(this.props.post.writtenAt),
      media: API_URL+ this.props.post.media.path.thumbnail,
      responseChangePhoto: null
    };
    this.choose = [
      {
        label: "Post",
        value: "Post"
      },
      {
        label: "Activity",
        value: "Activity"
      },
      {
        label: "Study",
        value: "Study"
      }
    ];

    // Navigation.events().bindComponent(this);

    this.changeProfilePhoto = this.changeProfilePhoto.bind(this);
  }

  changeProfilePhoto() {
    const { meActions } = this.props;

    const options = {
      title: "Select Photo",
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };
    
    ImagePicker.showImagePicker(options, async response => {
      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        this.setState({media: response.uri, responseChangePhoto: response})
      }
    });    
  }

  _renderItem = ({ item, index }) => {
    return (
      <View style={styles.container} key={index}>
        {generateGalleryTemplate(item.posts, this.onPress)}
      </View>
    );
  };

  get pagination() {
    const { element, index } = this.state;
    if (3 == 1)
      return (
        <View
          style={{
            height: 30,
            width: "100%",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <View
            style={{
              width: 8,
              height: 8,
              borderRadius: 4,
              backgroundColor: "#7D153F"
            }}
          />
        </View>
      );
    else
      return (
        <Pagination
          dotsLength={3}
          activeDotIndex={1}
          tappableDots={true}
          containerStyle={{
            backgroundColor: "#fff",
            paddingVertical: 0,
            height: 30
          }}
          dotColor="#7D153F"
          inactiveDotColor="#7D153F"
          dotStyle={{
            width: 8,
            height: 8,
            borderRadius: 5,
            margin: 0,
            padding: 0,
            top: 0,
            backgroundColor: "rgba(0, 0, 0, 0.80)"
          }}
          inactiveDotStyle={
            {
              // Define styles for inactive dots here
            }
          }
          inactiveDotOpacity={0.4}
          inactiveDotScale={0.6}
        />
      );
  }

  navigationButtonPressed = () =>  {
    const { newsActions, meActions, loadingActions, toastingActions, dialogActions } = this.props;
    loadingActions.show()
    const { 
    title,
    author,
    location,
    type,
    source,
    writtenAt,
    content,
    responseChangePhoto, } = this.state;
    const data = {
      title: title,
      source: source,
      author: author,
      location: location,
      type: type,
      source: source,
      content: content,
      writtenAt: new Date(writtenAt).getTime(),
      media: responseChangePhoto
    }
    //console.log(data)
    if(this.state.responseChangePhoto == null){
      newsActions
      .updateNews(this.props.post._id, data, false)
      .then((res) => {
        loadingActions.hide()
        toastingActions.show("Update Successfully !!", "bottom", "success");
        this.props.navigation.goBack();
      }).catch(rej => {
        loadingActions.hide()
        toastingActions.show("Error", "bottom", "error");
      });      
      return;
    }

    newsActions
      .updateNews(this.props.post._id, data, true)
      .then((res) => {
        loadingActions.hide()
        toastingActions.show("Update Successfully !!", "bottom", "success");
        this.props.navigation.goBack();
      }).catch (rej => {
        loadingActions.hide()
        toastingActions.show("Error", "bottom", "error");
      });      
      return;
  }
  
  render() {

    const {dialogActions} = this.props

    const placeholder = {
      label: "Select type ...",
      value: ""
    };

    return (
        <KeyboardAvoidingView
          style={styles.container}
          // keyboardVerticalOffset={100}
          // behavior="padding"
          enabled
          key={this.state.timestamp}
        >
          <Carousel
            data={[{}]}
            ref={ref => (this.carousel = ref)}
            renderItem={this._renderItem}
            sliderWidth={screenWidth}
            itemWidth={screenWidth}
            onSnapToItem={index => this.setState({ index: index })}
          />
          {this.pagination}
          <ScrollView>
            <ImageField
              onPress={this.changeProfilePhoto}
              source={this.state.media}
              title="Change Thumbnail"
            />
  
            <View style={styles.information}>
              <TextField
                title="Title"
                value={this.state.title}
                onChange={title => this.setState({ title })}
              />

              <TextField
                title="Author"
                value={this.state.author}
                onChange={author => this.setState({ author })}
              />

              <TextField
                title="Author"
                value={this.state.author}
                onChange={author => this.setState({ author })}
              />

              <DateField 
                title="Written At"
                value={this.state.writtenAt}
                onChange={writtenAt => this.setState({ writtenAt })}
              />

              <TextField
                title="Source"
                value={this.state.source}
                onChange={source => this.setState({ source })}
              />

              <TextField
                title="Location"
                value={this.state.location}
                onChange={location => this.setState({ location })}
              />

              <Picker 
                title="Type of News"
                arr={this.choose}
                onChange={value => {
                  this.setState({ type: value });
                }}
                value={this.state.type}
                placeholder={placeholder}
              />

              <TextField
                title="Content"
                value={this.state.description}
                onChange={description => this.setState({ description })}
              />
            </View>
          </ScrollView>
          <View style={{
            paddingVertical: 15, 
            paddingHorizontal: 15,
          }}>
            <Button
              title="Confirm"
              onPress={() => dialogActions.show("UpdateNews", "Do you want to update this news", "YES", "NO", this.navigationButtonPressed)}
            />
          </View>
        </KeyboardAvoidingView>
      );
  }
}

export default connect(
  state => ({me: state.me, post: state.news.post}),
  dispatch => channingActions({}, dispatch, bindMeActions, bindNewsActions, bindLoadingActions, bindToastNotificationActions, bindDialogActions)
)(EditNewsScreen)