import NewsScreen from './news'
import NewsDetailScreen from './news-detail'
import CreateNewsScreen from './create-news'
import EditNewsScreen from './news-edit';
import MyNewsScreen from './myNews';

export {
    NewsScreen,
    NewsDetailScreen,
    CreateNewsScreen,
    EditNewsScreen,
    MyNewsScreen,
}
