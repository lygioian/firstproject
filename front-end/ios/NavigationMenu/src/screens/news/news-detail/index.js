import React from "react";
import { View, TouchableOpacity, StyleSheet, Linking, SafeAreaView, ScrollView,Animated, Image} from "react-native";
import Utils from '../../../helpers/Utils'
import {API_URL} from '../../../config'
import {
  bindNewsActions,
  bindToastNotificationActions,
  bindMeActions,
  bindLoadingActions,
  bindDialogActions
} from '@actions'
import { connect } from 'react-redux'
import {
  channingActions
} from '@lib'
import _ from "lodash"
import {
  Text,
  Button
} from '@components'

class NewsDetailScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: "",
      locationReady: true,
      token: "",
      heightHeader: Utils.heightHeader(),
      savedDataLoaded: false
    };
    this.deltaY = new Animated.Value(0);
    this.heightImageBanner = Utils.scaleWithPixel(250, 1);

  }

  componentDidMount() {
    console.log("ID", this.props.route.params)
    const { newsActions } = this.props
    newsActions.getNewsById(this.props.route.params).then(res => {
      console.log("res", res.data)
    }).catch(rej => {
      console.log(rej)
    })
  }

  onHandleDelete = () => {
    const { dialogActions } = this.props;
    dialogActions.show("DeleteNews", "Are you sure to delete this Post", "YES", "NO",() => {this.deleteNews()})
  }

  deleteNews() {
    const {loadingActions, toastingActions, newsActions} = this.props;
    loadingActions.show();
    newsActions
      .deleteNews(this.props.post._id)
      .then(res => {
        loadingActions.hide()
        toastingActions.show("Delete News Successfully", "bottom", "success")
        this.props.navigation.goBack();
      }).catch(rej => {
        console.log("Error in deleteNews in news-view: ", rej)
        loadingActions.hide()
        toastingActions.show("Actions Fail (" + rej + ")", "bottom", "error")
      })
  }

  render(){
    const { heightHeader} = this.state;
    const deltaY = this.deltaY;
    let data = this.props.post;
    if (data == null) return null
    const me = this.props.me.information;
    console.log("Props in NewsDetail: ", this.props);
    return (
    <View style = {{flex: 1, backgroundColor: 'white'}}>
      <Animated.View
        style={[
          styles.imgBanner,
          {
            height: deltaY.interpolate({
              inputRange: [
                0,
                Utils.scaleWithPixel(140),
                Utils.scaleWithPixel(140),
              ],
              outputRange: [this.heightImageBanner, heightHeader, heightHeader],
            }),
          },
        ]}>
        <Image source={{uri: API_URL + data.media.path.thumbnail}} style={{flex: 1}} />
        <Animated.View
          style={{
            position: 'absolute',
            flexDirection: 'row',
            
            justifyContent: 'space-between',
            alignItems: 'flex-end',
            width: '100%',
            bottom: 0,
            padding: 10,
            backgroundColor: 'rgba(0,0,0,0.4)',

            opacity: deltaY.interpolate({
              inputRange: [
                0,
                Utils.scaleWithPixel(140),
                Utils.scaleWithPixel(140),
              ],
              outputRange: [1, 0, 0],
            }),
          }}>
          <View style={styles.rowBanner}>
            <Image source={{uri: API_URL + data.media.path.thumbnail}} style={styles.userIcon} />
            <View style={{alignItems: 'flex-start'}}>
              <Text headline semibold whiteColor style={{color:'white'}}>
                {data.userCreated.name}
              </Text>
              <Text footnote whiteColor style={{color:'white'}}>
                {data.title}
              </Text>
            </View>
          </View>
        </Animated.View>
      </Animated.View>
      <SafeAreaView style={{flex: 1}} forceInset={{top: 'always'}}>
       <ScrollView
          onScroll={Animated.event([
            {
              nativeEvent: {
                contentOffset: {y: this.deltaY},
              },
            },
          ])}
          onContentSizeChange={() => {
            this.setState({heightHeader: Utils.heightHeader()})
          }}
          scrollEventThrottle={8}>
            <View style={{height: 300 - heightHeader}} />
            <View
            style={{
              paddingHorizontal: 20,
              marginBottom: 20,
            }}>
              <Text title3 >
                  {data.title}
              </Text>
              <Text body2 semibold style={{marginTop: 20, marginBottom: 10}}>
                Content
              </Text>
              <Text body2 grayColor lineHeight={20}>
                {data.content}
              </Text>
              <Text body2 semibold style={{marginTop: 20, marginBottom: 10}}>
                Location
              </Text>
              <Text body2 grayColor lineHeight={20}>
                {data.location}
              </Text>
              <Text body2 semibold style={{marginTop: 10}}>
                Post at
              </Text>
              <Text body2 grayColor style={{marginTop: 10, marginBottom: 20}}>
                {data.createAt != -1 ? Utils.getDate(data.createdAt) : "Update Later"}
              </Text>
              <Text body2 semibold style={{marginTop: 10}}>
                Source
              </Text>
              <TouchableOpacity onPress={() => Linking.openURL(data.source)}>
                <Text body2 accentColor style={{marginTop: 10, marginBottom: 20}}>
                  {data.source}
                </Text>
              </TouchableOpacity>
              <Text body2 semibold style={{marginTop: 20, marginBottom: 10}}>
                Author
              </Text>
              <Text body2 grayColor lineHeight={20}>
                {data.author}
              </Text>
              <Text body2 semibold style={{marginTop: 20, marginBottom: 10}}>
                Category
              </Text>
              <Text body2 grayColor lineHeight={20}>
                {data.type}
              </Text>
          </View>
          <Button
            title="Edit Post"
            disabled={data.userCreated._id != me._id}
            onPress={() => {this.props.navigation.navigate("EditNews", data)}}
          />

          <Button
            title="Delete Post"
            disabled={data.userCreated._id != me._id}
            onPress={this.onHandleDelete}
          />
        </ScrollView>
      </SafeAreaView>
    </View>
    );
  }

}

const styles = StyleSheet.create({
  imgBanner: {
    width: '100%',
    height: 250,
    position: 'absolute',
  },
  rowBanner: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  userIcon: {
    width: 40,
    height: 40,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: 'white',
    marginRight: 5,
  },
  map: {
    // ...StyleSheet.absoluteFillObject,
  },
  contentButtonBottom: {
    borderTopWidth: 1,
    paddingVertical: 10,
    paddingHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  itemPrice: {
    borderBottomWidth: 1,
    paddingVertical: 10,
    alignItems: 'flex-start',
  },
  linePrice: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
  },
  iconRight: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  wrapContent: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    borderBottomWidth: 1,
    paddingBottom: 20,
  },
});

export default connect(
  state => ({me: state.me, post: state.news.post}),
  dispatch => channingActions({}, dispatch, bindMeActions, bindNewsActions, bindLoadingActions, bindDialogActions, bindToastNotificationActions)
)(NewsDetailScreen)