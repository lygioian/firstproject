import React from "react";
import { ScrollView, StyleSheet, Dimensions, Platform, TouchableWithoutFeedback  } from 'react-native';
import {
  Card, Block
} from 'galio-framework';
import theme from '../../constants/Theme';
import {API_URL} from '../../config'

const { width } = Dimensions.get('screen');

const styles = StyleSheet.create({
    cards: {
      width,
      backgroundColor: theme.COLORS.WHITE,
      alignItems: 'center',
      justifyContent: 'flex-start',
    },
    card: {
      backgroundColor: theme.COLORS.WHITE,
      width: width - theme.SIZES.BASE * 2,
      marginVertical: theme.SIZES.BASE * 0.875,
      elevation: theme.SIZES.BASE / 2,
    },
    full: {
      position: 'absolute',
      bottom: 0,
      right: 0,
      left: 0,
    },
    noRadius: {
      borderBottomLeftRadius: 0,
      borderBottomRightRadius: 0,
    },
    rounded: {
      borderRadius: theme.SIZES.BASE * 0.1875,
    },
    gradient: {
      bottom: 0,
      left: 0,
      right: 0,
      height: 90,
      position: 'absolute',
      overflow: 'hidden',
      borderBottomRightRadius: theme.SIZES.BASE * 0.5,
      borderBottomLeftRadius: theme.SIZES.BASE * 0.5,
    },
  });

class MyNewsScreen extends React.Component {
    constructor(props) {
        super(props);
    
        this.state = {
          username: "",
          password: "",
          locationReady: true,
          token: "",
          newsMenu: this.props.route.params,
    
          savedDataLoaded: false
        };
    } 

    render() {
      //console.error('You have reach the MyNews screen')
      const { newsMenu } = this.state;
      console.log("News: ", newsMenu)
      return (
        <Block safe flex style={{ backgroundColor: theme.COLORS.WHITE }}>
          <ScrollView contentContainerStyle={styles.cards}>
            <Block flex space="between">
              {newsMenu && newsMenu.map((post, id) => 
              { 
              // if (card.isChecking == false){
              //   return null;
              // }
              return (
              <TouchableWithoutFeedback
              key={`card-${post._id}`}
              onPress={() => this.props.navigation.navigate('EditNews', post)}>
                <Card
                  key={`card-${post._id}`}
                  flex
                  borderless
                  shadowColor={theme.COLORS.BLACK}
                  titleColor={ theme.COLORS.BLACK }
                  style={styles.card}
                  title={post.title}
                  caption={post.description}
                  // // location={card.location}
                  avatar={API_URL + post.userCreated.small}
                  image={API_URL + post.media.path.small}
                  imageStyle={ styles.rounded }
                  // imageBlockStyle={[
                  //   card.padded ? { padding: theme.SIZES.BASE / 2 } : null,
                  //   card.full ? null : styles.noRadius,
                  // ]}
                  // footerStyle={card.full ? styles.full : null}
                >
                </Card>
              </TouchableWithoutFeedback>
              )})}
            </Block>
          </ScrollView>
        </Block>
      );
    }
  }

export default MyNewsScreen;
