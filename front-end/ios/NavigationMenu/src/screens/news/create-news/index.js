import React from "react";
import {
  View,
  StyleSheet,
  KeyboardAvoidingView,
  ScrollView,
} from "react-native";
import {API_URL} from '../../../config'
import ImagePicker from "react-native-image-picker";
import { connect } from 'react-redux'
import {
  channingActions
} from '@lib'
import {
  bindMeActions
} from '@actions'
import {
  bindNewsActions,
  bindLoadingActions,
  bindDialogActions,
  bindToastNotificationActions
} from '@actions'
import {
  TextField,
  DateField,
  ImageField,
  Picker,
  Button
} from '@components'

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  information: {
    width: "100%",
    paddingTop: 20,
    paddingBottom: 20,
    backgroundColor: 'white'
  },
});

class CreateNews extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      title: "",
      author: "",
      source: "",
      type: "Post",
      location: "",
      writtenAt: "",
      content: "",
      media: API_URL+ "images/test",
      responseChangePhoto: null
    };
    this.choose = [
      {
        label: "Post",
        value: "Post"
      },
      {
        label: "Activity",
        value: "Activity"
      },
      {
        label: "Study",
        value: "Study"
      }
    ];

    // Navigation.events().bindComponent(this);

    this.changeProfilePhoto = this.changeProfilePhoto.bind(this);
  }

  changeProfilePhoto() {
    const { meActions } = this.props;

    const options = {
      title: "Select Photo",
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };
    
    ImagePicker.showImagePicker(options, async response => {
      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        this.setState({media: response.uri, responseChangePhoto: response})
      }
    });    
  }

  // get pagination() {
  //   const { element, index } = this.state;
  //   if (3 == 1)
  //     return (
  //       <View
  //         style={{
  //           height: 30,
  //           width: "100%",
  //           alignItems: "center",
  //           justifyContent: "center"
  //         }}
  //       >
  //         <View
  //           style={{
  //             width: 8,
  //             height: 8,
  //             borderRadius: 4,
  //             backgroundColor: "#7D153F"
  //           }}
  //         />
  //       </View>
  //     );
  //   else
  //     return (
  //       <Pagination
  //         dotsLength={3}
  //         activeDotIndex={1}
  //         tappableDots={true}
  //         containerStyle={{
  //           backgroundColor: "#fff",
  //           paddingVertical: 0,
  //           height: 30
  //         }}
  //         dotColor="#7D153F"
  //         inactiveDotColor="#7D153F"
  //         dotStyle={{
  //           width: 8,
  //           height: 8,
  //           borderRadius: 5,
  //           margin: 0,
  //           padding: 0,
  //           top: 0,
  //           backgroundColor: "rgba(0, 0, 0, 0.80)"
  //         }}
  //         inactiveDotStyle={
  //           {
  //             // Define styles for inactive dots here
  //           }
  //         }
  //         inactiveDotOpacity={0.4}
  //         inactiveDotScale={0.6}
  //       />
  //     );
  // }

  navigationButtonPressed = () =>  {
    const { newsActions, loadingActions, toastingActions } = this.props;
    loadingActions.show();
    const { 
    title,
    author,
    type,
    source,
    writtenAt,
    content,
    location,
    media,
    responseChangePhoto } = this.state;
    const data = {
      title: title,
      source: source,
      author: author,
      type: type,
      content: content,
      location: location,
      writtenAt: new Date(writtenAt).getTime(),
      media: responseChangePhoto
    }

    newsActions
    .createNews(data)
    .then((res) => {
      loadingActions.hide()
      toastingActions.show("Create post successfully", "bottom", "success")
      this.props.navigation.goBack();
    }).catch(rej => {
      loadingActions.hide()
      toastingActions.show("Error", "bottom", "error");
      console.log("error: ", rej)
    }); 
  
  }

  onButtonHandle = () => {
    const { toastingActions, dialogActions } = this.props;

    if(this.state.responseChangePhoto == null){
      toastingActions.show("You need choose the thumnail for the event", "bottom", "error");
      return null;
    }
    else if (this.state.title == "") {
      toastingActions.show("Title can not be empty", "bottom", "error")
      return null;
    }
    else if (this.state.content == "") {
      toastingActions.show("Content can not be empty", " bottom", "error")
      return null;
    }
    else {
      return dialogActions.show("createNews", "Do You want to create new post", "YES", "NO", this.navigationButtonPressed)
    }
  }

  render() {
    const placeholder = {
      label: "Select type ...",
      value: ""
    };

    return (
      <KeyboardAvoidingView
        style={styles.container}
        enabled
        key={this.state.timestamp}
      >
        <ScrollView>
          <ImageField
            onPress={this.changeProfilePhoto}
            source={this.state.media}
            title="Change Thumbnail"
          />

          <View style={styles.information}>
            <TextField
              title="Title"
              value={this.state.title}
              onChange={title => this.setState({ title })}
            />

            <TextField
              title="Author"
              value={this.state.author}
              onChange={author => this.setState({ author })}
            />

            <TextField
              title="Author"
              value={this.state.author}
              onChange={author => this.setState({ author })}
            />

            <DateField 
              title="Written At"
              value={this.state.writtenAt}
              onChange={writtenAt => this.setState({ writtenAt })}
            />

            <TextField
              title="Source"
              value={this.state.source}
              onChange={source => this.setState({ source })}
            />

            <TextField
              title="Location"
              value={this.state.location}
              onChange={location => this.setState({ location })}
            />

            <Picker 
              title="Type of News"
              arr={this.choose}
              onChange={value => {
                this.setState({ type: value });
              }}
              value={this.state.type}
              placeholder={placeholder}
            />

            <TextField
              title="Content"
              value={this.state.description}
              onChange={content => this.setState({ content })}
            />
          </View>
        </ScrollView>
        <View style={{
          paddingVertical: 15, 
          paddingHorizontal: 15,
        }}>
          <Button
            title="Confirm"
            onPress={this.onButtonHandle}
          />
        </View>
      </KeyboardAvoidingView>
    );
  }
}

// export default CreateEvent;

export default connect(
  state => ({me: state.me}),
  dispatch => channingActions({}, dispatch, bindMeActions, bindNewsActions, bindLoadingActions, bindToastNotificationActions, bindDialogActions)
)(CreateNews)