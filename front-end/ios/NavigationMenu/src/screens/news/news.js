import React from 'react';
import { StyleSheet, Dimensions, ScrollView,RefreshControl,TouchableOpacity } from 'react-native';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { Button, Block, Text, theme } from 'galio-framework';
import {
  bindNewsActions,
  bindMeActions,
  bindLoadingActions
} from '@actions'
import { connect } from 'react-redux'
import {
  channingActions
} from '@lib'
import Product from './components/Product';

const { width } = Dimensions.get('screen');
const screenWidth = Math.round(Dimensions.get("window").width);
const screenHeight = Math.round(Dimensions.get("window").height);

class NewsScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: "",
      locationReady: true,
      token: "",
      savedDataLoaded: false,
      onScrollBeginPos: 0,
      refreshing: false
    };
  }

  componentDidMount() {
    //console.log("Did it mount123");
    const { newsActions, loadingActions } = this.props;
    loadingActions.show()
    newsActions.getNews().then(res => {
      loadingActions.hide()
    }).catch(rej => {
      loadingActions.hide()
      console.log(rej)
    })
  }

  _onEndReach = () => {
    // if (this.state.search != "") return null;
    // const { postActions } = this.props;
    // postActions.fetchNearBy();
  };

  onScrollUp = () => {
    console.log("onScrollUp");
    const { newsActions, loadingActions } = this.props;
    loadingActions.show()
    newsActions.getNews().then(res => {
      loadingActions.hide()
    })
  };

  onScrollDown = () => {
    console.log("onScrollDown");
    const { newsActions, loadingActions } = this.props;
    loadingActions.show()
    newsActions.getNews().then(res => {
      loadingActions.hide()
    })
  };

  _onRefresh = () => {
    console.log("onRefresh");
    const { newsActions, loadingActions } = this.props;
    loadingActions.show()
    newsActions.getNews().then(res => {
      loadingActions.hide()
    })
  }

  // renderSearch = () => {
  //   const { navigation } = this.props;
  //   const iconCamera = <Icon size={16} color={theme.COLORS.MUTED} name="zoom-in" family="material" />

  //   return (
  //     <Input
  //       right
  //       color="black"
  //       style={styles.search}
  //       iconContent={iconCamera}
  //       placeholder="What are you looking for?"
  //       onFocus={() => navigation.navigate('Pro')}
  //     />
  //   )
  // }
  
  renderTabs = () => {
    const { navigation } = this.props;

    return (
      <Block row style={styles.tabs}>
        <Button shadowless style={[styles.tab, styles.divider]} onPress={() => navigation.navigate('Pro')}>
          <Block row middle>
            <Icon name="grid" family="feather" style={{ paddingRight: 8 }} />
            <Text size={16} style={styles.tabTitle}>Categories</Text>
          </Block>
        </Button>
        <Button shadowless style={styles.tab} onPress={() => navigation.navigate('Pro')}>
          <Block row middle>
            <Icon size={16} name="camera-18" family="GalioExtra" style={{ paddingRight: 8 }} />
            <Text size={16} style={styles.tabTitle}>Best Deals</Text>
          </Block>
        </Button>
      </Block>
    )
  }

  navigationButtonPressed = () =>  {
    this.props.navigation.navigate("CreateNews")
  }

  render() {
    const isRefreshPostNearby = false;
    const { navigation } = this.props;

    const isCloseToBottom = ({
      layoutMeasurement,
      contentOffset,
      contentSize
    }) => {
      const paddingToBottom = 100;
      return (
        layoutMeasurement.height + contentOffset.y >=
        contentSize.height - paddingToBottom
      );
    };

    this.props.navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity style={styles.iconContainer} onPress={this.navigationButtonPressed}>
          <Icon name="pencil-plus-outline"  size={25}/>
        </TouchableOpacity>
      )
    });

    return (
      <Block flex center style={styles.home}>
        <ScrollView
        showsVerticalScrollIndicator={false}
        onScrollBeginDrag={({ nativeEvent }) => {
          this.setState({ onScrollBeginPos: nativeEvent.contentOffset.y });
        }}
        // // onScrollEndDrag={({ nativeEvent }) => {
        // //   const y = nativeEvent.contentOffset.y;
        // //   if (y - this.state.onScrollBeginPos >= 100) {
        // //     this.onScrollUp();
        // //   }
        // //   if (y - this.state.onScrollBeginPos <= -100) {
        // //     this.onScrollDown();
        // //   }
        // }}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
          />
        }
        onScroll={({ nativeEvent }) => {
          if (isCloseToBottom(nativeEvent) && !isRefreshPostNearby) {
            this._onEndReach();
          }
        }}
        scrollEventThrottle={400}
        contentContainerStyle={styles.products}>
          <Block flex key ="blockOfNews">
          {
            this.props.news.newsfeed.data.map((element, index) => {
              switch (index % 5) {
                case 0: return <Product navigation ={navigation}  product={element} horizontal key={index}/>
                case 1: return (
                  <Block flex row>
                    <Product  navigation ={navigation} key={index} product={element} style={{ marginRight: theme.SIZES.BASE }} />
                    {element.length > index + 1 ?
                    <Product navigation ={navigation} key={index} product={this.state.data[index+1]} /> : null
                    }
                  </Block>
                )
                case 2: return <></>;
                case 3: return <Product  navigation ={navigation}  key={index} product={element} horizontal />
                case 4: return <Product  navigation ={navigation} key={index} product={element} full />
        
              }
            })
          }
          </Block>
        </ScrollView>
      </Block>
    );
  }
  
}const styles = StyleSheet.create({
  home: {
    width: width,    
  },
  search: {
    height: 48,
    width: width - 32,
    marginHorizontal: 16,
    borderWidth: 1,
    borderRadius: 3,
  },
  header: {
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 8,
    shadowOpacity: 0.2,
    elevation: 4,
    zIndex: 2,
  },
  tabs: {
    marginBottom: 24,
    marginTop: 10,
    elevation: 4,
  },
  tab: {
    backgroundColor: theme.COLORS.TRANSPARENT,
    width: width * 0.50,
    borderRadius: 0,
    borderWidth: 0,
    height: 24,
    elevation: 0,
  },
  tabTitle: {
    lineHeight: 19,
    fontWeight: '300'
  },
  divider: {
    borderRightWidth: 0.3,
    borderRightColor: theme.COLORS.MUTED,
  },
  products: {
    width: width - theme.SIZES.BASE * 2,
    paddingVertical: theme.SIZES.BASE * 2,
  },
});

export default connect(
  state => ({news: state.news}),
  dispatch => channingActions({}, dispatch, bindNewsActions, bindLoadingActions)
)(NewsScreen)
