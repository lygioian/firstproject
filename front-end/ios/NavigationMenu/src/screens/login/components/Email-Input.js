import React, { Component } from 'react';
import {
  StyleSheet,
  Dimensions,
} from 'react-native';
import { Input, Icon } from 'react-native-elements';

class EmailInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      videoPaused: true
    };
  }

  render() {
    return (
        <Input
            leftIcon={
            <Icon
                name="envelope-o"
                type="font-awesome"
                color="rgba(0, 0, 0, 0.38)"
                size={25}
                style={{ backgroundColor: 'transparent' }}
            />
            }
            value={email}
            keyboardAppearance="light"
            autoFocus={false}
            autoCapitalize="none"
            autoCorrect={false}
            keyboardType="email-address"
            returnKeyType="next"
            inputStyle={{ marginLeft: 10 }}
            placeholder={'Email'}
            containerStyle={{
            borderBottomColor: 'rgba(0, 0, 0, 0.38)',
            }}
            ref={input => (this.emailInput = input)}
            onSubmitEditing={() => this.passwordInput.focus()}
            onChangeText={email => this.setState({ email })}
            errorMessage={
            isEmailValid ? null : 'Please enter a valid email address'
            }
        />
    );
  }
}

export default Post;
const styles = StyleSheet.create({
  container: {
    width: "100%",
    padding: 2,
    borderColor: "#7D153F",
    borderWidth: 1,
    borderRadius: 7,
    marginBottom: 10
  },
  backgroundImage: {
    width: "100%",
    justifyContent: "space-between",
    backgroundColor: "rgba(0,0,0,0.35)",
    marginBottom: 5,
  },
  placeName: {
    color: "white",
    marginTop: 5,
    marginLeft: 5,
    fontWeight: "bold"
  },
  centerView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  postInfo: {
    paddingHorizontal: 5,
    flexDirection: "row",
    width: "100%",
    alignItems: "center"
  },
  textWhite: { color: "white", fontSize: 15 },
  userName: { fontWeight: "bold", color: "black" },
  row: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  peopleInLocation: {
    flexDirection: "row",
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center"
  }
});
