import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  Image,
  ImageBackground,
  Dimensions,
  LayoutAnimation,
  UIManager,
  KeyboardAvoidingView,
  Text
} from 'react-native';
import { Input, Button, Icon } from 'react-native-elements';
import { connect } from 'react-redux'
import {
  channingActions
} from '@lib';
import {
  bindMeActions,
  bindEventsActions,
  bindLoadingActions,
  bindToastNotificationActions
} from '@actions';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

const BG_IMAGE = require('../../assets/images/Youthava.png');
const BG = require('../../assets/images/login_background.jpg')

// Enable LayoutAnimation on Android
UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true);

const TabSelector = ({ selected }) => {
  return (
    <View style={styles.selectorContainer}>
      <View style={selected && styles.selected} />
    </View>
  );
};

TabSelector.propTypes = {
  selected: PropTypes.bool.isRequired,
};

class LoginScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      isEmailValid: true,
      isPasswordValid: true,
    };
    this.login = this.login.bind(this);
    this.signUp = this.signUp.bind(this);
  }

  validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    return re.test(email);
  }

  login(isAdmin) {
    const { meActions, loadingActions, toastingActions } = this.props
    const { email, password } = this.state;
    loadingActions.show();
    this.setState({ isLoading: true });
    // Simulate an API call
    console.log("Login")
    meActions.login(email,password, isAdmin).then(res => {
        this.setState({email: '', password: ''});
        loadingActions.hide();
        toastingActions.show("Log in successfully", "bottom", "success")
      }).catch(err => {
        console.log("Error:" ,err)
        loadingActions.hide();
        setTimeout(() => {
          this.setState({
            isPasswordValid: password.length >= 8 || this.passwordInput.shake() || this.emailInput.shake(),
          });
        }, 500);
      }).finally(() => {
        LayoutAnimation.easeInEaseOut();
        this.setState({isLoading: false});
      })
  }
  // componentDidUpdate() {
  //   const {me, eventsActions} = this.props;
  //   if(me.token != "" && me.information){
      
  //   }
  // }
  signUp() {
    const { email, password, passwordConfirmation } = this.state;
    const { meActions, loadingActions } = this.props
    loadingActions.show()
    this.setState({ isLoading: true });
    if(password != passwordConfirmation) {
      LayoutAnimation.easeInEaseOut();
      this.setState({
        isLoading: false,
        isPasswordValid: password.length >= 8 || this.passwordInput.shake() || this.emailInput.shake(),
      });
      loadingActions.hide()
      return;
    }
    // Simulate an API call
    meActions.signup(email,password).then(res => {
        console.log("Token: ", res.data.token);
        console.log("Login success")
        loadingActions.hide()
        toastingActions.show("Log in successfully", "bottom", "success")
        this.props.navigation.navigate('MainTab');
      }).catch(err => {
        console.log("Error:" ,err)
        setTimeout(() => {
          this.setState({
            isPasswordValid: password.length >= 8 || this.passwordInput.shake() || this.emailInput.shake(),
          });
        }, 500);
      }).finally(() => {
        LayoutAnimation.easeInEaseOut();
        this.setState({isLoading: false});
      })
  }

  render() {
    const {
      isLoading,
      isEmailValid,
      isPasswordValid,
      email,
      password,
      passwordConfirmation,
    } = this.state;
    return (
      <View style={styles.container}>
        <ImageBackground source={BG} style={styles.bgImage}>
          <View>
            <View style={styles.titleContainer}>
              <Button
                title={'BK-OS'}
                titleStyle={{ fontSize: 30, fontWeight: 'bold', color: '#4F4F4F'}}
                buttonStyle={{ backgroundColor: 'transparent' }}
                underlayColor="transparent"
                onPress={() => console.log('Account created')}
              />
            </View>
            <View style={styles.logoContainer}>
              <Image
                style={styles.itemPicture}
                source={BG_IMAGE}
                resizeMode="cover"
              />
            </View>
            <KeyboardAvoidingView
              contentContainerStyle={styles.loginContainer}
              behavior="position"
            >
              <View style={{ flexDirection: 'row' }}>
              </View>
              <View style={styles.formContainer}>
                <Input
                  leftIcon={
                    <Icon
                      name="envelope-o"
                      type="font-awesome"
                      color="rgba(0, 0, 0, 0.38)"
                      size={25}
                      style={{ backgroundColor: 'transparent' }}
                    />
                  }
                  value={email}
                  keyboardAppearance="light"
                  autoFocus={false}
                  autoCapitalize="none"
                  autoCorrect={false}
                  keyboardType="email-address"
                  returnKeyType="next"
                  inputStyle={styles.text}
                  placeholder={'Email'}
                  containerStyle={{
                    borderBottomColor: 'rgba(0, 0, 0, 0.38)',
                  }}
                  ref={input => (this.emailInput = input)}
                  onSubmitEditing={() => this.passwordInput.focus()}
                  onChangeText={email => this.setState({ email })}
                  errorMessage={
                    isEmailValid ? null : 'Please enter a valid email address'
                  }
                />
                <Input
                  leftIcon={
                    <Icon
                      name="lock"
                      type="simple-line-icon"
                      color="rgba(0, 0, 0, 0.38)"
                      size={25}
                      style={{ backgroundColor: 'transparent' }}
                    />
                  }
                  value={password}
                  keyboardAppearance="light"
                  autoCapitalize="none"
                  autoCorrect={false}
                  secureTextEntry={true}
                  returnKeyType={'done'}
                  blurOnSubmit={true}
                  containerStyle={{
                    marginTop: 16,
                    borderBottomColor: 'rgba(0, 0, 0, 0.38)',
                  }}
                  inputStyle={styles.text}
                  placeholder={'Password'}
                  ref={input => (this.passwordInput = input)}
                  onSubmitEditing={() =>
                    this.login()
                  }
                  onChangeText={password => this.setState({ password })}
                  errorMessage={
                    isPasswordValid
                      ? null
                      : 'Invalid username or password'
                  }
                />
                <Button
                  buttonStyle={styles.loginButton}
                  containerStyle={{ marginTop: 32, width: '100%' }}
                  activeOpacity={0.8}
                  title={'Log In'}
                  onPress={() => {this.login(true)}}
                  titleStyle={styles.loginTextButton}
                  loading={isLoading}
                  disabled={isLoading}
                />
              </View>
            </KeyboardAvoidingView>
            {/* <View style={styles.helpContainer}>
              <Button
                title={'Need help ?'}
                titleStyle={{ color: 'black' }}
                buttonStyle={{ backgroundColor: 'transparent' }}
                underlayColor="transparent"
                onPress={() => console.log('Account created')}
              />
            </View> */}
          </View>
        </ImageBackground>
      </View>
    );
  }
}

export default connect(
  state => ({me: state.me}),
  dispatch => channingActions({}, dispatch, bindMeActions, bindEventsActions, bindLoadingActions, bindToastNotificationActions)
)(LoginScreen)

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  loginContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleContainer: {
    // height: 100, 
    fontSize: 30,
    fontWeight: 'bold',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 30,
    borderColor: 'rgba(0, 0, 0, 0)'
  },
  selectorContainer: {
    flex: 1,
    alignItems: 'center',
  },
  formContainer: {
    width: SCREEN_WIDTH - 100,
    borderRadius: 10,
    paddingTop: 32,
    paddingBottom: 32,
    alignItems: 'center',
  },
  itemPicture: {
    width: 160,
    height: 160,
    borderRadius: 80,
    borderWidth: 2,
  },
  selected: {
    position: 'absolute',
    borderRadius: 50,
    height: 0,
    width: 0,
    top: -5,
    borderRightWidth: 70,
    borderBottomWidth: 70,
    borderColor: 'white',
    backgroundColor: 'white',
  },
  loginTextButton: {
    fontSize: 16,
    color: '#4F4F4F',
    fontWeight: 'bold',
  },
  loginButton: {
    backgroundColor: '#DCDCDC',
    borderRadius: 10,
    width: '100%',
  },
  bgImage: {
    top: 0,
    left: 0,
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
    paddingTop: 30,
    alignItems: 'center',
  },
  helpContainer: {
    height: 10, 
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: '#4F4F4F',
    fontSize: 18,
    paddingLeft: 30,
  }
});