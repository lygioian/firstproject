import React from "react";
import {
  View,
  StyleSheet,
  KeyboardAvoidingView,
  ScrollView,
} from "react-native";
import { connect } from 'react-redux'
import ImagePicker from "react-native-image-picker";
import {
  channingActions
} from '@lib'
import {
  bindMeActions,
  bindEventsActions,
  bindLoadingActions,
  bindToastNotificationActions,
  bindDialogActions
} from '@actions/'
import {API_URL} from '../../../config'
import {
  TextField,
  DateField,
  ImageField,
  Button,
  Picker,
  CheckBox
} from '@components'

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  information: {
    width: "100%",
    paddingTop: 20,
    paddingBottom: 20,
    backgroundColor: 'white'
  },
});


class CreateEventScreen extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      title: "",
      description: "",
      socialDays: "",
      maxRegister: "",
      eventAddress: "",
      eventUnit: "",
      type: "Normal",
      requirements: "",
      placeGather: "",
      formStart: null,
      formEnd: null,
      eventStart: null,
      eventEnd: null,
      timeGather: null,
      isUrgent: false,
      media: API_URL + "images/test",
      responseChangePhoto: null
    };
    this.choose = [
      {
        label: "Normal",
        value: "normal"
      },
      {
        label: "Special",
        value: "special"
      }
    ];
    this.changeProfilePhoto = this.changeProfilePhoto.bind(this);
  }

  changeProfilePhoto() {
    const { meActions, loadingActions } = this.props;

    const options = {
      title: "Select Photo",
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };
    
    ImagePicker.showImagePicker(options, async response => {
      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        this.setState({media: response.uri, responseChangePhoto: response})
      }
    });    
  }

  navigationButtonPressed = () =>  {
    const { eventsActions, loadingActions, toastingActions } = this.props;
    const { 
    title,
    description,
    socialDays,
    maxRegister,
    eventAddress,
    eventUnit,
    type,
    requirements,
    placeGather,
    formStart,
    formEnd,
    eventStart,
    eventEnd,
    timeGather,
    isUrgent,
    responseChangePhoto, } = this.state;
    const data = {
      title: title,
      description: description,
      socialDays: socialDays,
      maxRegister: maxRegister,
      eventAddress: eventAddress,
      eventUnit: eventUnit,
      type: type,
      requirements: requirements,
      placeGather: placeGather,
      formStart: new Date(formStart).getTime(),
      formEnd: new Date(formEnd).getTime(),
      eventStart: new Date(eventStart).getTime(),
      eventEnd: new Date(eventEnd).getTime(),
      timeGather: new Date(timeGather).getTime(),
      isUrgent: isUrgent,
      media: responseChangePhoto
    }
    if(this.state.responseChangePhoto == null){
      return;
    }

    console.log("Event created data: ", data);
    loadingActions.show()
    eventsActions
    .createEvent(data)
    .then((res) => {
      loadingActions.hide()
      toastingActions.show("Create event successfully", "bottom", "success")
      this.props.navigation.goBack();
    }).catch(rej => {
      console.log("Error in navigationButton in create-event: ", rej, rej.response);
      toastingActions.show("Error", "bottom", "error");
      loadingActions.hide()
    })
  }

  onButtonHandle = () => {
    const { toastingActions, dialogActions } = this.props;

    if(this.state.responseChangePhoto == null){
      toastingActions.show("You need choose the image for the event", "bottom", "error");
      return null;
    }
    else if (this.state.title == "") {
      toastingActions.show("Title can not be empty", "bottom", "error")
      return null;
    }
    else if (this.state.socialDays == "") {
      toastingActions.show("Social Days can not be empty", " bottom", "error")
      return null;
    }
    else if (this.state.maxRegister == "") {
      toastingActions.show("Max Register can not be empty", "bottom", "error")
      return null;
    }
    else if (this.state.description == "") {
      toastingActions.show("Description can not be empty", "bottom", "error")
      return null;
    }
    else if (this.state.formStart == null) {
      toastingActions.show("Time for posting form can not be empty", "bottom", "error")
      return null;
    }
    else if (this.state.formEnd == null) {
      toastingActions.show("Time for closing form can not be empty", "bottom", "error")
      return null;
    }
    else {
      return dialogActions.show("CreateEvent", "Do You want to create new event", "YES", "NO", this.navigationButtonPressed, () => {})
    }
  }

  render() {
    const placeholder = {
      label: "Select type ...",
      value: ""
    };

    return (
      <KeyboardAvoidingView
        style={styles.container}
        // keyboardVerticalOffset={100}
        // behavior="padding"
        enabled
        key={this.state.timestamp}
      >
        <ScrollView>
          <ImageField
            onPress={this.changeProfilePhoto}
            source={this.state.media}
            title="Change Thumbnail"
          />

          <View style={styles.information}>
            <TextField
              title="Title"
              value={this.state.title}
              onChange={title => this.setState({ title })}
            />

            <TextField
              title="Social Days"
              value={this.state.socialDays}
              onChange={socialDays => this.setState({ socialDays })}
              isNumberic={true}
            />

            <TextField
              title="Max Register"
              value={this.state.maxRegister}
              onChange={maxRegister => this.setState({ maxRegister })}
              isNumberic={true}
            />

            <TextField
              title="Description"
              value={this.state.description}
              onChange={description => this.setState({ description })}
            />

            <TextField
                title="Place Occur"
                value={this.state.eventAddress}
                onChange={eventAddress => this.setState({ eventAddress })}
            />

            <TextField
                title="Unit held"
                value={this.state.eventUnit}
                onChange={eventUnit => this.setState({ eventUnit })}
            />

            <DateField 
              title="Event Start At"
              value={this.state.eventStart}
              onChange={eventStart => this.setState({ eventStart })}
            />

            <DateField 
              title="Event End At"
              value={this.state.eventEnd}
              onChange={eventEnd => this.setState({ eventEnd })}
            />

            <TextField
              title="Requirements"
              value={this.state.requirements}
              onChange={requirements => this.setState({ requirements })}
            />

            <DateField 
              title="Time Gather"
              value={this.state.timeGather}
              onChange={timeGather => this.setState({ timeGather })}
            />

            <TextField
              title="Place Gather"
              value={this.state.placeGather}
              onChange={placeGather => this.setState({ placeGather })}
            />

            <DateField 
              title="Posting Form At"
              value={this.state.formStart}
              onChange={formStart => this.setState({ formStart })}
            />

            <DateField 
              title="Closing Form At"
              value={this.state.formEnd}
              onChange={formEnd => this.setState({ formEnd })}
            />

            <CheckBox
              title="Is this event urgent ?"
              value={this.state.isUrgent}
              onChange={() => this.setState({isUrgent: !this.state.isUrgent})}
              name={this.state.isUrgent ? "Yes" : "No"}
            />

            <Picker 
              title="Type of Event"
              arr={this.choose}
              onChange={value => {
                this.setState({ type: value });
              }}
              value={this.state.type}
              placeholder={placeholder}
            />
          </View>
        </ScrollView>
        <View style={{
          paddingVertical: 15, 
          paddingHorizontal: 20,
        }}>
          <Button
            title="Confirm"
            onPress={this.onButtonHandle}
          />
        </View>
      </KeyboardAvoidingView>
    );
  }
}

export default connect(
  state => ({me: state.me}),
  dispatch => channingActions({}, dispatch, bindMeActions, bindEventsActions, bindLoadingActions, bindToastNotificationActions, bindDialogActions)
)(CreateEventScreen)