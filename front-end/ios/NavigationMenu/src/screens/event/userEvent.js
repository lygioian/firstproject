import React from "react";
import { View, StyleSheet, ScrollView, RefreshControl, FlatList } from "react-native";
import {
  Block
} from 'galio-framework';
import theme from '../../constants/Theme';
import Post from "./components/Post";
import _ from 'lodash'
import {
  bindEventsActions,
  bindLoadingActions
} from '@actions'
import { connect } from 'react-redux'
import {
  channingActions
} from '@lib'
import {
  Select
} from '@components';

class UserEvent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
      isLoading: false,
      offsetId: 4,
      onScrollBeginPos: 0,
      refreshing: false,
      typeOfFilter: 'Available events',
      initial: true
    };

  }

  updateSearch = search => {
    this.setState({ search });
  };

  componentDidMount() {
    const {eventsActions, loadingActions} = this.props;
    loadingActions.show()
    eventsActions.getEvents({type: 1}, false).then(res => {
      loadingActions.hide()
    }).catch(rej => {
      console.log("Error in getAllEvents: ", rej)
    })
  }

  // componentWillReceiveProps(newProps) {
  //   if ((newProps.index == 0 && !_.isEqual(newProps.events.newsfeed, this.props.events.newsfeed)) || this.state.initial) {
  //     const {eventsActions, loadingActions} = this.props;
  //     loadingActions.show()
  //     console.log("Component Will Receive Props in user")
  //     this.setState({initial: false})
  //     eventsActions.getEvents({limit: 4}, false).then(res => {
  //       console.log(this.props.events.newsfeed.data)
  //       loadingActions.hide()
  //     }).catch(rej => {
  //       console.log("Error in getAllEvents: ", rej)
  //     })
  //   }
  // }

  shouldComponentUpdate(newProps, newState) {
    if ((newState.typeOfFilter == this.state.typeOfFilter && newProps.index == this.props.index && _.isEqual(newProps.events.newsfeed, this.props.events.newsfeed)) || newProps.index != 0) {
      console.log("new props: ", newProps)
      return false
    }
    else {
      return true
    }
  }

  // componentWillUpdate(nextProps, nextState) {
  //   console.log("Component Will Update")
  //   const {eventsActions, loadingActions} = this.props;
  //   loadingActions.show()
  //   switch (this.state.typeOfFilter) {
  //     case "All events":
  //       if (!_.isEqual(this.state.data, this.props.events.newsfeed.data)) {
  //         eventsActions.getEvents({limit: 4}, false).then(res => {
  //         }).catch(rej => {
  //           console.log("Error in getAllEvents: ", rej)
            
  //         })
  //         this.setState({data : this.props.events.newsfeed.data})
  //       }
  //       break;
  //     case "My events":
  //       if (!_.isEqual(this.state.data, this.props.events.eventUserCreated.data)) {
  //         eventsActions.getEvents({constraint: 1, limit: 4}, false).then(res => {
  //         }).catch(rej => {
  //         console.log("Error in getUserCreated: ", rej)
  //         })
  //         this.setState({data : this.props.events.eventUserCreated.data})
  //       }
  //       break;
  //     case "Available events":
  //       if (!_.isEqual(this.state.data, this.props.events.newsfeed.data)) {
  //         eventsActions.getEvents({type: 1, limit: 4}, false).then(res => {
  //         }).catch(rej => {
  //           console.log("Error in getAllEvents: ", rej)
  //         })
  //         this.setState({data : this.props.events.newsfeed.data})
  //       }
  //       break;
  //     default:
  //       if (!_.isEqual(this.state.data, this.props.events.newsfeed.data)) {
  //         eventsActions.getEvents({limit: 4}, false).then(res => {
  //         }).catch(rej => {
  //           console.log("Error in getAllEvents: ", rej)
  //         })
  //         this.setState({data : this.props.events.newsfeed.data})
  //       }
  //   }
  //   loadingActions.hide()
  // }

  _onRefresh = () => {
    console.log("On refresh")
    //this.setState({ refreshing: true });
    const { eventsActions } = this.props;
    if (this.state.typeOfFilter == "Available events") {
      eventsActions.getEvents({type: 1}, false).then(res => {
      // this.setState({ refreshing: false });
      });
    } else if (this.state.typeOfFilter == "All events") {
      eventsActions.getEvents({}, false).then(res => {
        // this.setState({ refreshing: false });
        });
    }
  };

  componentDidAppear() {
    
    // const { eventsActions } = this.props;
    // eventsActions.getAllEvents(true);
    // const { postActions } = this.props;
    // this.setState({ refreshing: true });
    // postActions.fetchNearBy(true).then(() => {
    //   this.setState({ refreshing: false });
    // });
  }
  
  _onEndReach = () => {
    // if (this.state.search != "") return null;
    // const { postActions } = this.props;
    // postActions.fetchNearBy();
  };

  requestSearch(keyword) {
    // const { postActions } = this.props;
    // if (!keyword) {
    //   postActions.fetchNearBy(true);
    // } else {
    //   postActions.getPostByKeyword(keyword).then(response => {
    //     this.setState({ places: response });
    //   });
    // }
  }

  onScrollUp = () => {
    console.log("onScrollUp");
    const { eventsActions } = this.props;
    eventsActions.getEvents({limit: 4, type: 1}, false).then(res => {
    });
  };

  onScrollDown = () => {
    
    const { eventsActions } = this.props;
    console.log("Scrolling down")
    eventsActions.getEvents({limit: 4, type: 1}, false).then(res => {
    });
  };

  handleFilterSelect = (index, value) => {
    console.log("Handle FIlter Select")
    const {eventsActions, loadingActions} = this.props;
    switch (value) {
      case "Available events":
        eventsActions.getEvents({type: 1}, false).then(res => {
          this.setState({typeOfFilter: value})
        }).catch(rej => {
          console.log("Error in getAllEvents: ", rej)
        })
        break;
      case "All events":
        eventsActions.getEvents({}, false).then(res => {
          this.setState({typeOfFilter: value})
        }).catch(rej => {
          console.log("Error in getAllEvents: ", rej)
        })
        break;
      default:
        eventsActions.getEvents({type: 1}, false).then(res => {
          this.setState({typeOfFilter: value})
        }).catch(rej => {
          console.log("Error in getAllEvents: ", rej)
        })
    }
  }

  renderTypeOfChecking = () => (
    <Block row center style={{ marginTop: theme.SIZES.BASE, marginBottom: theme.SIZES.BASE }}>
        <Select
          value = {this.state.typeOfFilter}
          defaultIndex={0}
          defaultValue={this.state.typeOfFilter}
          handleOnSelect = {this.handleFilterSelect}
          options={["Available events", "All events"]}
          style={styles.shadow}
        />
    </Block>
  )

  render() {
    // const { posts, isRefreshPostNearby } = this.props;
    const {data} = this.props.events.newsfeed;
    console.log("User data: ", data)
    console.log("render in user");
    
    const isRefreshPostNearby = false;
    let isFitLeft = true;
    let isFitRight = false;

    if(data === null || data === undefined){
      this.onScrollDown;
    }
    const isCloseToBottom = ({
      layoutMeasurement,
      contentOffset,
      contentSize
    }) => {
      const paddingToBottom = 100;
      return (
        layoutMeasurement.height + contentOffset.y >=
        contentSize.height - paddingToBottom
      );
    };
    return (
      <View style={styles.container}>
        {/* <FlatList style={{margin:5}}
        data={data}
        numColumns={2}
//        columnWrapperStyle={{justifyContent: "space-around"}}  // space them out evenly

        keyExtractor={(item, index) => item._id }
        renderItem={(data, index) => 
          <Post
          fitHeight={true}
          key={data.index}
          data={data.item}
          navigation={this.props.navigation}
        /> }
        /> */}
        
        <ScrollView
          showsVerticalScrollIndicator={false}
          onScrollBeginDrag={({ nativeEvent }) => {
            this.setState({ onScrollBeginPos: nativeEvent.contentOffset.y });
          }}
          onScrollEndDrag={({ nativeEvent }) => {
            const y = nativeEvent.contentOffset.y;
            // if (y - this.state.onScrollBeginPos >= 100) {
            //   this.onScrollUp();
            // }
            // if (y - this.state.onScrollBeginPos <= -100) {
            //   this.onScrollDown();
            // }
          }}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }
          onScroll={({ nativeEvent }) => {
            if (isCloseToBottom(nativeEvent) && !isRefreshPostNearby) {
              this._onEndReach();
            }
          }}
          scrollEventThrottle={400}
        >
          {this.renderTypeOfChecking()}
          <View style={styles.postsContainer}>
            <View style={styles.col}>
              {data && data.map((post, index) => {
                if (index % 2 == 0) {
                  isFitLeft = !isFitLeft;
                  return (
                    <Post
                      parentScreenId={this.props.parentScreenId}
                      fitHeight={isFitLeft}
                      key={index}
                      data={post}
                      me={this.props.me}
                      navigation={this.props.navigation}
                    />
                  );
                }
              })}
            </View>
            <View style={styles.col}>
              {data && data.map((post, index) => {
                if (index % 2 == 1) {
                  isFitRight = !isFitRight;
                  return (
                    <Post
                      parentScreenId={this.props.parentScreenId}
                      fitHeight={isFitRight}
                      key={index}
                      data={post}
                      me={this.props.me}
                      navigation={this.props.navigation}
                    />
                  );
                }
              })}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    paddingHorizontal: 10,
    paddingTop: 10
  },
  col: {
    width: "49%",
    alignItems: "center"
  },
  postsContainer: {
    flexDirection: "row",
    justifyContent: "space-between"
  }
});


export default connect(
  state => ({events: state.events, me: state.me}),
  dispatch => channingActions({}, dispatch, bindEventsActions, bindLoadingActions)
)(UserEvent)


// export default GeneralEvent;