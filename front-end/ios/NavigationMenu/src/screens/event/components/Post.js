import React from "react";
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
} from "react-native";
import {
  PEOPLE_WHITE_COLOR,
} from "../../../assets/images";
import { ProgressiveImage } from "@ui";
import {
  Text
} from '@components'
import {API_URL} from '../../../config'
import Utils from '../../../helpers/Utils'

class Post extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      videoPaused: true
    };
  }

  render() {
    const {navigate} = this.props.navigation;
    const { fitHeight, data } = this.props;

    const height = 2/ 3;
    const day = new Date().getTime();
    const imageBackground = {
      uri: API_URL + data.createdForm.media.path.small
    };
    return (
      <TouchableOpacity style={styles.container} onPress={() => navigate('EventDetail', data._id)}>
        <View style={{borderColor: "#7D153F"}}>
          <ProgressiveImage
            thumbnailSource={imageBackground}
            source={{
              uri: API_URL + data.createdForm.media.path.original
            }}
            resizeMode="cover"
            style={[
              styles.backgroundImage,
              {
                aspectRatio: height,
                position: "absolute",
                borderRadius: 5
              }
            ]}
            imageStyle={{ borderRadius: 7 }}
          />
        </View>
        <View style={[styles.backgroundImage, { aspectRatio: height }]}>
            <Text headline semibold style={styles.placeName} numberOfLines={2}>
              {data.createdForm.title}
            </Text>

          <View style={styles.postInfo}>
            <Text style={styles.textWhite}>{data.eventStart != -1 ? Utils.getDate(data.createdForm.eventStart) : "Update Later"}</Text>
            <View style={styles.peopleInLocation} >
              <Image
                source={PEOPLE_WHITE_COLOR}
                resizeMode="contain"
                style={{ width: 15, marginLeft: 3, marginRight: 3 }}
              />
            </View>
          </View>
        </View>

        <View style={{ flexDirection: "row" }}>
          <View style={{ marginLeft: 5, flex: 1 }}>
            <View style={styles.row}>
              <View style={{ width: "60%" }}>
                <Text style={styles.userName} numberOfLines={1}>
                  {data.userCreated ? data.userCreated.name : "Bách Khoa"}
                </Text>
              </View>
              <View>
                <Text numberOfLines={1}>
                  {Utils.formatTime(data.createdForm.formEnd)}
                </Text>
              </View>
            </View>
            <Text numberOfLines={1}>{data.createdForm.description}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

export default Post;
const styles = StyleSheet.create({
  container: {
    width: "100%",
    padding: 2,
    borderColor: "#7D153F",
    borderWidth: 1,
    borderRadius: 7,
    marginBottom: 10
  },
  backgroundImage: {
    width: "100%",
    justifyContent: "space-between",
    backgroundColor: "rgba(0,0,0,0.35)",
    marginBottom: 5,
  },
  placeName: {
    color: "white",
    marginTop: 5,
    marginLeft: 5,
    fontWeight: "bold"
  },
  centerView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  postInfo: {
    paddingHorizontal: 5,
    flexDirection: "row",
    width: "100%",
    alignItems: "center"
  },
  textWhite: { color: "white", fontSize: 15 },
  userName: { fontWeight: "bold", color: "black" },
  row: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  peopleInLocation: {
    flexDirection: "row",
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center"
  }
});
