import React from "react";
import { View, StyleSheet, ScrollView, RefreshControl } from "react-native";
import _ from 'lodash'
import {
  Block
} from 'galio-framework';
import theme from '../../constants/Theme';
import {
  Select
} from '@components';
import Post from "./components/Post";
import {
  bindEventsActions,
  bindLoadingActions
} from '@actions'
import { connect } from 'react-redux'
import {
  channingActions
} from '@lib'

class MyEvent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
      refreshing: false,
      onScrollBeginPos: 0,
      firstRun: true,
      typeOfFilter: 'All events',
    };

  }

  updateSearch = search => {
    this.setState({ search });
  };

  componentDidMount() {
    const {eventsActions, loadingActions} = this.props;
    loadingActions.show()
    eventsActions.getEvents({constraint: 1}, false).then(res => {
      loadingActions.hide()
    }).catch(rej => {
      console.log("Error in getAllEvents: ", rej)
    })
  }

  // componentWillReceiveProps(newProps) {
  //   console.log("Component Will Receive Props in register")
  //   if (newProps.index == 1 && _.isEqual(newProps.events.newsfeed, this.props.events.newsfeed)) {
  //     const {eventsActions, loadingActions} = this.props;
  //     loadingActions.show()
  //     eventsActions.getEvents({limit: 3}, false).then(res => {
  //       console.log(this.props.events.newsfeed.data)
  //       loadingActions.hide()
  //     }).catch(rej => {
  //       console.log("Error in getAllEvents: ", rej)
  //     })
  //   }
  // }

  shouldComponentUpdate(newProps, newState) {
    if ((newState.typeOfFilter == this.state.typeOfFilter && newProps.index == this.props.index && _.isEqual(newProps.events.myEvents, this.props.events.myEvents)) || (newProps.index == 0 && !this.state.firstRun)) {
      return false
    }
    else {  
      if (this.state.firstRun) {
        this.setState({firstRun: false})
      }
      return true
    }
  }

  _onRefresh = () => {
    console.log("OnRefresh")
    const {eventsActions, loadingActions} = this.props;
    loadingActions.show()
    if (this.state.typeOfFilter == "Available events") {
      eventsActions.getEvents({type: 1, constraint: 1}, false).then(res => {
        loadingActions.hide()
      });
    } else if (this.state.typeOfFilter == "All events") {
      eventsActions.getEvents({constraint: 1}, false).then(res => {
          loadingActions.hide()
        });
    }
  };

  componentDidAppear() {
    // const { postActions } = this.props;
    // this.setState({ refreshing: true });
    // postActions.fetchNearBy(true).then(() => {
    //   this.setState({ refreshing: false });
    // });
  }
  
  _onEndReach = () => {
    // if (this.state.search != "") return null;
    // const { postActions } = this.props;
    // postActions.fetchNearBy();
  };

  requestSearch(keyword) {
    // const { postActions } = this.props;
    // if (!keyword) {
    //   postActions.fetchNearBy(true);
    // } else {
    //   postActions.getPostByKeyword(keyword).then(response => {
    //     this.setState({ places: response });
    //   });
    // }
  }

  onScrollUp = () => {
    
  };

  onScrollDown = () => {
    const { eventsActions } = this.props;
    console.log("Scrolling down")
    eventsActions.getEvents({limit: 4, constraint: 1}, false).then(res => {
    });
  };

  renderTypeOfChecking = () => (
    <Block row center style={{ marginTop: theme.SIZES.BASE, marginBottom: theme.SIZES.BASE }}>
      <Select
        value = {this.state.typeOfFilter}
        defaultIndex={0}
        defaultValue={this.state.typeOfFilter}
        handleOnSelect = {this.handleFilterSelect}
        options={["All events", "Available events"]}
        style={styles.shadow}
      />
    </Block>
  )

  handleFilterSelect = (index, value) => {
    console.log("Handle FIlter Select")
    const {eventsActions, loadingActions} = this.props;
    switch (value) {
      case "Available events":
        eventsActions.getEvents({type: 1, constraint: 1}, false).then(res => {
          this.setState({typeOfFilter: value})
        }).catch(rej => {
          console.log("Error in getAllEvents: ", rej)
        })
        break;
      case "All events":
        eventsActions.getEvents({constraint: 1}, false).then(res => {
          this.setState({typeOfFilter: value})
        }).catch(rej => {
          console.log("Error in getAllEvents: ", rej)
        })
        break;
      default:
        eventsActions.getEvents({type: 1, constraint: 1}, false).then(res => {
          this.setState({typeOfFilter: value})
        }).catch(rej => {
          console.log("Error in getAllEvents: ", rej)
        })
    }
  }

  render() {
    const postData = this.props.events.myEvents.data;
    console.log(postData)
    console.log("render in register")

    const isRefreshPostNearby = false;
    let isFitLeft = true;
    let isFitRight = false;

    if(postData === null || postData === undefined){
      this.onScrollDown;
    }
    const isCloseToBottom = ({
      layoutMeasurement,
      contentOffset,
      contentSize
    }) => {
      const paddingToBottom = 100;
      return (
        layoutMeasurement.height + contentOffset.y >=
        contentSize.height - paddingToBottom
      );
    };
    return (
      <View style={styles.container}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          onScrollBeginDrag={({ nativeEvent }) => {
            this.setState({ onScrollBeginPos: nativeEvent.contentOffset.y });
          }}
          onScrollEndDrag={({ nativeEvent }) => {
            const y = nativeEvent.contentOffset.y;
            // if (y - this.state.onScrollBeginPos >= 100) {
            //   this.onScrollUp();
            // }
            // if (y - this.state.onScrollBeginPos <= -100) {
            //   this.onScrollDown();
            // }
          }}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }
          onScroll={({ nativeEvent }) => {
            if (isCloseToBottom(nativeEvent) && !isRefreshPostNearby) {
              this._onEndReach();
            }
          }}
          scrollEventThrottle={400}
        >
          {this.renderTypeOfChecking()}
          <View style={styles.postsContainer}>
            <View style={styles.col}>
              {postData && postData.map((post, index) => {
                if (index % 2 == 0) {
                  isFitLeft = !isFitLeft;
                  return (
                    <Post
                      parentScreenId={this.props.parentScreenId}
                      fitHeight={isFitLeft}
                      key={index}
                      data={post}
                      navigation={this.props.navigation}
                    />
                  );
                }
              })}
            </View>
            <View style={styles.col}>
              {postData && postData.map((post, index) => {
                if (index % 2 == 1) {
                  isFitRight = !isFitRight;
                  return (
                    <Post
                      parentScreenId={this.props.parentScreenId}
                      fitHeight={isFitRight}
                      key={index}
                      data={post}
                      navigation={this.props.navigation}
                    />
                  );
                }
              })}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    paddingHorizontal: 10,
    paddingTop: 10
  },
  col: {
    width: "49%",
    alignItems: "center"
  },
  postsContainer: {
    flexDirection: "row",
    justifyContent: "space-between"
  }
});

export default connect(
  state => ({events: state.events}),
  dispatch => channingActions({}, dispatch, bindEventsActions, bindLoadingActions)
)(MyEvent)