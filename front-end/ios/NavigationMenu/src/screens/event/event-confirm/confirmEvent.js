import React from "react";
import { View, StyleSheet, SafeAreaView, ScrollView,Animated, Image} from "react-native";
import Utils from '../../../helpers/Utils'
import {API_URL} from '../../../config'
import { connect, useDispatch } from 'react-redux'
import _ from "lodash"
import {
  bindEventsActions,
  bindMeActions,
  bindLoadingActions,
  bindDialogActions,
  bindToastNotificationActions
} from '@actions'
import {
  channingActions
} from '@lib'
import {
  Header,
  Content,
  Button,
  Text
 } from '@components'

const styles = StyleSheet.create({
  imgBanner: {
    width: '100%',
    height: 250,
    position: 'absolute',
  },
  rowBanner: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  userIcon: {
    width: 40,
    height: 40,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: 'white',
    marginRight: 5,
  },
});

class EventConfirmScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: "",
      locationReady: true,
      token: "",
      heightHeader: Utils.heightHeader(),
      savedDataLoaded: false
    };
    this.deltaY = new Animated.Value(0);
    this.heightImageBanner = Utils.scaleWithPixel(250, 1);
  }

  onHandleDelete = () => {
    const { dialogActions } = this.props;
    dialogActions.show("DeleteEvents", "Are you sure to delete this event", "YES", "NO",() => {this.deleteNews()})
  }

  startEvent = eventId => {
    const { toastingActions, loadingActions, dialogActions, eventsActions } = this.props
    dialogActions.show(
      "StartEvent", 
      "Do You want to start this event", 
      "YES", 
      "NO", 
      () => {
        loadingActions.show()
        eventsActions.startEvent(eventId).then(res => {
          loadingActions.hide()
          toastingActions.show("Start event successfully", "bottom", "success")
        }).catch(rej => {
          loadingActions.hide()
          toastingActions.show("Error", "bottom", "error")
          console.log(rej)
        })
      }, 
      () => {}
    )
  }

  finishEvent = (eventId) => {
    const { toastingActions, loadingActions, dialogActions, eventsActions } = this.props
    dialogActions.show(
      "FinishEvent", 
      "Do You want to finish this event", 
      "YES", 
      "NO", 
      () => {
        loadingActions.show()
        eventsActions.finishEvent(eventId).then(res => {
          loadingActions.hide()
          toastingActions.show("Finish event successfully", "bottom", "success")
        }).catch(rej => {
          loadingActions.hide()
          toastingActions.show("Error", "bottom", "error")
          console.log(rej)
        })
      }, 
      () => {}
    )
  }

  render(){
    const { username, password, savedDataLoaded, heightHeader} = this.state;
    const { me } = this.props;
    console.log("Props in eventRegister: ", this.props);
    const deltaY = this.deltaY;
    const data = this.props.event;
    let numOfStaff = 0
    let staffAttempt = 0
    let numOfRegister = 0
    let registerAttempt = 0
    data.userStaff.map(i => {
      numOfStaff++
      if (i.isAttempt) staffAttempt++; 
    })
    data.userRegistered.map(i => {
      numOfRegister++
      if (i.isAttempt) registerAttempt++; 
    })
    
    return (
    <View style = {{flex: 1, backgroundColor: 'white'}}>
      <Animated.View
        style={[
        styles.imgBanner,
          {
            height: deltaY.interpolate({
              inputRange: [
                0,
                Utils.scaleWithPixel(140),
                Utils.scaleWithPixel(140),
              ],
              outputRange: [this.heightImageBanner, heightHeader, heightHeader],
            }),
          },
        ]}
      >
        <Image source={{uri: API_URL + data.createdForm.media.path.thumbnail}} style={{flex: 1}} />
        <Animated.View
          style={{
            position: 'absolute',
            flexDirection: 'row',
            
            justifyContent: 'space-between',
            alignItems: 'flex-end',
            width: '100%',
            bottom: 0,
            padding: 10,
            backgroundColor: 'rgba(0,0,0,0.4)',

            opacity: deltaY.interpolate({
              inputRange: [
                0,
                Utils.scaleWithPixel(140),
                Utils.scaleWithPixel(140),
              ],
              outputRange: [1, 0, 0],
            }),
          }}>
          <View style={styles.rowBanner}>
            <Image source={{uri: API_URL + data.userCreated.media.thumbnail}} style={styles.userIcon} />
            <View style={{alignItems: 'flex-start'}}>
              <Text headline semibold whiteColor style={{color:'white'}}>
                {data.createdForm.title}
              </Text>
              <Text footnote whiteColor style={{color:'white'}}>
                {data.userCreated.name}
              </Text>
            </View>
          </View>
        </Animated.View>
      </Animated.View>
      <SafeAreaView style={{flex: 1}} forceInset={{top: 'always'}}>
        <ScrollView
          onScroll={Animated.event([
            {
              nativeEvent: {
                contentOffset: {y: this.deltaY},
              },
            },
          ])}
          onContentSizeChange={() => {
            this.setState({heightHeader: Utils.heightHeader()})
          }}
          scrollEventThrottle={8}
        >
          <View style={{height: 300 - heightHeader}} />
          <View
          style={{
            paddingHorizontal: 20,
            marginBottom: 20,
          }}>

            <Header header="Social Days" />
            <Content content={data.createdForm.socialDays} />

            <Header header="Staff's Attendance" />
            <Content content={staffAttempt + '/' + numOfStaff} />

            <Header header="Register's Attendance" />
            <Content content={registerAttempt + '/' + numOfRegister} />

            <Header header="Event Start At" />
            <Content content={data.eventStatus.start.isStart ? Utils.getDate(data.eventStatus.start.startAt) : "Event hasn't started yet"} />

            <Header header="Event Second Check" />
            <Content content={data.secondCheck.isEnd ? 'from ' + Utils.getDate(data.secondCheck.start.startAt) + ' to ' + Utils.getDate(data.secondCheck.end.endAt) : "Second Attendance Check hasn't finished yet"} />
            
            <Header header="Event Finish At" />
            <Content content={data.eventStatus.end.isEnd ? Utils.getDate(data.eventStatus.end.endAt) : "Event hasn't finished yet"} />
            
            <Header header="Place Occur" />
            <Content content={data.createdForm.eventAddress} />
            
            <Header header="Leader Confirm At" />
            <Content content={data.eventStatus.confirm.lead.isConfirm ? Utils.getDate(data.eventStatus.confirm.lead.confirmAt) : "Leader hasn't confirmed yet"} />

            <Header header="Admin Confirm At" />
            <Content content={data.eventStatus.confirm.admin.isConfirm ? Utils.getDate(data.eventStatus.confirm.admin.confirmAt) : "Admin hasn't confirmed yet"} />

            <Header header="Office Confirm At" />
            <Content content={data.eventStatus.confirm.myBK.isConfirm ? Utils.getDate(data.eventStatus.confirm.myBK.confirmAt) : "Office hasn't confirmed yet"} />

          </View>

          <Button
            title="Start Event"
            disabled={data.eventStatus.start.isStart}
            onPress={() => this.startEvent(data._id)}
          />

          <Button 
            title="First Attendance Check"
            disabled={data.firstCheck.end.isEnd || !data.eventStatus.start.isStart}
            onPress={() => {this.props.navigation.navigate("FirstCheck", data._id)}}
          />

          <Button 
            title="Second Attendance Check"
            disabled={data.secondCheck.end.isEnd || !data.firstCheck.end.isEnd}
            onPress={() => {this.props.navigation.navigate("SecondCheck", data._id)}}
          />

          <Button
            title="Finish Event"
            disabled={data.eventStatus.end.isEnd || !data.secondCheck.end.isEnd}
            onPress={() => this.finishEvent(data._id)}
          />

          <Button
            title="Staffs Report"
            onPress={() => {this.props.navigation.navigate("StaffsReport", data._id)}}
          />

          <Button
            title="Registers Report"
            onPress={() => {this.props.navigation.navigate("RegistersReport", data._id)}}
          />

          <Button
            title="Confirm Event"
            disabled={!data.eventStatus.confirm.lead.isConfirm || !data.eventStatus.end.isEnd}
          />

          <Button
            title="Confirm by Office"
            disabled={!data.eventStatus.confirm.admin.isConfirm}
          />

        </ScrollView>
      </SafeAreaView>
    </View>
    );
  }
  
}

export default connect(
  state => ({me: state.me, event: state.events.event}),
  dispatch => channingActions({}, dispatch, bindMeActions, bindEventsActions, bindLoadingActions, bindDialogActions, bindToastNotificationActions)
)(EventConfirmScreen)