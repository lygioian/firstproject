import RegistersReportScreen from './registers'
import RegisterReportScreen from './report'

export {
    RegistersReportScreen,
    RegisterReportScreen
}