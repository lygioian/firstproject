import React from 'react';
import {
  StyleSheet, ScrollView, View, TouchableOpacity, Image
} from 'react-native';
import _ from 'lodash'
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import {API_URL} from '../../../../config';
import { connect } from 'react-redux';
import Utils from '../../../../helpers/Utils'
import {
  Text,
  Header,
  Content,
  Button
} from '@components'
import {
  channingActions
} from '@lib'
import {
  bindMeActions,
  bindLoadingActions,
  bindEventsActions,
  bindToastNotificationActions,
  bindDialogActions
} from '@actions'

class FirstCheckScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
      data: null,
      refreshing: false,
      onScrollBeginPos: 0,
    };

  }

  start = eventId => {
    const { toastingActions, loadingActions, dialogActions, eventsActions } = this.props
    dialogActions.show(
      "StartFirstCheck", 
      "Do You want to start first attendance check", 
      "YES", 
      "NO", 
      () => {
        loadingActions.show()
        eventsActions.startFirstCheck(eventId).then(res => {
          loadingActions.hide()
          toastingActions.show("Start first check successfully", "bottom", "success")
        }).catch(rej => {
          loadingActions.hide()
          toastingActions.show("Error", "bottom", "error")
          console.log(rej)
        })
      }, 
      () => {}
    )
  }

  finish = (eventId) => {
    const { toastingActions, loadingActions, dialogActions, eventsActions } = this.props
    dialogActions.show(
      "FinishFirstCheck", 
      "Do You want to finish first attendance check", 
      "YES", 
      "NO", 
      () => {
        loadingActions.show()
        eventsActions.finishFirstCheck(eventId).then(res => {
          loadingActions.hide()
          toastingActions.show("Finish first check successfully", "bottom", "success")
        }).catch(rej => {
          loadingActions.hide()
          toastingActions.show("Error", "bottom", "error")
          console.log(rej)
        })
      }, 
      () => {}
    )
  }

  render() {
    let event = this.props.event;
    console.log("Selected Event", event)
    let numOfRegister = 0
    let registerAttempt = 0
    event.userRegistered.map(i => {
      numOfRegister++
      if (i.checkAttendance.firstCheck.isCheck) registerAttempt++; 
    })
    
    return (
      <ScrollView style={styles.list}>
        <View style={{
            marginBottom: 20,
        }}>
          <View style={{
              paddingHorizontal: 20,
              marginBottom: 20,
          }}>

            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <Text title3 >
                First Attendance Check
              </Text>

              <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                <Button
                  title="Start"
                  disabled={!event.eventStatus.start.isStart || event.firstCheck.start.isStart}
                  onPress={() => this.start(event._id)}
                />

                <Button
                  title="Finish"
                  disabled={!event.firstCheck.start.isStart || event.firstCheck.end.isEnd}
                  onPress={() => this.finish(event._id)}
                />
              </View>
            </View>

            <Header header="Start at" />
            <Content content={event.firstCheck.start.isStart ? Utils.getDate(event.firstCheck.start.startAt) : "First Attendance Check hasn't started yet"} />

            <Header header="Finish at" />
            <Content content={event.firstCheck.end.isEnd ? Utils.getDate(event.firstCheck.end.endAt) : "First Attendance Check hasn't finished yet"} />

            <Header header="Total Registers" />
            <Content content={numOfRegister} />

            <Header header="Number of Attendance" />
            <Content content={registerAttempt} />

            <Header header="Num of Absence" />
            <Content content={numOfRegister - registerAttempt} />

            <Header header="Registers List"/>
          </View>
          {event.userRegistered.map(element => (
            <View style={styles.item} key={element._id}>
              <TouchableOpacity>
                <TouchableOpacity 
                  onPress={() => this.props.navigation.navigate("ProfileView", {product: element})}
                >
                  <Image
                    style={styles.itemPicture}
                    source={{
                      uri: API_URL + element.user.media.thumbnail
                    }}
                    resizeMode="cover"
                  />
                </TouchableOpacity>
              </TouchableOpacity>
              <View style={styles.itemInfo}>
                <Text title3 style={styles.itemInfoTextFullName}>
                  {element.user.firstName + ' ' + element.user.lastName}
                </Text>
                <Text title3 style={styles.itemInfoTextFullName}>
                  {element.role}
                </Text>
              </View>
              {this.props.me.information._id == event.userCreated._id ? (
                <View style={styles.itemIconContainer}>
                  <Icon name={element.checkAttendance.firstCheck.isCheck ? "account-check" :"account-remove"} size={25}/>
                </View>
              ) : null}
            </View>
          ))} 
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  list: {
    flex: 1,
    marginTop: 20,
  },
  item: {
    flexDirection: "row",
    height: 60,
    paddingLeft: 30,
    width: "100%",
    paddingRight: 20,
    marginBottom: 15
  },
  itemPicture: {
    width: 60,
    height: 60,
    borderRadius: 30,
    borderWidth: 2,
    borderColor: "black"
  },
  itemInfo: {
    justifyContent: "center",
    marginLeft: 10,
  },
  itemInfoTextFullName: {
    color: "rgba(0,0,0,1)",
    fontSize: 15
  },
  itemIconContainer: {
    flex: 1,
    alignItems: "flex-end",
    justifyContent: "center"
  },
});

export default connect(
    state => ({me: state.me, event: state.events.event}),
    dispatch => channingActions({}, dispatch, bindMeActions, bindEventsActions, bindLoadingActions, bindToastNotificationActions, bindDialogActions)
  )(FirstCheckScreen)