import React from 'react';
import {
  StyleSheet, ScrollView, View, TouchableOpacity, Image
} from 'react-native';
import _ from 'lodash'
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import {API_URL} from '../../../../config';
import { connect } from 'react-redux';
import {
  Text
} from '@components'
import {
  channingActions
} from '@lib'
import {
  bindMeActions,
  bindLoadingActions,
  bindEventsActions,
  bindToastNotificationActions,
  bindUsersActions,
  bindDialogActions
} from '@actions'

class StaffsReportScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
      data: null,
      refreshing: false,
      onScrollBeginPos: 0,
    };
  }

  onHeaderButton = () => {
    const { dialogActions } = this.props
    console.log("HeaderButton", this.props)
    dialogActions.show(
      "Update Social Days", 
      "Do You want to update all social days", 
      "YES", 
      "NO", 
      value => {
        console.log(value)
      }, 
      () => {},
      true,   
    )
  }
  
  onPress = data => {
    const { usersActions } = this.props
    usersActions.getStaffReport(data).then(res => {
      this.props.navigation.navigate('StaffReport', data._id)
    }).catch(rej => {
      console.log("Error", rej)
    })
  }

  render() {
    console.log("Props in Register View: ", this.props);
    let event = this.props.event

    this.props.navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity style={styles.iconContainer} onPress={this.onHeaderButton}>
            <Icon name="pencil-plus-outline"  size={25}/>
        </TouchableOpacity>
      )
    });
    
    return (
      <ScrollView style={styles.list}>
        {event.userStaff.map(element => (
          <View style={styles.item} key={element._id}>
            <TouchableOpacity>
              <TouchableOpacity 
                onPress={() => this.onPress(element)}
              >
                <Image
                  style={styles.itemPicture}
                  source={{
                    uri: API_URL + element.user.media.thumbnail
                  }}
                  resizeMode="cover"
                />
              </TouchableOpacity>
            </TouchableOpacity>
            <View style={styles.itemInfo}>
              <Text title3 style={styles.itemInfoTextFullName}>
                Name: {element.user.firstName + ' ' + element.user.lastName}
              </Text>
              <Text title3 style={styles.itemInfoTextFullName}>
                Role: {element.role}
              </Text>
              <Text title3 style={styles.itemInfoTextFullName}>
                Social Days: {element.socialDays}
              </Text>
            </View>
          </View>
        ))} 
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  list: {
    flex: 1,
    marginTop: 20,
  },
  item: {
    flexDirection: "row",
    height: 60,
    paddingLeft: 30,
    width: "100%",
    paddingRight: 20,
    marginBottom: 15
  },
  itemPicture: {
    width: 60,
    height: 60,
    borderRadius: 30,
    borderWidth: 2,
    borderColor: "black"
  },
  itemInfo: {
    justifyContent: "center",
    marginLeft: 10,
  },
  itemInfoTextFullName: {
    color: "rgba(0,0,0,1)",
    fontSize: 15
  },
  itemIconContainer: {
    flex: 1,
    alignItems: "flex-end",
    justifyContent: "center"
  },
});

export default connect(
    state => ({me: state.me, event: state.events.event}),
    dispatch => channingActions({}, dispatch, bindMeActions, bindEventsActions, bindLoadingActions, bindToastNotificationActions, bindUsersActions, bindDialogActions)
  )(StaffsReportScreen)