import React from "react";
import {
  View,
  StyleSheet,
  KeyboardAvoidingView,
  ScrollView,
} from "react-native";
import { connect } from 'react-redux'
import {
  channingActions
} from '@lib'
import {
  bindMeActions,
  bindEventsActions,
  bindLoadingActions,
  bindDialogActions,
  bindToastNotificationActions
} from '@actions'
import _ from 'lodash';
import {API_URL} from '../../../../config'
import {
  ProfileDetail,
  Button,
  Header,
  Content,
} from '@components'

class StaffReportScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {

    }
  }

  onButtonPress = () => {
    const { dialogActions } = this.props
    dialogActions.show(
      "Update Social Days", 
      "Do You want to update social days", 
      "YES", 
      "NO", 
      value => {
        console.log(value)
      }, 
      () => {},
      true,   
    )
  }
  
  render() {
    console.log("Event Report props: ", this.props)
    const report = this.props.report
    return (
      <KeyboardAvoidingView
        style={styles.container}
        enabled
        key={this.state.timestamp}
      >
        <ScrollView>
          <View style={styles.contain}>
            <ProfileDetail
              image={ API_URL+ report.user.media.thumbnail}
              textFirst={"Name: " + report.user.fullName}
              point={'9.5'}
              textSecond={"Phone: " + report.user.phone}
              textThird={"Username: " + report.user.username}
              onPress={() => this.props.navigation.navigate('ProfileView', {product: report.user})}
            />

            <View
              style={{
                marginBottom: 20,
            }}>
              <Header header="Role" />
              <Content content={report.role} />

              <Header header="Social Days" />
              <Content content={report.socialDays} />

              <Header header="Attendance" />
              <Content content={report.isAttempt ? "100%" : "0%"} />

              <Header header="Note (Updating)" />
              <Content content='Update on next version' />
            </View>

            <Button
              title="Update Social Days"
              onPress={this.onButtonPress}
            />

          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  information: {
    width: "100%",
    paddingTop: 20,
    paddingBottom: 20,
    backgroundColor: 'white'
  },
  contain: {
    flex: 1,
    padding: 20
  },
});

export default connect(
  state => ({me: state.me, report: state.users.staffReport}),
  dispatch => channingActions({}, dispatch, bindMeActions, bindEventsActions, bindLoadingActions, bindDialogActions, bindToastNotificationActions)
)(StaffReportScreen)