import StaffsReportScreen from './staffs'
import StaffReportScreen from './report'

export {
    StaffsReportScreen,
    StaffReportScreen
}