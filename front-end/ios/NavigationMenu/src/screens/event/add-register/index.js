import React from "react";
import {
  View,
  Image,
  StyleSheet,
  ImageBackground,
  ScrollView,
  TouchableOpacity
} from "react-native";
import _ from "lodash";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { connect } from 'react-redux'
import {
  Text,
  SearchInput
} from '@components/'
import {
  channingActions
} from '@lib'
import {
  bindMeActions,
  bindEventsActions,
  bindLoadingActions,
  bindUsersActions
} from '@actions'
import { API_URL } from '../../../config';

class AddRegisterScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      unfollow: [],
      search: "",
      user: [],
    };
  }

  componentDidMount() {
    const {usersActions, loadingActions} = this.props;
    loadingActions.show()
    usersActions.getUsers({}, true).then(res => {
      loadingActions.hide()
    }).catch(rej => {
      loadingActions.hide()
      console.log(rej)
    })
  }

  add = user => {
    const { eventsActions } = this.props;
    const {userId} = this.state;
    let check = _.includes(userId, user._id);
    if(check == true){
      return;
    }
    eventsActions.addRegister(this.props.route.params._id, user._id).then(res => {
      console.log(res)
    }).catch((rej) => {
      console.log("Error in addRegister in AddRegister: ", rej)
    })
  };

  guestView() {
    this.props.history.push({
      pathname: routes.GUEST,
      state: element._id
    });
  }

  gotoGuest(data) {
    if (data._id == this.props.info._id) {
      AppNavigation.popToRoot(this.props.componentId);
      AppNavigation.selectTab(4);
    } else {
      const { toastActions } = this.props;
      AppNavigation.pushScreen.Profile(this.props.componentId, data._id);
    }
  }

  renderIcon = (event, element) => {
    for (let i = 0; i < event.userRegistered.length; i++) {
      if (event.userRegistered[i].user._id == element._id) return null
    }
    for (let i = 0; i < event.userStaff.length; i++) {
      if (event.userStaff[i].user._id == element._id) return null
    }

    return (
      <TouchableOpacity
        onPress={() => this.add(element)}
      >
        <Icon name="account-plus" size={25} color="blue"/>
      </TouchableOpacity>
    )
  }

  requestSearch(keyword) {
    const { meActions } = this.props;

    if (!keyword && keyword == "") {
      return;
    } else {
      meActions.getUserByKeyword(keyword).then(response => {
        this.setState({ user: response.data});
      }).catch(rej => {
        console.log("Error in requestSearch in addRegister: ", rej)
      });
    }
  }

  render() {
    console.log("UserRegister: ", this.props)
    let event = this.props.event;

    return (
      <ImageBackground
        // source={IMG_BACKGROUND}
        resizeMode="cover"
        style={styles.container}
      >
        <View style={{ height: 15, }} />
        <SearchInput
            onSearchEnd={() => this.requestSearch(this.state.search)}
            value={this.state.search}
            updateSearch={search => {
              this.setState({ search });
              this.requestSearch(search);
            }}
          />
        <ScrollView>       
          {this.props.users.list.data.map(element => (
            <View style={styles.item} key={element._id}>
              <TouchableOpacity>
                <TouchableOpacity 
                // onPress={() => this.gotoGuest(element)}
                >
                  <Image
                    style={styles.itemPicture}
                    source={{
                      uri: API_URL + element.media.thumbnail
                    }}
                    resizeMode="cover"
                  />
                </TouchableOpacity>
              </TouchableOpacity>
              <View style={styles.itemInfo}>
                <Text title3 style={styles.itemInfoTextFullName}>
                  {element.firstName + " " + element.lastName}
                </Text>
              </View>
                <View style={styles.itemIconContainer}>
                  {this.renderIcon(event, element)}
                </View>
            </View>
          ))} 
        </ScrollView>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  item: {
    flexDirection: "row",
    height: 60,
    paddingLeft: 30,
    width: "100%",
    paddingRight: 20,
    marginBottom: 15
  },
  itemPicture: {
    width: 60,
    height: 60,
    borderRadius: 30,
    borderWidth: 2,
    borderColor: "black"
  },
  itemInfo: {
    justifyContent: "center",
    marginLeft: 10,
  },
  itemInfoTextFullName: {
    color: "rgba(0,0,0,1)",
    fontSize: 15
  },
  itemIconContainer: {
    flex: 1,
    alignItems: "flex-end",
    justifyContent: "center"
  },
});


export default connect(
    state => ({event: state.events.event, me: state.me, users: state.users}),
    dispatch => channingActions({}, dispatch, bindMeActions, bindEventsActions, bindLoadingActions, bindUsersActions)
)(AddRegisterScreen)