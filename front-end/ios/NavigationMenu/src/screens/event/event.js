import React from "react";
import { StyleSheet, Dimensions, TouchableOpacity } from "react-native";
import { TabView, TabBar } from "react-native-tab-view";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import {
  Text
} from '@components'
import UserEvent from "./userEvent";
import MyEvent from "./myEvent";
import {
  bindEventsActions,
  bindLoadingActions
} from '@actions'
import { connect } from 'react-redux'
import {
  channingActions
} from '@lib'

class EventScreen extends React.Component {
  constructor(props) {
    super(props);
  }
  state = {
    index: 0,
    routes: [
      { key: "userNearby", title: "Available Events" },
      { key: "userPopular", title: "My Events" },
    ],
    error: null
  };

  shouldComponentUpdate(newProps, newState) {
    if (newState.index === this.state.index) {
      console.log("Component shouldn't update in index")
      return false
    }
    console.log("Component should update in index")
    return true
  }

  navigationButtonPressed = () =>  {
    this.props.navigation.navigate("CreateEvent")
  }


  UserEventRoute = () => <UserEvent navigation = {this.props.navigation} index = {this.state.index}/>;
  UserRegisteredEventRoute = () => <MyEvent navigation = {this.props.navigation} index = {this.state.index}/>;

  _renderLabel = ({ route }) => <Text body2>{route.title}</Text>;

  render(){
    this.props.navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity style={styles.iconContainer} onPress={this.navigationButtonPressed}>
          <Icon name="pencil-plus-outline"  size={25}/>
        </TouchableOpacity>
      )
    });

    return (
          <TabView
          navigationState={this.state}
          renderScene = {({ route }) => {
            switch (route.key) {
              case 'userNearby':
                return <UserEvent navigation={this.props.navigation} index = {this.state.index}/>;
              case 'userPopular':
                return <MyEvent navigation={this.props.navigation} index = {this.state.index}/>;
              default:
                return null;
            }
          }}
          onIndexChange={index => this.setState({ index })}
          initialLayout={{ width: Dimensions.get("window").width }}
          renderTabBar={props => (
            <TabBar
              {...props}
              renderLabel={this._renderLabel}
              indicatorStyle={styles.tabBarBorder}
              style={styles.tabBarBackground}
              labelStyle={styles.tabBarText}
            />
          )}
          />
    );
  }
  
}

const styles = StyleSheet.create({
  tabBarBorder: {
    backgroundColor: "#ddd"
  },
  tabBarText: {
    color: "black",
  },
  tabBarBackground: {
    backgroundColor: "white"
  }
});

export default connect(
  state => ({events: state.events, user: state.me}),
  dispatch => channingActions({}, dispatch, bindEventsActions, bindLoadingActions)
)(EventScreen)
