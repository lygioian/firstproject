import React from 'react';
import {
  StyleSheet, ScrollView, View, TouchableOpacity, Image
} from 'react-native';
import _ from 'lodash'
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import {API_URL} from '../../../config';
import { connect } from 'react-redux';
import {
  Text
} from '@components'
import {
  channingActions
} from '@lib'
import {
  bindMeActions,
  bindLoadingActions,
  bindEventsActions,
  bindToastNotificationActions
} from '@actions'

class ViewRegisterScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
      data: null,
      refreshing: false,
      onScrollBeginPos: 0,
    };

  }
  
  navigationButtonPressed = (event) =>  {
    const { eventsActions, loadingActions, toastingActions } = this.props;
    loadingActions.show()
    this.props.navigation.navigate('AddRegister', event);
    loadingActions.hide()
  }

  delete = element => {
    const { eventsActions } = this.props;
    const {userId} = this.state;
    _.remove(userId, (e) => {return e === Id});
    eventsActions.deleteRegister(this.props.event._id, element.user._id).then(res => {
      console.log("Delete staff: ", res)
    }).catch((rej) => {
        console.log("Error in deleteStaff in AddLead: ", rej)
    })
  };

  render() {
    console.log("Props in Register View: ", this.props);
    let event = this.props.event;

    if (this.props.me.information._id == event.userCreated._id) {
      this.props.navigation.setOptions({
        headerRight: () => (
          <TouchableOpacity style={styles.iconContainer} onPress={() => this.props.navigation.navigate('AddRegister', event)}>
              <Icon name="pencil-plus-outline"  size={25}/>
          </TouchableOpacity>
        )
      });
    }
    
    return (
      <ScrollView style={styles.list}>
        {event.userRegistered.map(element => (
          <View style={styles.item} key={element._id}>
            <TouchableOpacity>
              <TouchableOpacity 
                onPress={() => this.props.navigation.navigate("ProfileView", {product: element.user})}
              >
                <Image
                  style={styles.itemPicture}
                  source={{
                    uri: API_URL + element.user.media.thumbnail
                  }}
                  resizeMode="cover"
                />
              </TouchableOpacity>
            </TouchableOpacity>
            <View style={styles.itemInfo}>
              <Text title3 style={styles.itemInfoTextFullName}>
                {element.user.firstName + ' ' + element.user.lastName}
              </Text>
              <Text title3 style={styles.itemInfoTextFullName}>
                {element.role}
              </Text>
            </View>
            {this.props.me.information._id == event.userCreated._id ? (
              <View style={styles.itemIconContainer}>
                <TouchableOpacity
                    onPress={() => this.delete(element)}
                  >
                    <Icon name="account-remove" size={25}/>
                  </TouchableOpacity>
              </View>
            ) : null}
          </View>
        ))} 
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  list: {
    flex: 1,
    marginTop: 20,
  },
  item: {
    flexDirection: "row",
    height: 60,
    paddingLeft: 30,
    width: "100%",
    paddingRight: 20,
    marginBottom: 15
  },
  itemPicture: {
    width: 60,
    height: 60,
    borderRadius: 30,
    borderWidth: 2,
    borderColor: "black"
  },
  itemInfo: {
    justifyContent: "center",
    marginLeft: 10,
  },
  itemInfoTextFullName: {
    color: "rgba(0,0,0,1)",
    fontSize: 15
  },
  itemIconContainer: {
    flex: 1,
    alignItems: "flex-end",
    justifyContent: "center"
  },
});

export default connect(
    state => ({me: state.me, event: state.events.event}),
    dispatch => channingActions({}, dispatch, bindMeActions, bindEventsActions, bindLoadingActions, bindToastNotificationActions)
  )(ViewRegisterScreen)