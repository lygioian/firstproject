import React from "react";
import {
  View,
  StyleSheet,
  KeyboardAvoidingView,
  ScrollView,
} from "react-native";
import { connect } from 'react-redux'
import ImagePicker from "react-native-image-picker";
import {API_URL} from '../../../config'
import _ from 'lodash';
import {
  channingActions
} from '@lib'
import {
  bindMeActions,
  bindEventsActions,
  bindLoadingActions,
  bindToastNotificationActions,
  bindDialogActions
} from '@actions'
import {
  TextField,
  DateField,
  ImageField,
  Button,
  CheckBox,
  Picker
} from '@components'

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  information: {
    width: "100%",
    paddingTop: 20,
    paddingBottom: 20,
    backgroundColor: 'white'
  },
});


class EditEventScreen extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      title: this.props.event.createdForm.title,
      description: this.props.event.createdForm.description,
      socialDays: this.props.event.createdForm.socialDays +"",
      maxRegister: this.props.event.createdForm.maxRegister +"",
      eventAddress: this.props.event.createdForm.eventAddress,
      eventUnit: this.props.event.createdForm.eventUnit,
      type: this.props.event.createdForm.type,
      requirements: this.props.event.createdForm.requirements,
      placeGather: this.props.event.createdForm.placeGather,
      formStart: new Date(this.props.event.createdForm.formStart),
      formEnd: new Date(this.props.event.createdForm.formEnd),
      eventStart: this.props.event.createdForm.eventStart != -1 ? new Date(this.props.event.createdForm.eventStart) : null,
      eventEnd: this.props.event.createdForm.eventEnd != -1 ? new Date(this.props.event.createdForm.eventEnd) : null,
      timeGather: this.props.event.createdForm.timeGather != -1 ? new Date(this.props.event.createdForm.timeGather) : null,
      isUrgent: this.props.event.createdForm.isUrgent,
      media: API_URL + this.props.event.createdForm.media.path.thumbnail,
      responseChangePhoto: null
    };
    this.choose = [
      {
        label: "Normal",
        value: "normal"
      },
      {
        label: "Special",
        value: "special"
      }
    ];
    this.changeProfilePhoto = this.changeProfilePhoto.bind(this);
  }

  changeProfilePhoto() {
    const { meActions } = this.props;
    const options = {
      title: "Select Photo",
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };
    
    ImagePicker.showImagePicker(options, async response => {
      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        this.setState({media: response.uri, responseChangePhoto: response})
      }
    });    
  }

  navigationButtonPressed = () =>  {
    const { eventsActions, meActions, loadingActions, toastingActions } = this.props;
    const event = this.props.event;
    loadingActions.show()
    const { 
    title,
    description,
    socialDays,
    maxRegister,
    eventAddress,
    eventUnit,
    type,
    requirements,
    placeGather,
    formEnd,
    formStart,
    eventStart,
    eventEnd,
    timeGather,
    isUrgent,
    responseChangePhoto, } = this.state;

    let data = {
      ...event,
      title: title,
      description: description,
      socialDays: parseInt(socialDays),
      maxRegister: parseInt(maxRegister),
      eventAddress: eventAddress,
      eventUnit: eventUnit,
      timeGather: timeGather ? new Date(timeGather).getTime() : -1,
      placeGather: placeGather,
      formStart: new Date(formStart).getTime(),
      formEnd: new Date(formEnd).getTime(),
      eventStart: eventStart ? new Date(eventStart).getTime() : -1,
      eventEnd: eventEnd ? new Date(eventEnd).getTime() : -1,
      requirements: requirements,
      type: type,
      isUrgent: isUrgent,
    }

    if(this.state.responseChangePhoto == null){
      eventsActions
        .updateEvent(data, this.props.event._id, false)
        .then((res) => {
        loadingActions.hide()
        toastingActions.show("Update Successfully !!", "bottom", "success");
        this.props.navigation.goBack()
        }).catch((rej) => {
          console.log(rej);
          loadingActions.hide();
          toastingActions.show("Update Error (" + rej + ")", "bottom", "error");
        });      
      return;
    }
    data.media = this.state.responseChangePhoto;
    eventsActions
      .updateEvent(data, this.props.event._id, true)
      .then((res) => {
        loadingActions.hide()
        toastingActions.show("Update Successfully !!", "bottom", "success");
        this.props.navigation.goBack()
        console.log("Succc", res)
      }).catch((rej) => {
        console.log(rej);
        loadingActions.hide();
        toastingActions.show("Update Error (" + rej + ")", "bottom", "error");
      });
    return
  }

  onButtonHandle = () => {
    const { toastingActions, dialogActions } = this.props;
    const event = this.props.event;
    let createdForm = {
      title: this.state.title,
      description: this.state.description,
      socialDays: parseInt(this.state.socialDays),
      maxRegister: parseInt(this.state.maxRegister),
      eventAddress: this.state.eventAddress,
      eventUnit: this.state.eventUnit,
      requirements: this.state.requirements,
      type: this.state.type,
      formStart: new Date(this.state.formStart).getTime(),
      formEnd: new Date(this.state.formEnd).getTime(),
      timeGather: this.state.timeGather ? new Date(this.state.timeGather).getTime() : -1,
      placeGather: this.state.placeGather,
      eventStart: this.state.eventStart ? new Date(this.state.eventStart).getTime() : -1,
      eventEnd: this.state.eventEnd ? new Date(this.state.eventEnd).getTime() : -1,
      isUrgent: this.state.isUrgent,
    }
    let data = {
      ...event,
      createdForm: {
        ...event.createdForm,
        ...createdForm,
      }
    }

    const isChange = !_.isEqual(event, data)
    if (!isChange && this.state.responseChangePhoto == null) {
      toastingActions.show("You haven't change anything", "bottom", "warning")
    }
    else {
      return dialogActions.show("UpdateEvent", "Do you want to update this event", "YES", "NO", this.navigationButtonPressed)
    }
  }
  
  render() {
    console.log("state: ", this.state)
    const placeholder = {
      label: "Select type ...",
      value: ""
    };

    return (
      <KeyboardAvoidingView
        style={styles.container}
        // keyboardVerticalOffset={100}
        // behavior="padding"
        enabled
        key={this.state.timestamp}
      >
        <ScrollView>
          <ImageField
            onPress={this.changeProfilePhoto}
            source={this.state.media}
            title="Change Profile Image"
          />

          <View style={styles.information}>
            <TextField
                title="Title"
                value={this.state.title}
                onChange={title => this.setState({ title })}
            />

            <TextField
                title="Social Days"
                value={this.state.socialDays}
                onChange={socialDays => this.setState({ socialDays })}
                isNumberic={true}
            />

            <TextField
                title="Max Register"
                value={this.state.maxRegister}
                onChange={maxRegister => this.setState({ maxRegister })}
                isNumberic={true}
            />

            <TextField
                title="Description"
                value={this.state.description}
                onChange={description => this.setState({ description })}
            />

            <TextField
                title="Place Occur"
                value={this.state.eventAddress}
                onChange={eventAddress => this.setState({ eventAddress })}
            />

            <TextField
                title="Unit held"
                value={this.state.eventUnit}
                onChange={eventUnit => this.setState({ eventUnit })}
            />

            <DateField 
                title="Event Start At"
                value={this.state.eventStart}
                onChange={eventStart => this.setState({ eventStart })}
            />

            <DateField 
                title="Event End At"
                value={this.state.eventEnd}
                onChange={eventEnd => this.setState({ eventEnd })}
            />

            <TextField
                title="Requirements"
                value={this.state.requirements}
                onChange={requirements => this.setState({ requirements })}
            />

            <DateField 
                title="Time Gather"
                value={this.state.timeGather}
                onChange={timeGather => this.setState({ timeGather })}
            />

            <TextField
                title="Place Gather"
                value={this.state.placeGather}
                onChange={placeGather => this.setState({ placeGather })}
            />

            <DateField 
                title="Posting Form At"
                value={this.state.formStart}
                onChange={formStart => this.setState({ formStart })}
            />

            <DateField 
                title="Closing Form At"
                value={this.state.formEnd}
                onChange={formEnd => this.setState({ formEnd })}
            />

            <CheckBox
              title="Is this event urgent ?"
              value={this.state.isUrgent}
              onChange={() => this.setState({isUrgent: !this.state.isUrgent})}
              name={this.state.isUrgent ? "Yes" : "No"}
            />

            <Picker 
              title="Type of Event"
              arr={this.choose}
              onChange={value => {
                this.setState({ type: value });
              }}
              value={this.state.type}
              placeholder={placeholder}
            />
          </View>
        </ScrollView>
        <View style={{
          paddingVertical: 15, 
          paddingHorizontal: 20,
        }}>
          <Button
            title="Confirm"
            onPress={this.onButtonHandle}
          />
        </View>
      </KeyboardAvoidingView>
    );
  }
}

export default connect(
  state => ({me: state.me, event: state.events.event}),
  dispatch => channingActions({}, dispatch, bindMeActions, bindEventsActions, bindLoadingActions, bindToastNotificationActions, bindDialogActions)
)(EditEventScreen)