import React from "react";
import { View, StyleSheet, SafeAreaView, ScrollView,Animated, Image} from "react-native";
import _ from "lodash"
import Utils from '../../../helpers/Utils'
import {API_URL} from '../../../config'
import { connect } from 'react-redux'
import {
  channingActions
} from '@lib'
import {
  bindMeActions,
  bindLoadingActions,
  bindDialogActions,
  bindToastNotificationActions,
  bindEventsActions
} from '@actions'
import {
  Header,
  Content,
  Button,
  Text
} from '@components'

const styles = StyleSheet.create({
  imgBanner: {
    width: '100%',
    height: 250,
    position: 'absolute',
  },
  rowBanner: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  userIcon: {
    width: 40,
    height: 40,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: 'white',
    marginRight: 5,
  },
});

class EventDetailScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: "",
      locationReady: true,
      token: "",
      heightHeader: Utils.heightHeader(),
      savedDataLoaded: false
    };
    this.deltaY = new Animated.Value(0);
    this.heightImageBanner = Utils.scaleWithPixel(250, 1);

  }

  componentDidMount() {
    console.log("ID", this.props.route.params)
    const { eventsActions } = this.props
    eventsActions.getEventById(this.props.route.params).then(res => {
      console.log("res", res.data)
    }).catch(rej => {
      console.log(rej)
    })
  }

  onHandleDelete = () => {
    const { dialogActions } = this.props;
    dialogActions.show("DeleteEvents", "Are you sure to delete this event", "YES", "NO",() => {this.deleteNews()})
  }

  deleteNews() {
    const {loadingActions, toastingActions, eventsActions} = this.props;
    loadingActions.show();
    eventsActions
      .deleteEvent(this.props.route.params._id)
      .then(res => {
        loadingActions.hide()
        toastingActions.show("Delete Event Successfully", "bottom", "success")
        this.props.navigation.goBack();
      }).catch(rej => {
        console.log("Error in deleteNews in news-view: ", rej)
        loadingActions.hide()
        toastingActions.show("Actions Fail (" + rej + ")", "bottom", "error")
      })
  }

  render(){
    const { username, password, savedDataLoaded, heightHeader} = this.state;
    const deltaY = this.deltaY;
    const data = this.props.event;
    if (data == null) return null;
    const { me } = this.props;
    console.log("Props in eventRegister: ", this.props);
    return (
    <View style = {{flex: 1, backgroundColor: 'white'}}>
      <Animated.View
        style={[
          styles.imgBanner,
          {
            height: deltaY.interpolate({
              inputRange: [
                0,
                Utils.scaleWithPixel(140),
                Utils.scaleWithPixel(140),
              ],
              outputRange: [this.heightImageBanner, heightHeader, heightHeader],
            }),
          },
        ]}>
        <Image source={{uri: API_URL + data.createdForm.media.path.thumbnail}} style={{flex: 1}} />
        <Animated.View
          style={{
            position: 'absolute',
            flexDirection: 'row',
            
            justifyContent: 'space-between',
            alignItems: 'flex-end',
            width: '100%',
            bottom: 0,
            padding: 10,
            backgroundColor: 'rgba(0,0,0,0.4)',

            opacity: deltaY.interpolate({
              inputRange: [
                0,
                Utils.scaleWithPixel(140),
                Utils.scaleWithPixel(140),
              ],
              outputRange: [1, 0, 0],
            }),
          }}>
          <View style={styles.rowBanner}>
            <Image source={{uri: API_URL + data.userCreated.media.thumbnail}} style={styles.userIcon} />
            <View style={{alignItems: 'flex-start'}}>
              <Text headline semibold whiteColor style={{color:'white'}}>
                {data.createdForm.title}
              </Text>
              <Text footnote whiteColor style={{color:'white'}}>
                {data.userCreated.name}
              </Text>
            </View>
          </View>
        </Animated.View>
      </Animated.View>
      <SafeAreaView style={{flex: 1}} forceInset={{top: 'always'}}>
        <ScrollView
          onScroll={Animated.event([
            {
              nativeEvent: {
                contentOffset: {y: this.deltaY},
              },
            },
          ])}
          onContentSizeChange={() => {
            this.setState({heightHeader: Utils.heightHeader()})
          }}
          scrollEventThrottle={8}
        >
          <View style={{height: 300 - heightHeader}} />
          <View
          style={{
            paddingHorizontal: 20,
            marginBottom: 20,
          }}>
            <Text title3 >
              {data.createdForm.title}
            </Text>
            <Header header="Description" />
            <Content content={data.createdForm.description} />

            <Header header="Social Days" />
            <Content content={data.createdForm.socialDays} />

            <Header header="Max Register" />
            <Content content={data.createdForm.maxRegister} />

            <Header header="Posting Form at" />
            <Content content={Utils.getDate(data.createdForm.formStart)} />

            <Header header="Closing Form at" />
            <Content content={Utils.getDate(data.createdForm.formEnd)} />

            <Header header="Event Start At" />
            <Content content={data.createdForm.eventStart != -1 ? Utils.getDate(data.createdForm.eventStart) : "Update Later"} />
            
            <Header header="Event Finish At" />
            <Content content={data.createdForm.eventEnd != -1 ? Utils.getDate(data.createdForm.eventEnd) : "Update Later"} />
            
            <Header header="Place Occur" />
            <Content content={data.createdForm.eventAddress ? data.createdForm.eventAddress : "Update later"} />
            
            <Header header="Unit held" />
            <Content content={data.createdForm.eventUnit ? data.createdForm.eventUnit : "Update later"} />
            
            <Header header="Requirements" />
            <Content content={data.createdForm.requirements ? data.createdForm.requirements : "Update later"} />

            <Header header="Gather At" />
            <Content content={data.createdForm.timeGather != -1 ? Utils.getDate(data.createdForm.timeGather) : "Update Later"} />

            <Header header="Place Gather" />
            <Content content={data.createdForm.placeGather ? data.createdForm.placeGather : "Update later"} />

            <Header header="Urgent ?" />
            <Content content={data.createdForm.isUrgent ? "Yes" : "No"} />
          </View>

          <Button
            onPress={() => {this.props.navigation.navigate("EditEvent", data._id)}}
            title="Edit Event"
            disabled={me.information._id == data.userCreated._id ? false : true}
          />

          <Button
            onPress={() => {this.props.navigation.navigate("ConfirmEvent", data._id)}}
            title="Report"
            disabled={me.information._id == data.userCreated._id ? false : true}
          />

          <Button 
            onPress={() => {this.props.navigation.navigate("ViewStaff", data._id)}}
            title="Staff's List"
          />

          <Button 
            onPress={() => {this.props.navigation.navigate("ViewRegister", data._id)}}
            title="Register's List"
          />

          <Button
            onPress={() => {this.onHandleDelete()}}
            title="Delete Event"
          />

        </ScrollView>
      </SafeAreaView>
    </View>
    );
  }
  
}

export default connect(
  state => ({me: state.me, event: state.events.event}),
  dispatch => channingActions({}, dispatch, bindMeActions, bindEventsActions, bindLoadingActions, bindDialogActions, bindToastNotificationActions)
)(EventDetailScreen)