import React from "react";
import { View, StyleSheet, Animated } from "react-native";
const styles = StyleSheet.create({
  imageOverlay: {
    position: "absolute",
    top: 0,
    zIndex: 9999
  },
  container: {
    // backgroundColor: "#e1e4e8"
  }
});

class ProgressiveImage extends React.Component {
  thumbnailAnimated = new Animated.Value(0);
  imageAnimated = new Animated.Value(0);

  handleThumbnailLoad = () => {
    Animated.timing(this.thumbnailAnimated, {
      toValue: 1
    }).start();
  };

  onImageLoad = () => {
    Animated.timing(this.imageAnimated, {
      toValue: 1
    }).start();
  };

  render() {
    const { thumbnailSource, source, style, ...props } = this.props;
    return (
      <View style={styles.container}>
       
        <Animated.Image
          {...props}
          source={thumbnailSource}
          style={[style, { opacity: this.thumbnailAnimated }]}
          blurRadius={1}
          onLoad={this.handleThumbnailLoad}
        />
         <Animated.Image
          {...props}
          source={source}
          style={[styles.imageOverlay, { opacity: this.imageAnimated }, style]}
          onLoad={this.onImageLoad}
        />
      </View>
    );
  }
}
export default ProgressiveImage;
