import { BASE_URL } from "../config";
import moment from "moment";
// import geolib from "geolib";
import {
  Dimensions,
  PixelRatio,
  Platform
} from 'react-native';
const scaleValue = PixelRatio.get() / 2;

class Utils {
  constructor() {}

  scaleWithPixel = (size, limitScale = 1.2) => {
    /* setting default upto 20% when resolution device upto 20% with defalt iPhone 7 */
    const value = scaleValue > limitScale ? limitScale : scaleValue;
    return size * value;
  };
  heightHeader = () => {
    const width = Dimensions.get('window').width;
    const height = Dimensions.get('window').height;
    const landscape = width > height;
  
    if (Platform.OS === 'android') return 45;
    if (Platform.isPad) return 65;
    switch (height) {
      case 375:
      case 414:
      case 812:
      case 896:
        return landscape ? 45 : 88;
      default:
        return landscape ? 45 : 65;
    }
  };
  getImgUrl(url) {
    return url;
  }
  formatTimeByPercent(time, timeOver) {
    var time = moment(new Date(time), "YYYYMMDD").fromNow();
    if (time.indexOf("s") !=-1)
    {
      // time = time.split("s");      
      time = parseInt(time);
      return (time*100)/(timeOver*3600);
    }
    else if(time.indexOf("m") !=-1)
    {
      // time = time.split("m");
      time = parseInt(time);
      return (time*100)/(timeOver*60);
    }
    else
    {
      // time = time.split("h");      
      time = parseInt(time);
      return (time*100)/(timeOver);
    }
  }
  getDate(time){
    return moment(new Date(time)).format("DD/MM/YYYY");
  }

  formatTime(time) {
    moment.updateLocale("en", {
      relativeTime: {
        future: "%s left",
        past: "%s ago",
        s: "1s",
        ss: "%ss",
        m: "1m",
        mm: "%dm",
        h: "1h",
        hh: "%dh",
        d: "1d",
        dd: "%dd",
        M: "1m",
        MM: "%dM",
        y: "1y",
        yy: "%dY"
      }
    });
    return moment(new Date(time), "YYYYMMDD").fromNow();
  }

  // getDistance(a, b) {
  //   let meter = geolib.getDistance(a, b);

  //   if (meter > 1000) return meter / 1000 + "km";
  //   else return meter + "m";
  // }
}

export default (Utils = new Utils());
