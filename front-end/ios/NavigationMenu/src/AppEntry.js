import React, { Component } from 'react';
// import App from 'next/app'
import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware } from 'redux'
import logger from 'redux-logger'
import navigation from './redux/reducers'

import { NavigationContainer } from '@react-navigation/native';
import { Router } from "./router";
import {initializeStore} from "./redux"
import Loading from './components/Loading';
import ToastingNotification from './components/ToastNotification';
import DialogScreen from './components/Dialog';
import {MenuProvider} from "react-native-popup-menu";
import Login from './screens/login'
// export default () => <Router />;
const store = initializeStore({})

export default class App extends Component {
    render() {
      return (
        <Provider store={store}>
            <Router />
            <Loading />
            <ToastingNotification />
            <DialogScreen />
        </Provider>
      );
    }
  }