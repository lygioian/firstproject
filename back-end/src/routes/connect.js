import { Router } from 'express';
import { ServerEventSystem } from '../server-events';

const router = Router();

router.get('/', (req, res) => {
    res.send(ServerEventSystem.tracking.getUserConnecting());
})

router.get('/test', (req, res) => {
    ServerEventSystem.notifyUser("5eb14ca56ff46324e4184f3f","Hi all!");
    res.send("Success");
})


export default router;
