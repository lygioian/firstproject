import { Router } from 'express';
import * as AdminService from '../services/admin.service';
import { verifyJWT_MW } from '../services/auth.service';

const router = Router();

/*
* Signup 
*/

router.post('/', (req, res) => {
    if(!req.body.firstName) {
        req.body.firstName = req.body.username;
    }

    const admin = {
        username: req.body.username,
        password: req.body.password,
        profile: {
            ...req.body
        }
    }
    
    AdminService.add(admin).then(data => {
        res.send(data);
    }).catch(error => {
        res.status(202).send({error: error.message});
    })
})

/*
* Authenticate required
*/

router.patch('*', verifyJWT_MW);
router.get('*', verifyJWT_MW);

/*
* Update personal info
*/
router.patch('/:id', (req, res) => {
    AdminService.editOne(req.params.id, req.body).then(data => {
        res.send(data);
    }).catch(error => {
        res.status(400).send({error: "Cannot update. No resource exist"});
    })
});

/*
* Change password
*/
router.patch('/password/change', (req, res) => {
    AdminService.changePassword(req.user._id, req.body.newPassword).then(data => {
        res.send(data);
    }).catch(error => {
        res.status(400).send({error: "Cannot update. No resource exist"});
    })
});

/*
* Get personal info
*/
router.get('/:id', (req, res) => {
    AdminService.getOne(req.params.id).then(data => {
        data ? res.send(data) : res.status(404).send({error: "User is not exist."});
    }).catch(error => {
        res.status(400).send({error: "User ID is not correct or not exist"});
    })
});

/**
 * List of all admin
 */
router.get('/', (req, res) => {
    AdminService.getAll().then(data => {
        res.send(data);
    });
})

/*
* Ban user
*/
router.patch('/:userId/ban', (req, res) => {
    AdminService.banUser(req.params.userId, req.body.banReason).then(data => {
        res.send(data);
    }).catch(error => {
        res.status(400).send(error);
    })
});

/*
* Dis-ban user
*/
router.patch('/:userId/disban', (req, res) => {
    AdminService.disbanUser(req.params.userId).then(data => {
        res.send(data);
    }).catch(error => {
        res.status(400).send(error);
    })
});

/*
* Activate user
*/
router.patch('/:userId/activate', (req, res) => {
    AdminService.activateUser(req.params.userId).then(data => {
        res.send(data);
    }).catch(error => {
        res.status(400).send(error);
    })
});

export default router;