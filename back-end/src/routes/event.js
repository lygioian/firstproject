import e, { Router } from "express";
import { verifyJWT_MW, verifyPermission } from "../services/auth.service";
import { filterLocation } from "../services/filter.service";
import { filterPaginator } from "../services/filter.service";
import {
  uploadImageIntoServer,
  changeUploadImageIntoServer,
} from "../services/upload.service";
// import * as Notification from '../services/notification.service';
import * as EventService from "../services/event.service";
import * as UserService from "../services/user.service";
import * as VerifyService from "../services/verify.service";
import * as SearchService from "../services/search.service";
// import * as PlaceService from '../services/place.service';
import { ServerEventSystem } from "../server-events";

import _ from "lodash";

const router = Router();

/**
 * Filter BEFORE handling
 */

router.all("*", verifyJWT_MW);

/**
 * Get Events
 */
const defaultQuery = {
  offset: 0,
  limit: 8,
  eventId: null,
  type: 0,
  constraint: 0,
  keyword: null,
};

router.get("/", (req, res) => {
  const query = {
    ...defaultQuery,
    ...req.query,
  };
  if (query.eventId != null) {
    EventService.getOne(query.eventId).then((data) => {
        data ? res.send(data) : res.status(404).send({ error: "Event does not exist." });
      }).catch((error) => {
        res.status(400).send({ error: "Event ID is not correct or does not exist" });
      });
  } else {
    switch (Number(query.constraint)) {
      case 0:
        EventService.getEvent(query).then((data) => {
          res.send(data);
        });
        break;

      case 1:
        EventService.postByUser(req.user._id).then((data) => {
          // data = _.sortBy(data, [(e) => { return parseInt(e.formEnd)}]);
          res.send(data);
        });
        break;

      case 2:
        EventService.postOfStaff(req.user._id)
          .then((data) => {
            data = _.sortBy(data, [
              (e) => {
                return parseInt(e.formEnd);
              },
            ]);
            res.send(data);
          })
          .catch((error) => {
            res.status(400).send(error);
          });
        break;

      case 3:
        UserService.getOneEvent(req.user._id)
          .then((data) => {
            res.send(data.eventRegisteredHistory);
          })
          .catch((error) => {
            res.status(400).send(error);
          });
        break;

      default:
        res.send("Invalid Constraint");
        break;
    }
  }
});

/**
 * Create an event (DONE)
 */

router.post("/", uploadImageIntoServer);

router.post("/", VerifyService.verifyAdmin, (req, res) => {
  if (req.user.role == "admin") {
    req.body.media = {
      type: req.fileUploaded.type,
      path: {
        original: req.fileUploaded.path,
        thumbnail: req.fileUploaded.thumbnailPath,
        small: req.fileUploaded.smallPath,
      },
    };

    const event = {
      userCreated: req.user._id,
      createdForm: {
        ...req.body,
      },
    };

    EventService.add(event)
      .then((post) => post)
      .then((post) => {
        res.send(post);
        UserService.updateNumberPost(req.user._id).then((user) => user);
      })
      .catch((error) => {
        res.status(202).send({ error: error.message });
      });
  } else {
    res.status(202).send({ error: "Not admin" });
  }
});

/*
 * Update event info ( DONE )
 */

router.patch("/:eventId", changeUploadImageIntoServer);

router.patch("/:eventId", VerifyService.verifyUserCreated, async function (req, res) {
  try {
    if (req.files !== null)
      req.body.media = {
        type: req.fileUploaded.type,
        path: {
          original: req.fileUploaded.path,
          thumbnail: req.fileUploaded.thumbnailPath,
          small: req.fileUploaded.smallPath,
        },
      };

    EventService.getOne(req.params.eventId)
      .then(async (data) => {
        data.createdForm.updatedAt = Date.now();
        const defaultData = data.createdForm;
        const updatedData = {
          ...defaultData,
          ...req.body,
        };

        let event = await EventService.editOneAsync(req.params.eventId, updatedData)
        .then((data) => { res.send(data); })
        .catch((error) => { res.status(400).send(error) });
      })
    .catch((err) => { res.status(500).json(err) });
  } catch (err) {
    res.status(500).send(err);
  }
});

/**
 * Delete a specific event by eventId (DONE)
 */

// An event is only deleted when it does not start (NOT DONE) !!!
router.delete("/:eventId", async function (req, res) {
  try {
    let a = await EventService.remove(req.params.eventId, req.user._id)
    .then((event) => {
      if (!event) { res.status(202).send({ error: "Cannot execute. Event not exist or you not created it.", }) }
      res.send(event);
    }).catch((error) => {
      res.status(400).send(error);
    });
  } catch (err) {
    res.status(500).send(err);
  }
  
});

/**
 * Register to a specific event ( DONE )
 */

router.post("/:eventId/register", async function (req, res) {
  try {
    let today = Date.now();
    let event = await EventService.getOne(req.params.eventId).then((data) => {
      if ((data.createdForm.formStart <= today) && (data.createdForm.formEnd >= today) && (data.userRegistered.length < data.createdForm.maxRegister)) {
        switch (String(req.user.role)) {
          case "user":
            EventService.registerEvent(req.params.eventId, req.user._id)
              .then((event) => {
                UserService.registerEvent(req.params.eventId, req.user._id).then(
                  (user) => {
                    if (!event) res.status(202).send({ error: "Cannot Register. The event does not exist!" });
                    else { res.send(event) }
                  });
              }).catch((error) => { res.status(400).send(error) });
            break;
      
          case "admin":
            EventService.registerEvent(req.params.eventId, req.query.userId)
              .then((event) => {
                UserService.registerEvent(req.params.eventId, req.query.userId).then(
                  (user) => {
                    if (!event) res.status(202).send({ error: "Cannot add this registration. User has been registered yet." });
                    else { res.send(event) }
                  });
              }).catch((error) => { res.status(400).send(error) });
            break;
        }
      }
      else 
        res.status(402).send({ error: "Can not register"})
    })
  } catch (err) {
    res.status(500).send(err);
  }
});

/**
 * Unregister a specific post ( DONE )
 */

router.delete("/:eventId/register", async function (req, res) {
  try {
    switch (String(req.user.role)) {
      case "user":
        let a = await EventService.unRegisterEvent(req.params.eventId, req.user._id)
          .then((event) => {
            UserService.unRegisterEvent(req.params.eventId, req.user._id)
            .then((user) => {
                if (!event) res.status(202).send({ error: "Cannot execute. User has never been registered yet." })
                else res.send(event);
              });
          }).catch((error) => { res.status(400).send(error) });
        break;
  
      case "admin":
        let b = await EventService.unRegisterEvent(req.params.eventId, req.query.userId)
          .then((event) => {
            UserService.unRegisterEvent(req.params.eventId, req.query.userId)
            .then((user) => {
                if (!event) res.status(202).send({ error: "Cannot execute. User has never been registered yet." })
                else res.send(event);
              });
          }).catch((error) => { res.status(400).send(error) });
        break;
    }
  } catch (err) {
    res.status(500).send(err);
  }
});

/**
 * AddStaff specific event ( DONE )
 */

router.post("/:eventId/staff", async function (req, res) {
  try {
    let a = await EventService.addStaff(req.params.eventId, req.query.userId, req.query.role)
    .then((event) => {
      if (!event) res.status(202).send({ error: "Cannot execute. User has been added yet." });
      else res.send(event);
    })
    .catch((error) => {
      res.status(400).send(error);
    });
  } catch (err) {
    res.status(500).send(err);
  }
});

/**
 * deleteStaff specific post ( )
 */

router.delete("/:eventId/staff", async function (req, res) {
  try {
    let a = await EventService.deleteStaff(req.params.eventId, req.query.userId, req.query.role)
    .then((event) => {
      if (!event) res.status(202).send({ error: "Cannot execute. User has never been added yet." });
      else res.send(event);
    })
    .catch((error) => {
      res.status(400).send(error);
    });
  } catch (err) {
    res.status(500).send(err);
  }
});

/**
 * Start / Finish Event ( DONE )
 */

router.patch("/:eventId/start", async function (req, res) {
  try {
    let a = await EventService.startEvent(req.params.eventId)
    .then((event) => {
      if (!event) res.status(202).send({ error: "Event does not exist!" });
      else res.send(event);
    })
    .catch((error) => {
      res.status(400).send(error);
    });
  } catch (err) {
    res.status(500).send(err);
  }
});

router.patch("/:eventId/finish", async function (req, res) {
  try {
    let a = await EventService.getOne(req.params.eventId)
    .then((event) => {
      if (event.eventStatus.start.isStart) {
        EventService.finishEvent(req.params.eventId).then((event) => {
          if (!event) res.status(202).send({ error: "Event does not exist!" });
          else res.send(event);
        });
      } else {
        res.status(202).send({ error: "Event has not started yet" });
      }
    })
    .catch((error) => {
      res.status(400).send(error);
    });
  } catch (err) {
    res.status(500).send(err);
  }
});

/**
 * Start / Finish FIRST Check Attendance ( DONE )
 */

router.patch("/:eventId/attendance/first/start", async function (req, res) {
  try {
    let a = await EventService.startFirstCheck(req.params.eventId)
    .then((event) => {
      if (!event) res.status(202).send({ error: "Event does not exist!" });
      else res.send(event);
    })
    .catch((error) => {
      res.status(400).send(error);
    });
  } catch (err) {
    res.status(500).send(err);
  }
});

router.patch("/:eventId/attendance/first/finish", async function (req, res) {
  try {
    let a = await EventService.getOne(req.params.eventId)
    .then((event) => {
      if (event.firstCheck.start.isStart) {
        EventService.finishFirstCheck(req.params.eventId).then((event) => {
          if (!event) res.status(202).send({ error: "Event does not exist!" });
          else res.send(event);
        });
      } else {
        res.status(202).send({ error: "First Check has not started yet" });
      }
    })
    .catch((error) => {
      res.status(400).send(error);
    });
  } catch (err) {
    res.status(500).send(err);
  }
});

/**
 * Start / Finish SECOND Check Attendance ( DONE )
 */

router.patch("/:eventId/attendance/second/start", async function (req, res) {
  try {
    let a = await EventService.startSecondCheck(req.params.eventId)
    .then((event) => {
      if (!event) res.status(202).send({ error: "Event does not exist!" });
      else res.send(event);
    })
    .catch((error) => {
      res.status(400).send(error);
    });
  } catch (err) {
    res.status(500).send(err);
  }
});

router.patch("/:eventId/attendance/second/finish", async function (req, res) {
  try {
    let a = await EventService.getOne(req.params.eventId)
    .then((event) => {
      if (event.secondCheck.start.isStart) {
        EventService.finishSecondCheck(req.params.eventId).then((event) => {
          if (!event) res.status(202).send({ error: "Event does not exist!" });
          else res.send(event);
        });
      } else {
        res.status(202).send({ error: "Second Check has not started yet" });
      }
    })
    .catch((error) => {
      res.status(400).send(error);
    });
  } catch (err) {
    res.status(500).send(err);
  }
});

/**
 * Check Attendance for User by userId ( DONE )
 */

router.patch("/:eventId/attendance/:userId", async function (req, res) {
  try {
    let a = await EventService.getOne(req.params.eventId)
    .then((event) => {
      UserService.getOne(req.params.userId).then((user) => {
        if (event.eventStatus.start.isStart) {
          if (event.firstCheck.start.isStart && !event.firstCheck.end.isEnd) {
            UserService.firstCheckAttendance(user._id, event._id).then(
              (data) => { res.send(data) }
            );
          } else if (event.secondCheck.start.isStart && !event.secondCheck.end.isEnd) {
            UserService.secondCheckAttendance(user._id, event._id).then(
              (data) => { res.send(data) }
            );
          } else {
            if (!event.firstCheck.start.isStart && !event.secondCheck.start.isStart)
              res.status(202).send({ error: "Checking Attendance does not started" });
            if (event.firstCheck.start.isStart && event.firstCheck.end.isEnd && !event.secondCheck.start.isStart)
              res.status(202).send({ error: "Second Checking Attendance does not started" });
            if (event.firstCheck.end.isEnd && event.secondCheck.end.isEnd)
              res.status(202).send({ error: "Checking Attendance has been done" });
          }
        } else res.status(202).send({ error: "The event does not start" });
      });
    })
    .catch((error) => {
      res.status(400).send(error);
    });
  } catch (err) {
    res.status(500).send(err);
  }
});

/**
 * Admin Confirm
 */
router.patch("/:eventId/admin/confirm", async function (req, res) {
  try {
    let a = await EventService.adminConfirm(req.params.eventId)
    .then((event) => {
      if (!event) res.status(202).send({ error: "Event does not exist!" });
      else res.send(event);
    })
    .catch((error) => {
      res.status(400).send(error);
    });
  } catch (err) {
    res.status(500).send(err);
  }
});

/**
 * MyBK Confirm
 */
router.patch("/:eventId/mybk/confirm", async function (req, res) {
  try {
    let a = await EventService.mybkConfirm(req.params.eventId)
    .then((event) => {
      if (!event) res.status(202).send({ error: "Event does not exist!" });
      else res.send(event);
    })
    .catch((error) => {
      res.status(400).send(error);
    });
  } catch (err) {
    res.status(500).send(err);
  }
});


/**
 * Upload Image in there
 */

router.patch("/:id", changeUploadImageIntoServer);

export default router;
