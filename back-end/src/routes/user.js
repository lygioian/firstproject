import { Router } from 'express';
import * as UserService from '../services/user.service';
import { verifyJWT_MW } from '../services/auth.service';
import { uploadImageIntoServer, changeUploadImageIntoServer } from '../services/upload.service';

const router = Router();

/*
* Signup 
*/

router.post('/', (req, res) => {
    if(!req.body.firstName) {
        req.body.firstName = req.body.username;
    }

    const user = {
        username: req.body.username,
        password: req.body.password,
        ...req.body
    }
    
    UserService.add(user).then(data => {
        res.send(data);
    }).catch(error => {
        res.status(202).send({error: error.message});
    })
})

/*
* Authenticate required
*/

router.patch('*', verifyJWT_MW);
router.get('*', verifyJWT_MW);

/*
* Update personal info
*/
router.patch('/', changeUploadImageIntoServer);

router.patch('/', (req, res) => {
    if(req.files !== null)
        req.body.media = {
                original: req.fileUploaded.path, 
                thumbnail: req.fileUploaded.thumbnailPath,
                small: req.fileUploaded.smallPath,
        };
    UserService.editOne(req.user._id, req.body, null, ['username', 'password']).then(data => {
        res.send(data);
    }).catch(error => {
        res.status(400).send({error: "Cannot update. No resource exist"});
    })
});

/*
* Change password
*/
router.patch('/password/change', (req, res) => {
    UserService.changePassword(req.user._id, req.body.newPassword).then(data => {
        res.send(data);
    }).catch(error => {
        res.status(400).send({error: "Cannot update. No resource exist"});
    })
});

/*
* Get personal info
*/
router.get('/:id', (req, res) => {
    UserService.getOne(req.params.id).then(data => {
        data ? res.send(data) : res.status(404).send({error: "User is not exist."});
    }).catch(error => {
        res.status(400).send({error: "User ID is not correct or not exist"});
    })
});

/**
 * List of all user
 */
// router.get('/', (req, res) => {
//     UserService.getAll().then(data => {
//         res.send(data);
//     });
// })

/**
 * Get User
 */

const defaultQuery = {
    offset: 0,
    limit: 8,
    type: 0,
    keyword: null,
}

router.get('/', (req, res) => {
    const query = {
        ...defaultQuery,
        ...req.query
    }
    console.log(query);
    if (query.userId != null) {
        UserService.getOne(query.userId).then(data => {
            data ? res.send(data) : res.status(404).send({error: "User is not exist."});
        }).catch(error => {
            res.status(400).send({error: "User ID is not correct or not exist"});
        })
    } else {
        UserService.getUser(query).then(data => {
            res.send(data);
        }); 
    }
})

/**
 * Upload Student Card Picture
 */

// router.post('/:id/uploadstudentpic', uploadImageIntoServer);

// router.post('/:id/uploadstudentpic', (req, res) => {
//     req.body. = {
//         type: req.fileUploaded.type,
//         path: { 
//             original: req.fileUploaded.path, 
//             thumbnail: req.fileUploaded.thumbnailPath,
//             small: req.fileUploaded.smallPath,
//         }
//     };    
//     UserService.uploadStudentPicture(req.params.id, req.body).then(data => {
//         res.send(data);
//     }).catch(error => {
//         res.status(400).send(error);
//     });
// });

export default router;