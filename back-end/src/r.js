// import { Router } from "express";
// import UserController from "./controllers/user";
// import EventController from "./controllers/event";
// import NewsController from "./controllers/news";
// import MeController from "./controllers/me";
// import ImageController from "./controllers/image";

// var jwt = require("jwt-simple");
// var secret = "aaaa";

// const routes = Router();

// const auth = (req, res, next) => {
//     const token = req.headers.authorization;
//     if(token == null){
//         res.status(401).send('Auth error');
//         return;
//     }

//     req.payload = jwt.decode(token, secret);
//     next();
// };

// //User services
// routes.post('/auth/login', UserController.login);
// routes.get("/users", UserController.getAll);
// routes.get("/users/:id", UserController.getById);
// routes.get("/search/users/:username", UserController.searchByUsername);
// routes.post('/auth/signup', UserController.create);
// routes.get("/me", auth, MeController.login);
// //Me services
// routes.patch('/me/changePassword', auth, UserController.changePassword);
// routes.patch('/me/profile', auth, MeController.update);

// //Event services
// routes.post('/events/create', auth, EventController.create);
// routes.get("/events", EventController.getAll);
// routes.post("/events/:eventId/checkin/:userId", EventController.checkInEvent);
// routes.delete("/events/:eventId/checkin/:userId", EventController.unCheckInEvent);
// routes.get("/events/users",auth ,EventController.getEventUserCreated);
// routes.get("/events/register",auth ,EventController.getEventRegister);
// routes.get("/events/users/lead",auth ,EventController.getEventUserLead);
// routes.get("/events/viewById/:id", EventController.getById);
// routes.patch('/events/:id', auth, EventController.update);
// routes.post('/events/:id/register', auth, EventController.register);
// routes.post('/events/:id/cancel', auth, EventController.cancelRegister);

// //News services
// routes.get('/news/viewAll', NewsController.getAll);
// routes.get("/news/viewById/:id", NewsController.getById);
// routes.post('/news/create', auth, NewsController.create);
// routes.patch("/news/:id/update", auth, NewsController.update);
// routes.get("/news/users",auth ,NewsController.getByUserCreated);

// //Upload file services
// routes.post('/image/upload', ImageController.upload)
// routes.get('/images/:image_name', ImageController.getImg)
// export default routes;
