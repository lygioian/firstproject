import Event from "../models/event.model";
import User from "../models/user.model";
// import Place from '../models/place.model';
import _ from "lodash";
import { Types, disconnect } from "mongoose";
import e from "express";

let userDefaultData = [
    "profile.firstName",
    "profile.lastName",
    "profile.socialDays",
];

export async function getEvent(query) {
    let now = Date.now();
    let result = await Event.aggregate([
        query.keyword == null
            ? { $skip: 0 }
            : {
                  $match: { $text: { $search: query.keyword } },
              },
        {
            $addFields: { typeGet: query.type },
        },
        {
            $match: {
                $expr: {
                    $cond: {
                        if: { $eq: ["3", "$typeGet"] },
                        then: {
                            $and: [
                                { $eq: ["$eventStatus.start.isStart", true] },
                                { $eq: ["$eventStatus.end.isEnd", false] },
                            ],
                        },
                        else: {
                            $cond: {
                                if: { $eq: ["2", "$typeGet"] },
                                then: { $eq: ["$createdForm.isUrgent", true] },
                                else: {
                                    $cond: {
                                        if: { $eq: ["1", "$typeGet"] },
                                        then: {
                                            $and: [
                                                {
                                                    $lte: [
                                                        "$createdForm.formStart",
                                                        now,
                                                    ],
                                                },
                                                {
                                                    $gte: [
                                                        "$createdForm.formEnd",
                                                        now,
                                                    ],
                                                },
                                            ],
                                        },
                                        else: {},
                                    },
                                },
                            },
                        },
                    },
                },
            },
        },
        {
            $limit: Number(query.limit),
        },
    ]);
    result = result.map((r) => new Event(r));
    result = await Event.populate(result, [
        { path: "userStaff.user" },
        { path: "userRegistered.user" },
        { path: "userCreated" },
    ]);

    return result;
}

export async function getOne(_id) {
    let event = await Event.findOne({ _id: _id })
        .populate("userCreated")
        .populate("userStaff.user")
        .populate("userRegistered.user");

    return event;
}

export async function getOneAsync(_id) {
    let event = await Event.findById(_id)
        .populate("userCreated")
        .populate("userStaff.user")
        .populate("userRegistered.user");

    return event;
}

export function add(data) {
    return Event.create(data);
}

export function remove(_id, userId) {
    return Event.findOneAndDelete({
        _id: _id,
        //userCreated: userId
    });
}

export async function editOneAsync(eventId, data) {
    let event = await Event.findOneAndUpdate(
        { _id: eventId },
        { $set: { createdForm: data } },
        { new: true }
    )
        .populate("userCreated")
        .populate("userStaff.user")
        .populate("userRegistered.user");

    return event;
}

export function postNearby(coordinate, radius, pageSize, offsetId) {
    // return Place.aggregate([
    //     {
    //         $geoNear: {
    //             near: {
    //                 type: "Point",
    //                 coordinates: coordinate
    //             },
    //             distanceField: "distance",
    //             maxDistance: radius,
    //         },
    //     },
    //     {
    //         $lookup: {
    //             from: 'post',
    //             let: { placeId: "$_id"},
    //             pipeline: [
    //                 {
    //                     $match: {
    //                         $expr: {
    //                             $and: [
    //                                 { $eq: ["$placeId", "$$placeId"] },
    //                                 { $gte: [ "$createdAt", new Date((new Date()).getTime()- 1000 * 60 * 60 * 3) ] }
    //                             ]
    //                         }
    //                     }
    //                 }
    //             ],
    //             as: 'post'
    //         }
    //     },
    //     {
    //         $addFields: {
    //             peopleVisiting: {
    //                 $size: {
    //                     $setUnion: {
    //                         $map: {
    //                             input: "$post",
    //                             as: "post",
    //                             in: "$$post.userPost"
    //                         }
    //                     }
    //                 }
    //             },
    //         }
    //     },
    //     {
    //         $unwind: "$post"
    //     },
    //     {
    //         $project: {
    //             _id: "$post._id",
    //             media: "$post.media",
    //             friends: "$post.friends",
    //             interactUser: "$post.interactUser",
    //             content: "$post.content",
    //             placeId: {
    //                 _id: "$_id",
    //                 location: "$location",
    //                 name: "$name"
    //             },
    //             rate: "$post.rate",
    //             userPost: "$post.userPost",
    //             atTime:  "$post.atTime",
    //             createdAt:  "$post.createdAt",
    //             distance: "$distance",
    //             peopleVisiting: "$peopleVisiting"
    //         }
    //     },
    //     {
    //         $lookup: {
    //             from: 'user',
    //             localField: 'userPost',
    //             foreignField: '_id',
    //             as: 'userPost'
    //         }
    //     },
    //     {
    //         $unwind: "$userPost"
    //     },
    //     {
    //         $project: {
    //             "userPost.isActivated": 0,
    //             "userPost.following": 0,
    //             "userPost.honoring": 0,
    //             "userPost.follower": 0,
    //             "userPost.honorer": 0,
    //             "userPost.username": 0,
    //             "userPost.password": 0,
    //             "userPost.email": 0,
    //             "userPost.createdAt": 0,
    //             "userPost.updatedAt": 0,
    //             "userPost.__v": 0,
    //             "userPost.bio": 0,
    //             "userPost.gender": 0,
    //             "userPost.phone": 0,
    //         }
    //     },
    //     {
    //         $sort: {
    //             atTime: -1,
    //             distance: -1
    //         }
    //     },
    //     {
    //         $match: {
    //             $expr: {
    //                 $cond: {
    //                     if: { $not: [ {$eq: [null, offsetId] } ] },
    //                     then: {
    //                         $lte: ["$_id", Types.ObjectId(offsetId)]
    //                     },
    //                     else: {
    //                     }
    //                 }
    //             }
    //         }
    //     },
    //     {
    //         $limit: pageSize
    //     }
    // ]);
}

export async function registerEvent(eventId, userId) {
    let event = await Event.findOneAndUpdate(
        {
            _id: eventId,
            "userRegistered.user": { $nin: [userId] },
        },
        {
            $push: { userRegistered: { user: userId } },
        },
        {
            new: true,
        }
    )
        .populate("userCreated")
        .populate("userStaff.user")
        .populate("userRegistered.user");

    return event;
}

export async function unRegisterEvent(eventId, userId) {
    let event = await Event.findOneAndUpdate(
        {
            _id: eventId,
            "userRegistered.user": { $in: [userId] },
        },
        {
            $pull: { userRegistered: { user: userId } },
        },
        {
            new: true,
        }
    )
        .populate("userCreated")
        .populate("userStaff.user")
        .populate("userRegistered.user");

    return event;
}

export async function startEvent(eventId) {
    let event = await Event.findOneAndUpdate(
        { _id: eventId },
        {
            $set: {
                "eventStatus.start.isStart": true,
                "eventStatus.start.startAt": Date.now(),
            },
        },
        { new: true }
    )
        .populate("userCreated")
        .populate("userStaff.user")
        .populate("userRegistered.user");

    return event;
}

export async function finishEvent(eventId) {
    let event = await Event.findOneAndUpdate(
        { _id: eventId },
        {
            $set: {
                "eventStatus.end.isEnd": true,
                "eventStatus.end.endAt": Date.now(),
            },
        },
        { new: true }
    )
        .populate("userCreated")
        .populate("userStaff.user")
        .populate("userRegistered.user");

    return event;
}

export async function startFirstCheck(eventId) {
    let event = await Event.findOneAndUpdate(
        { _id: eventId },
        {
            $set: {
                "firstCheck.start.isStart": true,
                "firstCheck.start.startAt": Date.now(),
            },
        },
        { new: true }
    )
        .populate("userCreated")
        .populate("userStaff.user")
        .populate("userRegistered.user");

    return event;
}

export async function finishFirstCheck(eventId) {
    let event = await Event.findOneAndUpdate(
        { _id: eventId },
        {
            $set: {
                "firstCheck.end.isEnd": true,
                "firstCheck.end.endAt": Date.now(),
            },
        },
        { new: true }
    )
        .populate("userCreated")
        .populate("userStaff.user")
        .populate("userRegistered.user");

    return event;
}

export async function startSecondCheck(eventId) {
    let event = await Event.findOneAndUpdate(
        { _id: eventId },
        {
            $set: {
                "secondCheck.start.isStart": true,
                "secondCheck.start.startAt": Date.now(),
            },
        },
        { new: true }
    )
        .populate("userCreated")
        .populate("userStaff.user")
        .populate("userRegistered.user");

    return event;
}

export async function finishSecondCheck(eventId) {
    let event = await Event.findOneAndUpdate(
        { _id: eventId },
        {
            $set: {
                "secondCheck.end.isEnd": true,
                "secondCheck.end.endAt": Date.now(),
            },
        },
        { new: true }
    )
        .populate("userCreated")
        .populate("userStaff.user")
        .populate("userRegistered.user");

    return event;
}

export async function firstCheckAttendance(eventId, userId) {
    let event = await Event.findOneAndUpdate(
        {
            _id: eventId,
            "userRegistered.user": {
                $in: [userId],
            },
        },
        {
            $set: { "userRegistered.$.checkAttendance.firstCheck": true },
        },
        {
            new: true,
        }
    )
        .populate("userCreated")
        .populate("userStaff.user")
        .populate("userRegistered.user");

    return event;
}

export async function secondCheckAttendance(eventId, userId) {
    let event = await Event.findOneAndUpdate(
        {
            _id: eventId,
            "userRegistered.user": {
                $in: [userId],
            },
        },
        {
            $set: { "userRegistered.$.checkAttendance.secondCheck": true },
        },
        {
            new: true,
        }
    )
        .populate("userCreated")
        .populate("userStaff.user")
        .populate("userRegistered.user");

    return event;
}

export async function adminConfirm(eventId) {
    let event = await Event.findOneAndUpdate(
        { _id: eventId },
        {
            "eventStatus.confirm.admin.isConfirm": true,
            "eventStatus.confirm.admin.confirmAt": Date.now(),
        },
        { new: true }
    )
        .populate("userCreated")
        .populate("userStaff.user")
        .populate("userRegistered.user");

    return event;
}

export async function mybkConfirm(eventId) {
    let event = await Event.findOneAndUpdate(
        { _id: eventId },
        {
            "eventStatus.confirm.myBK.isConfirm": true,
            "eventStatus.confirm.myBK.confirmAt": Date.now(),
        },
        { new: true }
    )
        .populate("userCreated")
        .populate("userStaff.user")
        .populate("userRegistered.user");

    return event;
}

export async function addStaff(eventId, userId, role) {
    let event = await Event.findOneAndUpdate(
        {
            _id: eventId,
            "userStaff.user": { $nin: [userId] },
        },
        {
            $push: { userStaff: [{ user: userId, role: role }] },
        },
        {
            new: true,
        }
    )
        .populate("userCreated")
        .populate("userStaff.user")
        .populate("userRegistered.user");

    return event;
}

export async function deleteStaff(eventId, userId, role) {
    let event = await Event.findOneAndUpdate(
        {
            _id: eventId,
            "userStaff.user": { $in: [userId] },
        },
        {
            $pull: { userStaff: { user: userId, role: role } },
        },
        {
            new: true,
        }
    )
        .populate("userCreated")
        .populate("userStaff.user")
        .populate("userRegistered.user");

    return event;
}

export function addScanner(_id, userId) {
    return Event.findOneAndUpdate(
        {
            _id: _id,
            eventScanner: {
                $nin: [userId],
            },
        },
        {
            $push: { eventScanner: userId },
        },
        {
            new: true,
        }
    )
        .populate("userCreated")
        .populate("userStaff.user")
        .populate("userRegistered.user");
}

export function deleteScanner(_id, userId) {
    return Event.findOneAndUpdate(
        {
            _id: _id,
        },
        {
            $pull: { eventScanner: userId },
        },
        {
            new: true,
        }
    )
        .populate("userCreated")
        .populate("userStaff.user")
        .populate("userRegistered.user");
}

export async function postByUser(userPost) {
    let event = await Event.find({ userCreated: userPost })
        .populate("userCreated")
        .populate("userStaff.user")
        .populate("userRegistered.user");

    return event;
}

export async function registerdByUser(user) {
    let event = await Event.find({
        "userRegistered.user": {
            $in: [user],
        },
    })
        .populate("userCreated")
        .populate("userStaff.user")
        .populate("userRegistered.user");

    return event;
}

export async function postOfStaff(userId) {
    let event = await Event.find({
        "userStaff.user": {
            $in: [userId],
        },
    })
        .populate("userCreated")
        .populate("userStaff.user")
        .populate("userRegistered.user");

    return event;
}

export function postOfScanner(userPost) {
    return Event.find({ eventScanner: userPost })
        .populate("userCreated")
        .populate("userStaff.user")
        .populate("userRegistered.user");
}
