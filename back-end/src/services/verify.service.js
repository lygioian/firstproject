import config from '../config';
import Admin from '../models/admin.model';
import User from '../models/user.model';
import Event from '../models/event.model';

export async function verifyAdmin(req, res, next) {
    try {
        if (req.user.role == 'user') 
            res.status(202).send({message: "Permission Denied"});
        next();
    }
    catch(err) {
        res.send(err);
    }
}

export async function verifyUserCreated(req, res, next) {
    try {
        Event.findById(req.params.eventId).then(event => {
            if (event.userCreated != req.user._id)
                res.status(202).send({message: "Permission Denied"});
            next();
        })
    }
    catch(err) {
        res.send(err);
    }
}