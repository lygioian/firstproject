import User from '../models/user.model';
import Event from '../models/event.model';
import * as HelperService from './helper.service';
import _ from 'lodash';
import { deactiveTokenUser } from '../services/token.service';

let userDefaultData = ['firstName', 'lastName', 'profilePicture', 'isPublished'];
let eventDefault = ['createdForm', 'eventStatus']

export function getUser(query) {
    return User.aggregate([
        (query.keyword == null) ? { $skip: 0 } :
        { $match: { $text: { $search: query.keyword } } },
        { $addFields: { typeGet: query.type } },
        {
            $match: {
                $expr: {
                    $cond: { // type 3: Banned users
                        if: { $eq: ["3", "$typeGet"] }, 
                        then: { $eq: ["$userStatus.isBan", true] },
                        else: {
                            $cond: { // type 2: Unactivated users
                                if: { $eq: ["2", "$typeGet"] }, 
                                then: { $eq: ["$userStatus.isActivated", false] },
                                else: {
                                    $cond: { // type 1: Activated users
                                        if: { $eq: ["1", "$typeGet"] }, 
                                        then: { $eq: ["$userStatus.isActivated", true] },
                                        else: { }
                                    } } }
                } } }
            }
        },
        // { $unwind: "$eventRegisteredHistory" },
        // {
        //     $lookup: 
        //     {
        //         from: "events",
        //         localField: "eventRegisteredHistory.event",
        //         foreignField: "_id",
        //         as: "eventInfo"
        //     }
        // },
        // { $unwind: "$eventInfo" },
        // {
        //     $project: { 
        //         "_id": 1,
        //         "eventRegisteredHistory":[{
        //             "event": "$eventRegisteredHistory.event",
        //             "eventInfo": "$eventInfo"
        //         }]
        //     }
        // },
        // {
        //     $project: { "eventRegisteredHistory.event": { "createdForm": 1 } }
        // },
        {
            $limit: Number(query.limit)
        }
    ]);
}

export function getAll() {
    return User.find()
    .populate('eventRegisteredHistory.event', 'createdForm')
    .populate('following', 'profile');
}

export function add(data, fields = null, excludes_field = null) {
    
    //filter some fields can be updated
    // if(fields != null) data = _.pick (data, fields);

    //filter some fields must not be updated
    // if(excludes_field != null) data = _.omit (data, excludes_field);

    //hashing password
    if(data.password !== undefined && data.password !== "") {
        data.password = HelperService.hashingPassword(data.password); 
    }

    return User.create(data);
}

export function getOne(_id) {
    return User.findOne({ _id: _id })
    .populate('eventStaff.event')
    .populate('eventRegisteredHistory.event');
}

export function getOneEvent(_id) {
    return User.findOne({ _id: _id })
    .populate('eventStaff.event')
    .populate('eventRegisteredHistory.event');
}

export function getRegisteredHistory(_id) {
    return User.findOne({ _id: _id})
    .populate('eventStaff.event')
    .populate('eventRegisteredHistory.event');
}

export function editOne(_id, updated_data, fields = null, excludes_field = null) {
    //filter some fields can be updated
    // if(fields) updated_data = _.pick(updated_data, fields);

    //filter some fields must not be updated
    // if(excludes_field != null) updated_data = _.omit (updated_data, excludes_field);

    return User.findOneAndUpdate(
        { _id: _id }, 
        updated_data, 
        { new: true }
    )
    .populate('eventStaff.event')
    .populate('eventRegisteredHistory.event');
}

export function uploadStudentPicture(_id, updated_data) {
    return User.findOneAndUpdate(
        { _id: _id }, 
        updated_data, 
        { new: true }
    )
    .populate('eventStaff.event')
    .populate('eventRegisteredHistory.event');
}

export function changePassword(_id, newPassword) {
    if(newPassword !== undefined && newPassword !== "") {
        newPassword = HelperService.hashingPassword(newPassword); 
    }

    return User.findOneAndUpdate(
        { _id: _id }, 
        { password: newPassword }, 
        { new: true}
    )
    .populate('eventStaff.event')
    .populate('eventRegisteredHistory.event');
}

export function deleteOne(id) {
    return User.destroy({
        where: { _id: id }
    });
}

export function authenticate(username, password) {
    return User.findOne({
        $or: [
            { username: username },
            { email: username }
        ],
        password: password,
        //isActivated: true,
        // isSocialAccount: false
    })
    .populate('eventStaff.event')
    .populate('eventRegisteredHistory.event');
}

export function updateNumberPost(_id) {
    return editOne(_id, { $inc: { "numberPost": 1 } }).then(user => user);
}

export function registerEvent(eventId, userId) {
    return User.findOneAndUpdate({ 
        _id: userId,
        "eventRegisteredHistory.event" : { $nin: [eventId] },
    }, {
        $push: { eventRegisteredHistory: { event: eventId } }
    },{ 
        new: true,
    })
    .populate('eventStaff.event')
    .populate('eventRegisteredHistory.event');
}

export function unRegisterEvent(eventId, userId) {
    return User.findOneAndUpdate({ 
        _id: userId,
        "eventRegisteredHistory.event" : { $in: [eventId] },
    }, {
        $pull: { eventRegisteredHistory: { event: eventId } }
    },{ 
        new: true,
    })
    .populate('eventStaff.event')
    .populate('eventRegisteredHistory.event');
}

export function firstCheckAttendance(userId, eventId) {
    return User.findOneAndUpdate({ 
        _id: userId,
        "eventRegisteredHistory.event": { $in: [eventId] }, 
    }, {
        "eventRegisteredHistory.$.checkAttendance.firstCheck.isCheck": true,
        "eventRegisteredHistory.$.checkAttendance.firstCheck.checkedTime": Date.now(),
    },{ 
        new: true,
    })
    .populate('eventStaff.event')
    .populate('eventRegisteredHistory.event');
}

export function secondCheckAttendance(userId, eventId) {
    return User.findOneAndUpdate({ 
        _id: userId,
        "eventRegisteredHistory.event": { $in: [eventId] }, 
    }, {
        "eventRegisteredHistory.$.checkAttendance.secondCheck.isCheck": true,
        "eventRegisteredHistory.$.checkAttendance.secondCheck.checkedTime": Date.now(),
    },{ 
        new: true,
    })
    .populate('eventStaff.event')
    .populate('eventRegisteredHistory.event');
}

/**
 * Follow
 */
export function followUser(_id, userIdFollow) {
    return new Promise((resolve, reject) => {
        if(userIdFollow == _id) reject("Cannot follow yourself");
        else {
            addUserFollower(userIdFollow, _id).then(userFollow => {
                resolve(follow(_id, userIdFollow))
            }).catch(err => {
                reject(err)
            })
        }
    })
}

export function unfollowUser(_id, userIdFollow) {
    return new Promise((resolve, reject) => {
        removeUserFollower(userIdFollow, _id).then(userFollow => {
            resolve(unfollow(_id, userIdFollow))
        }).catch(err => {
            reject(err)
        })
    })
}

export function follow(_id, userIdFollow) {
    return User.findOneAndUpdate({ 
        _id: _id,
        following: {
            $nin: [ userIdFollow ]
        }, 
    }, {
        $push: { following: userIdFollow }
    }, { 
        new: true,
    }).populate('following').populate('follower');
}

export function addUserFollower(_id, userIdFollower) {
    return User.findOneAndUpdate({ 
        _id: _id,
        follower: {
            $nin: [ userIdFollower ]
        }, 
    }, {
        $push: { follower: userIdFollower }
    }, { 
        new: true,
    }).populate('following').populate('follower');
}

export function unfollow(_id, userIdFollow) {
    return User.findOneAndUpdate({ 
        _id: _id,
    }, {
        $pull: { following: userIdFollow }
    }, { 
        new: true,
    }).populate('following').populate('follower');
}

export function removeUserFollower(_id, userIdFollower) {
    return User.findOneAndUpdate({ 
        _id: _id,
    }, {
        $pull: { follower: userIdFollower }
    }, { 
        new: true,
    }).populate('following').populate('follower');
}

/**
 * Honor User 
 */

export function honorUser(_id, userIdFollow) {
    return new Promise((resolve, reject) => {
        if(userIdFollow == _id) reject("Cannot honor yourself");
        else {
            addUserHonorer(userIdFollow, _id).then(userFollow => {
                resolve(honor(_id, userIdFollow))
            }).catch(err => {
                reject(err)
            })
        }
    })
}

export function unhonorUser(_id, userIdFollow) {
    return new Promise((resolve, reject) => {
        removeUserHonorer(userIdFollow, _id).then(userFollow => {
            resolve(unhonor(_id, userIdFollow))
        }).catch(err => {
            reject(err)
        })
    })
}

export function honor(_id, userIdFollow) {
    return User.findOneAndUpdate({ 
        _id: _id,
        honoring: {
            $nin: [ userIdFollow ]
        }, 
    }, {
        $push: { honoring: userIdFollow }
    }, { 
        new: true,
    }).populate('honoring').populate('honorer');
}

export function addUserHonorer(_id, userIdFollower) {
    return User.findOneAndUpdate({ 
        _id: _id,
        honorer: {
            $nin: [ userIdFollower ]
        }, 
    }, {
        $push: { honorer: userIdFollower }
    }, { 
        new: true,
    }).populate('honoring').populate('honorer');
}

export function unhonor(_id, userIdFollow) {
    return User.findOneAndUpdate({ 
        _id: _id,
    }, {
        $pull: { honoring: userIdFollow }
    }, { 
        new: true,
    }).populate('honoring').populate('honorer');
}

export function removeUserHonorer(_id, userIdFollower) {
    return User.findOneAndUpdate({ 
        _id: _id,
    }, {
        $pull: { honorer: userIdFollower }
    }, { 
        new: true,
    }).populate('honoring').populate('honorer');
}