const mongoose = require("mongoose");
import { Schema } from 'mongoose';
import Database from '../database';

var adminSchema = new mongoose.Schema({
  username: {
    type: String,
    unique: true,
    required: [true, "Username cannot blank"],
  },

  password: {
    type: String,
    required: [true, "Password name cannot blank"]
  },

  createdAt: {
    type: Number,
    default: new Date()
  },

  userStatus: {

    isBan: {
      type: Boolean,
      default: false,
    },

    reason: {
      type: String,
      default: ''
    }

  },

  role: {
    type: String,
    default: 'admin'
  },
  
  name: {
    type: String,
    default: "Bach Khoa Administrator"
  },

  description: {
    type: String,
    default: "Bach Khoa Administrator"
  },

  address: {
    type: String,
    default: "Ho Chi Minh University of Technology"
  },

  phone: {
    type: String,
    default: "123456789"
  },

  email: {
    type: String,
    default: "admin@hcmut.edu.vn"
  },

  media: {
    original: {
      type: String,
      required: true,
      default: '/static/1592493602.jpg'
    },

    thumbnail: {
        type: String,
        required: true,
        default: '/static/1592493602.jpg'
    },

    small: {
        type: String,
        required: true,
        default: '/static/1592493602.jpg'
    }
  },

  follower: [{
    type: Schema.Types.ObjectId, 
    ref: 'User'
  }],

  eventHistory: [{
    event: {
      type: Schema.Types.ObjectId, 
      ref: 'Event'
    },
    isConfirm: {
      type: Boolean,
      default: false,
    }
  }],

  newsHistory: [{
    post: {
      post: {
        type: Schema.Types. ObjectId,
        ref: 'News'
      }
    }
  }]

});

// userSchema.index({ username: 'text'});
// userSchema.index({ firstName: 'text' });
// userSchema.index({ lastName: 'text' });
// userSchema.index({ email: 'text' });
// userSchema.index({ bio: 'text' });

export default Database.model("Admin", adminSchema, "admin");
