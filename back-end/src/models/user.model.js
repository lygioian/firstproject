const mongoose = require("mongoose");
import { Schema } from 'mongoose';
import Database from '../database';

var userSchema = new mongoose.Schema({
  username: {
    type: String,
    unique: true,
    required: [true, "Username cannot be blank"],
  },

  password: {
    type: String,
    required: [true, "Password cannot be blank"]
  },

  createdAt: {
    type: Number,
    default: new Date()
  },

  studentCardPicture: {
    original: {
      type: String,
      required: true,
      default: '/static/1592493602.jpg'
    },
    thumbnail: {
        type: String,
        required: true,
        default: '/static/1592493602.jpg'
    },
    small: {
        type: String,
        required: true,
        default: '/static/1592493602.jpg'
    }
  },

  userStatus: {

    isActivated: {
      type: Boolean,
      default: false
    },

    activatedStatus: {
      type: String,
      default: 'notActivated' // notActivated, waiting, success, fail
    },

    isBan: {
      type: Boolean,
      default: false,
    },

    banReason: {
      type: String,
      default: ''
    }

  },

  role: {
    type: String,
    default: 'user'
  },
  lastName: {
    type: String,
  },

  firstName: {
    type: String,
  },

  fullName: {
    type: String,
  },

  description: {
    type: String,
  },

  dob: {
    type: Number,
  },

  address: {
    type: String,
  },

  phone: {
    type: String,
  },

  email: {
    type: String,
  },

  gender: {
    type: String,
  },

  studentId: {
    type: String,
  },

  classId: {
    type: String,
  },

  socialDays: {
    type: Number,
  },

  media: {

    original: {
      type: String,
      required: true,
      default: '/static/1592493602.jpg'
    },

    thumbnail: {
        type: String,
        required: true,
        default: '/static/1592493602.jpg'
    },

    small: {
        type: String,
        required: true,
        default: '/static/1592493602.jpg'
    }

  },

  following: [{
    type: Schema.Types.ObjectId, 
    ref: 'Admin'
  }],

  eventStaff: [{

    event: {
      type: Schema.Types.ObjectId,
      ref: 'Event'
    },

    isAttempt: {
      type: Boolean,
      default: false,
    },

    attemptAt: {
      type: Number,
    },

    socialDays: {
      type: Number,
      default: 0,
    },

    reason: {
      type: String,
      default: '',
    },

    role: {
      type: String,
      required: true,
    }

  }],

  eventRegisteredHistory: [{

    event: {
      type: Schema.Types.ObjectId,
      ref: 'Event'
    },

    checkAttendance: {

      firstCheck: {
        checkedTime: {
          type: Number,
        },
        isCheck: {
          type: Boolean,
          default: false,
        }
      },

      secondCheck: {
        checkedTime: {
          type: Number,
        },
        isCheck: {
          type: Boolean,
          default: false,
        }
      },

    },

    socialDays: {
      type: Number,
      default: 0,
    },

    isAttempt: {
      type: Boolean,
      default: false,
    },

    registeredAt: {
      type: Number,
      default: Date.now(),
    },
  }]

});

// userSchema.index({ username: 'text'});
// userSchema.index({ firstName: 'text' });
// userSchema.index({ lastName: 'text' });
// userSchema.index({ email: 'text' });
// userSchema.index({ bio: 'text' });
userSchema.index({username: 'text'});
userSchema.index({"lastName": "text"});
userSchema.index({"firstName": 'text'});
userSchema.index({"fullName": 'text'});

// export default mongoose.model("User", userSchema);
export default Database.model("User", userSchema, "user");
